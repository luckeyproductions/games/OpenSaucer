# ![Open Saucer](logo.png)

Endianness ends here.

## Controls

  Action       | Keyboard | Mouse       | Controller                
---------------|----------|-------------|----------------           
 Move          | WASD     |             | Left stick                
 Thruster      | Shift    | Left-click  | A / Cross                 
 Jump          | Space    | Right-click | X / Square   
 Signal        | Control  |             | B / Circle          
 Rotate camera | Numpad   | Move        | Right stick and shoulders 
 Field of view | +/-      | Wheel       | Triggers                  

## Screenshots

[![](Screenshots/Screenshot0.jpg)](Screenshots/Screenshot0.jpg)

[![](Screenshots/Screenshot1.jpg)](Screenshots/Screenshot1.jpg)

[![](Screenshots/Screenshot2.jpg)](Screenshots/Screenshot2.jpg)
