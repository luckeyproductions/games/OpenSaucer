## Used licenses

_[CC0](https://creativecommons.org/publicdomain/zero/1.0/), [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/), [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)_

## Assets by type, author and license

### Various

##### Author irrelevant
- CC0
    + Screenshots/*
    + Resources/Materials/*
    + Resources/PostProcess/*
    + Resources/RenderPaths/*
    + Resources/Shaders/*
    + Resources/Techniques/*
    + Resources/Textures/Ramp.*
    + Resources/Textures/Spot.*

### Music

##### Anyer Quantum (edited by Modanung)
- CC-BY 4.0 [| Source |](https://ektoplazm.com/free-music/anyer-quantum-subliminal-bouquet)
    + Resources/Music/Menu.ogg (derived from Ghastly Deluxe)
    + Resources/Music/Green.ogg (derived from Ghastly Deluxe)
    + Resources/Music/Red.ogg (derived from Introduction to Chaos Magick)
    + Resources/Music/Cyans.ogg (derived from Subliminal Bouquet)
     
### Samples

##### Modanung
- CC0
    + Resources/Samples/Engine.ogg (made with SuperCollider)
    + Resources/Samples/Hit.ogg (made with SuperCollider)
    + Resources/Samples/Jump.ogg (made with SuperCollider)
    + Resources/Samples/Thud.ogg (made with SuperCollider)
    + Resources/Samples/ThudEcho.ogg (made with SuperCollider)
     
### 2D   

##### GGBotNet
- CC0
    + Resources/Fonts/Unitblock.ttf [| Source |](https://www.dafont.com/unitblock.font)
    
##### Modanung
- CC-BY-SA 4.0
    + ball*
    + logo.*
    + saucer.svg
    + Resources/icon.png
    + Resources/Textures/Plasma.png
    + Resources/Textures/Thrust.png
    + Resources/Textures/Trail.png
    + OpenEncounter/*.svg

### 3D

##### Modanung
- CC-BY-SA 4.0
    + Blends/*
    + Resources/Models/*