#!/bin/sh

sudo chown -R $USER ~/.local/share/luckey/saucer/
sudo chown $USER ~/.local/share/icons/saucer.svg
update-icon-caches ~/.local/share/icons/
