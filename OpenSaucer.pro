include(src/OpenSaucer.pri)

TARGET = saucer

LIBS += \
    $$(DRY_HOME)/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    $$(DRY_HOME)/include \
    $$(DRY_HOME)/include/Dry/ThirdParty \

TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

DISTFILES += \
    LICENSE \
    README.md

unix
{
    isEmpty(DATADIR) DATADIR = $$(XDG_DATA_HOME)
    isEmpty(DATADIR) DATADIR = $$(HOME)/.local/share

    target.path = /usr/games/
    INSTALLS += target

    resources.path = $$DATADIR/luckey/saucer/
    resources.files = Resources/*
    INSTALLS += resources

    DEFINES += RESOURCEPATH=\\\"$${resources.path}\\\"
}
