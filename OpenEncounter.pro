TARGET = saucer-launcher

include(OpenEncounter/OpenEncounter.pri)

QT += core gui \
#    network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

LIBS += -lsfml-window

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

RESOURCES += resources.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

unix
{
    isEmpty(DATADIR) DATADIR = $$(XDG_DATA_HOME)
    isEmpty(DATADIR) DATADIR = $$(HOME)/.local/share

    target.path = /usr/games/
    INSTALLS += target

    icon.path = $$DATADIR/icons/
    icon.files = saucer.svg
    INSTALLS += icon

    desktop.path = $$DATADIR/applications/
    desktop.files = saucer.desktop
    INSTALLS += desktop

    DEFINES += RESOURCEPATH=\\\"$${resources.path}\\\"
}
