/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "spawnmaster.h"
#include "inputmaster.h"
#include "jib.h"
#include "saucer.h"
#include "saucerbrain.h"
#include "player.h"
#include "jukebox.h"

#include "game.h"

void Game::RegisterObject(Context* context)
{
    Match::RegisterObject(context);
}

Game::Game(Context* context): Object(context),
    gameState_{ GS_MENU },
    currentMatch_{ nullptr },
    replay_{ context_->CreateObject<class Replay>() }
{
    SubscribeToEvent(E_MATCHJOINED, DRY_HANDLER(Game, HandleMatchJoined));
}

void Game::PrepareMatch(Match* match)
{
    if (currentMatch_)
        currentMatch_->End();

    if (match)
    {
        currentMatch_ = match;

        if (currentMatch_->IsHosted())
        {
            const unsigned short port{ MC->ServerPort() };
            if (!NETWORK->StartServer(port))
            {
                if (ENGINE->IsHeadless())
                    MC->ErrorExit("Failed to start server on port " + String{ port });
                // else display UI Text
            }
            else
            {
                SubscribeToEvent(E_CLIENTCONNECTED,    DRY_HANDLER(Game, HandleClientConnected));
                SubscribeToEvent(E_CLIENTDISCONNECTED, DRY_HANDLER(Game, HandleClientDisconnected));
                SubscribeToEvent(E_REQUESTSAUCERS,     DRY_HANDLER(Game, HandleRequestSaucers));
            }
        }
    }
    else
    {
        // Start default local match
        currentMatch_ = context_->CreateObject<Match>();
    }

    Ball* ball{ SPAWN->GetActive<Ball>().Front() };
    if (!ball)
        return;

    ball->Reset();

    // Place Saucers
    const int numSaucers{ currentMatch_->GetNumSaucers() };
    const int saucersPerRow{ Clamp(numSaucers / 2, 1, 7) };

    for (int s{ 0 }; s < numSaucers; ++s)
    {
        Saucer* saucer{ SPAWN->Create<Saucer>() };
        const bool cyans{ s % 2 == 0 };
        const float evenShift{ .5f * ((saucersPerRow + 1) % 2) };
        saucer->Set((cyans ? Vector3::LEFT : Vector3::RIGHT) * (343.75f - (31.25f * (s / (2 * saucersPerRow)))) +
                    (((s / 2) % saucersPerRow - saucersPerRow / 2) + evenShift) * Vector3::FORWARD * 28.125f, static_cast<Team>(cyans));
    }

    PODVector<Saucer*> freeSaucers{ SPAWN->GetSaucers(TEAM_ALL) };
    PODVector<Saucer*> redSaucers{ SPAWN->GetSaucers(TEAM_RED) };
    PODVector<Saucer*> cyanSaucers{ SPAWN->GetSaucers(TEAM_CYANS) };

    if (!ENGINE->IsHeadless())
    {
        if (MC->GetArgNumPlayers() == 0)
        {
            MC->GetPlayers().Front()->GetJib()->SetTarget(ball);
        }
        else if (numSaucers > 0)
        {
            // Assign saucers to local players
            for (Player* p: MC->GetPlayers())
            {
                Jib* jib{ p->GetJib() };

                if (freeSaucers.IsEmpty())
                {
                    IM->SetPlayerControl(p, nullptr);
                    jib->SetTarget(ball->GetNode());
                    continue;
                }

                Saucer* saucer{ nullptr };
                const Team preference{ p->GetTeamPreference() };
                if (preference == TEAM_RED && !redSaucers.IsEmpty())
                    saucer = redSaucers.At(Random(static_cast<int>(redSaucers.Size())));
                else if (preference == TEAM_CYANS && !cyanSaucers.IsEmpty())
                    saucer = cyanSaucers.At(Random(static_cast<int>(cyanSaucers.Size())));
                else
                    saucer = freeSaucers.At(Random(static_cast<int>(freeSaucers.Size())));

                assert(saucer);
                Node* saucerNode{ saucer->GetNode() };

                if (saucerNode->HasComponent<SaucerBrain>())
                    saucerNode->RemoveComponent<SaucerBrain>();

                IM->SetPlayerControl(p, saucer);
                jib->SetTarget(saucer);
                jib->Rotate({ saucer->GetTeam() == TEAM_RED ? -90.f : 90.f, 0.f });

                freeSaucers.Remove(saucer);
                if (redSaucers.Contains(saucer))
                    redSaucers.Remove(saucer);
                else if (cyanSaucers.Contains(saucer))
                    cyanSaucers.Remove(saucer);
            }
        }
    }

    // Add brains to saucers without
    for (Saucer* saucer: freeSaucers)
        saucer->GetNode()->CreateComponent<SaucerBrain>(LOCAL);

    // Play fitting music
    GetSubsystem<JukeBox>()->PickSong();

    // Start recording
    currentMatch_->Start();
    gameState_ = GS_PLAY;
}

void Game::JoinMatch(SharedPtr<Match> match)
{
    currentMatch_ = match;
    gameState_ = GS_PLAY;
}

void Game::StartReplay(Match* match)
{
    currentMatch_ = match;
    gameState_ = GS_REPLAY;

    SPAWN->GetAll<Ball>().Front()->Reset();

    // Play fitting music
    GetSubsystem<JukeBox>()->PickSong();

    Replay();
}

void Game::Replay(float from, float to)
{
    if (!currentMatch_)
        return;

    MC->GetScene()->GetComponent<PhysicsWorld>()->SetUpdateEnabled(false);
}

void Game::InstantReplay(float duration)
{
    if (!currentMatch_)
        return;

    // Create replay scene
//    Replay(Max(start, now - duration), now);
}

void Game::HandleClientConnected(StringHash /*eventType*/, VariantMap& eventData)
{
    Connection* newConnection{ static_cast<Connection*>(eventData[ClientConnected::P_CONNECTION].GetPtr()) };
    newConnection->SetScene(MC->GetScene());
    newConnection->SendRemoteEvent(E_MATCHJOINED, false, currentMatch_->MatchInfo());
}

void Game::HandleRequestSaucers(StringHash /*eventType*/, VariantMap& eventData)
{
    Connection* client{ static_cast<Connection*>(eventData[RemoteEventData::P_CONNECTION].GetPtr()) };
    VectorBuffer players{ eventData[RequestSaucers::P_IDS].GetVectorBuffer() };
    PODVector<Saucer*> freeSaucers{ SPAWN->GetSaucers(TEAM_ALL, true) };

    Log::Write(LOG_INFO, client->GetAddress() + " requested " + String{ players.GetSize() / sizeof(int) } + " saucers");

    while (!players.IsEof())
    {
        int id{ players.ReadInt() };
        Player* remotePlayer{ MC->AddPlayer(client, id) };

        if (!freeSaucers.IsEmpty())
        {
            Saucer* saucer{ freeSaucers.At(Random(static_cast<int>(freeSaucers.Size()))) };
            Node* saucerNode{ saucer->GetNode() };
            saucerNode->RemoveComponent<SaucerBrain>();
            freeSaucers.Remove(saucer);

            IM->SetPlayerControl(remotePlayer, saucer);
        }
        else
        {
            IM->SetPlayerControl(remotePlayer, nullptr);
        }
    }
}

void Game::HandleClientDisconnected(StringHash /*eventType*/, VariantMap& eventData)
{
    Connection* disconnection{ static_cast<Connection*>(eventData[ClientDisconnected::P_CONNECTION].GetPtr()) };

    for (Player* p: MC->GetRemotePlayers(disconnection))
    {
        Controllable* c{ p->GetControllable() };
        if (c)
        {
            IM->SetPlayerControl(p, nullptr);
            c->GetNode()->CreateComponent<SaucerBrain>(LOCAL);
        }
    }

    MC->RemovePlayers(disconnection);
}

void Game::HandleMatchJoined(StringHash /*eventType*/, VariantMap& eventData)
{
    JoinMatch(MakeShared<Match>(context_, eventData));
}
