/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "inputmaster.h"
#include "controllable.h"
#include "player.h"

void Controllable::RegisterObject(Context* context)
{
    DRY_ACCESSOR_ATTRIBUTE("Move", GetMove, SetMove, Vector3, Vector3::ZERO, AM_NET);
    context->NETWORK->RegisterRemoteEvent(E_SETMOVE);
    DRY_ACCESSOR_ATTRIBUTE("Actions", GetActions, SetActions, unsigned, 0u, AM_NET);
    context->NETWORK->RegisterRemoteEvent(E_SETACTIONS);
}

Controllable::Controllable(Context* context): SceneObject(context),
    move_{},
    actions_{},
    actionSince_{},
    actionInterval_{},
    lastMoveUpdateJibYaw_{ M_INFINITY }
{
    for (unsigned a{ 0u }; a < actions_.size(); ++a)
        actionSince_[a] = actionInterval_[a] = 0.f;
}

void Controllable::Start()
{
    node_->SetName("Controllable");
    SubscribeToEvent(node_, E_SETMOVE, DRY_HANDLER(Controllable, HandleClientSetMove));
    SubscribeToEvent(node_, E_SETACTIONS, DRY_HANDLER(Controllable, HandleClientSetActions));
}

void Controllable::Update(float timeStep)
{
    for (unsigned a{ 0u }; a < actions_.size(); ++a)
    {
        if (actions_[a] || actionSince_[a] > 0.f)
            actionSince_[a] += timeStep;
    }
}

void Controllable::SetMove(const Vector3& move)
{
    const Vector3 oldMove{ move_ };

    move_ = (move.Length() < 1.f ? move : move.Normalized());

    Player* p{ GetPlayer() };
    Jib* j{ (p == nullptr ? nullptr : p->GetJib()) };
    float jibYaw{ (j ? j->GetNode()->GetWorldRotation().YawAngle() : 0.f) };
    if (move_ != oldMove || lastMoveUpdateJibYaw_ != jibYaw)
    {
        if (NETWORK->IsServerRunning())
        {
            MarkNetworkUpdate();
        }
        else if (node_ && NETWORK->GetServerConnection() && p)
        {
            Connection* serverConnection{ NETWORK->GetServerConnection() };
            const Quaternion camRot{ jibYaw, Vector3::UP };
            VariantMap eventData{ { SetMove::P_MOVE, camRot * move_ } };
            serverConnection->SendRemoteEvent(node_, E_SETMOVE, true, eventData);
        }

        lastMoveUpdateJibYaw_ = jibYaw;
    }
}

void Controllable::SetActions(std::bitset<4> actions)
{
    if (actions == actions_)
    {
        return;
    }
    else
    {
        for (unsigned i{ 0u }; i < actions.size(); ++i)
        {
            if (actions_[i] != actions[i])
            {
                actions_[i] = actions[i];

                if (actions_[i])
                {
                    if (actionSince_[i] >= actionInterval_[i])
                        HandleAction(i);
                }
                else
                {
                    HandleReleaseAction(i);
                }
            }
        }

        if (NETWORK->IsServerRunning())
        {
            MarkNetworkUpdate();
        }
        else if (node_ && NETWORK->GetServerConnection() && GetPlayer())
        {
            Connection* serverConnection{ NETWORK->GetServerConnection() };
            VariantMap eventData{ { SetActions::P_ACTIONS, GetActions()} };
            serverConnection->SendRemoteEvent(node_, E_SETACTIONS, true, eventData);
        }
    }
}

void Controllable::HandleAction(int actionId)
{
    actionSince_[actionId] = 0.f;
}

void Controllable::HandleReleaseAction(int actionId)
{
}

void Controllable::ClearControl()
{
    move_ = Vector3::ZERO;
    actions_.reset();

    MarkNetworkUpdate();
}

Player* Controllable::GetPlayer()
{
    Player* local{ IM->GetPlayerByControllable(this) };
    if (local)
        return local;
    else
        for (Player* p: MC->GetRemotePlayers())
            if (p->GetControllable() == this)
                return p;

    return nullptr;
}

void Controllable::HandleClientSetMove(StringHash /*eventType*/, VariantMap& eventData)
{
    if (!NETWORK->IsServerRunning())
        return;

//    Connection* connection{ dynamic_cast<Connection*>(eventData[RemoteEventData::P_CONNECTION].GetPtr()) };
//    Player* player{ GetPlayer() };

//    if (player && player->GetConnection() == connection && GetEventSender() == this)
    SetAttribute("Move", eventData[SetMove::P_MOVE].GetVector3());
}

void Controllable::HandleClientSetActions(StringHash /*eventType*/, VariantMap& eventData)
{
    if (!NETWORK->IsServerRunning())
        return;

    SetAttribute("Actions", eventData[SetActions::P_ACTIONS].GetUInt());
}
