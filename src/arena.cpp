/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "arena.h"
#include "spawnmaster.h"

#include "ball.h"
#include "saucer.h"

Arena::Arena(Context* context): Component(context),
    wallNode_{ nullptr },
    walls_{ nullptr },
    size_{},
    goalSize_{ 34.f, 27.f, 55.f },
    wallAngle_{},
    hall_{},
    spectators_{ 100, Spectator{} }
{
    SubscribeToEvent(E_SCENEUPDATE, DRY_HANDLER(Arena, UpdateSpectators));
    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Arena, Draw));
}

void Arena::OnNodeSet(Node* node)
{
    if (!node)
        return;
    else
        context_->RegisterSubsystem(this);

    wallNode_ = node_->CreateChild("Walls");

    //    StaticModel* arenaModel{ wallNode_->CreateComponent<StaticModel>() };
    //    arenaModel->SetModel(walls_);
    //    arenaModel->SetMaterial(RES(Material, "Materials/Field.xml"));
    RigidBody* rigidBody{ wallNode_->CreateComponent<RigidBody>() };
    rigidBody->SetRestitution(.7f);
    rigidBody->SetFriction(1/3.f);
    rigidBody->SetCollisionLayer(LAYER(1));
    rigidBody->SetCollisionMask(LAYER(1)|LAYER(2)|LAYER(4));

    for (int c{ 0 }; c < 12; ++c)
        wallColliders_.Push(wallNode_->CreateComponent<CollisionShape>());
    for (int c{ 0 }; c < 8; ++c)
        goalColliders_.Push(wallNode_->CreateComponent<CollisionShape>());

    SetSize({ 750.f, 125.f, 225.0f }, 30.f);
}

void Arena::OnMarkedDirty(Node* node) {}
void Arena::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Arena::Draw(StringHash eventType, VariantMap& eventData)
{
    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };

    // Caps
    for (bool first: { true, false })
    {
        const PODVector<Vector3>& r{ hall_.faces_.At(first) };
        const int n{ 9 };

        for (int i{ 0 }; i < n; ++i)
        {
            const float alpha{ (i == 0 ? 1.f : 1.f / 3.f * (1.f * n - i) / n) };
            const Color color{    (first ? Color::CYAN   : Color::RED).Transparent(alpha) };
            const Vector3 offset{ (first ? Vector3::LEFT : Vector3::RIGHT) * goalSize_.x_ / 2.f * i };

            for (unsigned v{ 0 }; v < r.Size(); ++v)
            {
                const Vector3 from{ r.At(v)  };
                const Vector3 to{ r.At((v + 1) % r.Size()) };

                debug->AddLine(from + offset, to + offset, color, true);
            }
        }
    }

    // Goals
    const Color golor{ Color::YELLOW * .8f };
    for (bool mirror: { false, true })
    {
        const Vector3 goalCenter{ GetGoalCenter(mirror) };
        debug->AddBoundingBox(BoundingBox(goalCenter - goalSize_ * .5f,
                                          goalCenter + goalSize_ * .5f ),
                              golor, true);
    }


    // Floor grid
    Color grass{ Color::GREEN.Lerp(Color::YELLOW, .5f) };
    for (unsigned f{ 2u }; f < hall_.faces_.Size(); ++f)
    {
        const PODVector<Vector3> face{ hall_.faces_.At(f) };
        const Vector3 v0{ face.At(0) };
        const Vector3 v1{ face.At(1) };
        const Vector3 v2{ face.At(2) };
        const Vector3 v3{ face.At(3) };
        const IntVector2 n{ 24, f % 3 == 1 ? 8 : 3 };

        for (int i{ 0 }; i < n.x_ - 1; ++i)
        {
            debug->AddLine(v0.Lerp(v3, (i+1.f) / n.x_),
                           v1.Lerp(v2, (i+1.f) / n.x_),
                           grass * .5f, true);
        }

        for (int i{ 0 }; i < n.y_ + 1; ++i)
        {
            debug->AddLine(v0.Lerp(v1, (i*1.f) / n.y_),
                           v3.Lerp(v2, (i*1.f) / n.y_),
                           grass * .5f, true);
        }
    }

    // Spectators
    for (const Spectator& s: spectators_)
        debug->AddCross(s.position_, 1.f, TeamColor(TEAM_ALL).Transparent(1.f/3.f), true);

//    for (CollisionShape* collider: wallColliders_)
//        collider->DrawDebugGeometry(debug, false);
}

void Arena::SetSize(const Vector3& size, float wallAngle)
{
    if (size_ == size && wallAngle_ == wallAngle)
        return;

    size_ = VectorMax(goalSize_, size);;
    wallAngle_ = wallAngle;

    const Vector3 halfSize{ size_ * .5f };
    const float midWidth{ GetMidWidth() };


    const PODVector<Vector3> corner{ { halfSize.x_,        0.f , halfSize.z_ },
                                     { halfSize.x_, halfSize.y_, midWidth * .5f },
                                     { halfSize.x_,    size_.y_, halfSize.z_ } };

    PODVector<Vector3> ringA{ corner };
    for (unsigned v{ 0 }; v < corner.Size(); ++v)
        ringA += corner.At(corner.Size()-v-1u) * Vector3{ 1.f, 1.f, -1.f };

    PODVector<Vector3> ringB{ ringA };
    for (unsigned v{ 0 }; v < ringB.Size(); ++v)
        ringB.At(v) *= Vector3{ -1.f, 1.f, 1.f };

    hall_ = Polyhedron{{ ringA, ringB }};
    const unsigned sides{ ringA.Size() };
    for (unsigned f{ 0 }; f < sides; ++f)
        hall_.AddFace({ ringA.At(f),   ringA.At((f + 1) % sides),
                        ringB.At((f + 1) % sides), ringB.At(f) });

    const float depth{ 100.f };
    unsigned c{ 0u };

    for (unsigned f{ 0u }; f < hall_.faces_.Size(); ++f)
    {
        CollisionShape* collider{ wallColliders_.At(c) };
        const bool lid{ f < 2 };
        const bool wall{ (f - 1) % 3 != 0 };

        const PODVector<Vector3> face{ hall_.faces_.At(f) };
        const Vector3 centroid{ Average(face.Begin(), face.End()) };
        const Vector3 v0{ face.At(0) };
        const Vector3 v1{ face.At(1) };
        const Vector3 v2{ face.At(2) };
        const Vector3 v3{ face.At(3) };

        Vector3 normal{ (v1 - v0).CrossProduct(v2 - v0).Normalized() };
        if ((centroid - GetCenteroid()).DotProduct(normal) > 0.f)
            normal = -normal;

        if (lid)
        {
            const Vector3 position{ centroid - depth * normal * .5f };
            collider->SetBox({ depth, size.y_ + depth - goalSize_.y_, goalSize_.z_ + depth },
                             position + Vector3::UP * (goalSize_.y_ + depth) * .5f);

            for (int i{ 0 }; i < 2; ++i)
            {
                ++c;
                collider = wallColliders_.At(c);
                collider->SetBox({ depth, size.y_ + depth, (midWidth + depth - goalSize_.z_) * .5f },
                                 position + Vector3::FORWARD * (midWidth + goalSize_.z_ + depth) * (.25f - i * .5f));
            }
        }
        else
        {
//            Quaternion rotation;
//            rotation.FromLookRotation(Vector3::LEFT.CrossProduct(normal), normal);
//            collider->SetStaticPlane(centroid, rotation);

            const Vector3 position{ centroid - depth * normal * .5f };
            collider->SetBox({ size.x_ * 1.5f, depth, (wall ? halfSize.y_ / Cos(wallAngle_) : size.z_) + depth},
                             position,
                             Quaternion{ normal.Angle(Vector3::UP),
                                         centroid.z_ > 0.f ? Vector3::LEFT : Vector3::RIGHT }); // Cos I is n00b
        }

        ++c;
    }

    for (unsigned c{ 0 }; c < goalColliders_.Size(); ++c)
    {
        const bool cyans{ c >= 4 };
        const int n{ static_cast<int>(c % 4) };
        const Vector3 center{ GetGoalCenter(cyans) };

        const bool back{ n == 0 };
        const bool side{ n == 1 || n == 2 };
        const bool flat{ n == 3 };

        const float x{ (back ? center.x_ + goalSize_.x_ * (cyans ? -1.f : 1.f)
                             : center.x_ + (goalSize_.x_ + depth) * (cyans ? -.5f : .5f)) };
        const float y{ (flat ? center.y_ + goalSize_.y_
                             : center.y_) };
        const float z{ (side ? center.z_ + goalSize_.z_ * (n == 1 ? -1.f : 1.f)
                             : center.z_) };

        const float width{  (back ? goalSize_.x_ : (goalSize_.x_ * 2.f)) };
        const float height{ (flat ? goalSize_.y_ : (goalSize_.y_ * 3.f)) };
        const float depth{  (side ? goalSize_.z_ : (goalSize_.z_ * 3.f)) };

        goalColliders_.At(c)->SetBox({ width, height, depth }, { x, y, z });
    }

    RandomizeSpectators();
}

void Arena::UpdateSpectators(StringHash eventType, VariantMap& eventData)
{
    float t{ eventData[SceneUpdate::P_TIMESTEP].GetFloat() };
    for (Spectator& s: spectators_)
    {
        Ball* ball{ SPAWN->GetNearest<Ball>(s.position_) };
        if (!ball)
            return;

        const float zeal{ 2.f * Abs(s.bias_ - .5f) };
        const Vector3 ballPos{ ball->GetWorldPosition() };
        const Vector3  redGoal{ GetGoalCenter(false) };
        const Vector3 cyanGoal{ GetGoalCenter(true) };
        const float danger{ 1.f - Pow((s.bias_ < .5f ? redGoal : cyanGoal).DistanceToPoint(ballPos) / size_.x_, 2.f) };
        const Vector3  redPos{ SPAWN->AveragePosition(SPAWN->GetSaucers(TEAM_RED)) };
        const Vector3 cyanPos{ SPAWN->AveragePosition(SPAWN->GetSaucers(TEAM_CYANS)) };

        const Vector3 interest{ redPos.Lerp(cyanPos, s.bias_).Lerp(ballPos, .5f * (zeal + danger)) };
        const Pair<Vector3, Vector3> limit{ SpectatorLimits(s) };
        const Vector3 targetPos{ interest.ProjectOntoLine(limit.first_, limit.second_, true)};

        s.position_ = Lerp(s.position_, targetPos, zeal * zeal * t * (0.1f + zeal + ball->GetVelocity().LengthSquared() * 1e-5f));
    }
}

void Arena::RandomizeSpectators()
{
    for (Spectator& s: spectators_)
    {
        s.position_ = Vector3{
                size_.x_ * Random(-.5f, .5f),
                size_.y_ * .5f,
                (Random(2) - .5f) * GetMidWidth() };
        s.bias_ = Random();
    }
}
