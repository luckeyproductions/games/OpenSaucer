/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "saucer.h"
#include "player.h"

#include "jukebox.h"

JukeBox::JukeBox(Context* context): Object(context),
    musicNode_{ nullptr },
    tracks_{}
{
    Scene* musicScene{ new Scene{ context_ } };
    musicNode_ = musicScene->CreateChild("Music", LOCAL, true);

    SubscribeToEvent(E_MUSICFADED, DRY_HANDLER(JukeBox, HandleMusicFaded));
}

void JukeBox::Play(const String& song, bool loop, float gain)
{
    Stop(.05f);

    if (!song.IsEmpty())
    {
        SharedPtr<Sound> music{ RES(Sound, String{ "Music/" } + song) };
        if (!music)
            return;

        music->SetLooped(loop);
        SoundSource* musicSource{ musicNode_->CreateComponent<SoundSource>(LOCAL) };
        musicSource->SetSoundType(SOUND_MUSIC);
        SharedPtr<ValueAnimation> fade{ MakeShared<ValueAnimation>(context_) };
        fade->SetKeyFrame(0.f, 0.f);
        fade->SetKeyFrame(.2f + .3f * tracks_.IsEmpty(), gain);
        musicSource->SetGain(0.f);
        musicSource->SetAttributeAnimation("Gain", fade, WM_CLAMP);
        musicSource->Play(music);
        tracks_.Push(musicSource);
    }
}

void JukeBox::Stop(float fadeTime)
{
    for (SoundSource* source: tracks_)
    {
        if (fadeTime > 0.f)
        {
            SharedPtr<ValueAnimation> fade{ MakeShared<ValueAnimation>(context_) };
            fade->SetKeyFrame(0.f, { source->GetGain() });
            fade->SetKeyFrame(fadeTime, { 0.f });
            fade->SetEventFrame(fadeTime, E_MUSICFADED);
            source->SetAttributeAnimation("Gain", fade, WM_CLAMP);
        }
        else
        {
            source->Remove();
        }
    }
}

void JukeBox::HandleMusicFaded(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    SoundSource* source{ static_cast<SoundSource*>(GetEventSender()) };
    source->Remove();
    tracks_.Remove(source);
}

void JukeBox::PickSong()
{
    bool red{ false };
    bool cyans{ false };

    for (Player* p: MC->GetPlayers())
    {
        Saucer* saucer{ static_cast<Saucer*>(p->GetControllable()) };
        if (saucer)
        {
            switch (saucer->GetTeam())
            {
            case TEAM_RED:   red   = true; break;
            case TEAM_CYANS: cyans = true; break;
            default: break;
            }
        }
    }

    if ((!red && !cyans) || (red && cyans))
        Play(MUSIC_GREEN);
    else if (red)
        Play(MUSIC_RED);
    else
        Play(MUSIC_CYANS);
}
