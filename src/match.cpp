/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "spawnmaster.h"
#include "ball.h"
#include "saucer.h"
#include "game.h"

#include "match.h"

void Match::RegisterObject(Context* context)
{
    context->RegisterFactory<Match>();
    context->RegisterFactory<Replay>();

    DRY_ATTRIBUTE("TimeStamp", String, timeStamp_, "", AM_FILE);
    DRY_ATTRIBUTE("NumTeams", int, numTeams_, 2, AM_FILE);
    DRY_ATTRIBUTE("TeamSize", int, teamSize_, 3, AM_FILE);
    DRY_ATTRIBUTE("Duration", float, duration_, 0.f, AM_FILE);
    DRY_ATTRIBUTE("FastestBall", float, fastestBall_, 0.f, AM_FILE);

    context->NETWORK->RegisterRemoteEvent(E_MATCHJOINED);
    context->NETWORK->RegisterRemoteEvent(E_GOAL);
}

Match::Match(Context* context): Serializable(context),
    hosted_{ false },
    replayName_{},
    timeStamp_{},
    start_{},
    lastT_{ -M_INFINITY },
    duration_{},
    numTeams_{},
    teamSize_{},
    goalsRed_{},
    goalsCyans_{},
    fastestBall_{},
    trackBall_{},
    trackRed_{},
    trackCyans_{}
{
    ResetToDefault();
    SetAttribute("TeamSize", Random(1, 4));

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Match, HandlePostRenderUpdate));
}

Match::Match(Context* context, const VariantMap& info): Match(context)
{
    SetAttribute("TimeStamp", info[MatchJoined::P_TIMESTAMP]->GetString());
    VectorBuffer goals{ info[MatchJoined::P_GOALS]->GetBuffer() };
    while (!goals.IsEof())
    {
        float goal{ goals.ReadFloat() };
        if (goal < 0.f)
            goalsCyans_.Push(-goal);
        else
            goalsRed_.Push(goal);
    }

    SubscribeToEvent(E_GOAL, DRY_HANDLER(Match, HandleGoal));
}

void Match::Start()
{
    if (GetAttribute("TimeStamp").GetString().IsEmpty())
    {
        SetAttribute("TimeStamp", TIME->GetTimeStamp());
        start_ = TIME->GetElapsedTime();
        replayName_ = MC->GetArgRecord();

        if (!replayName_.IsEmpty())
        {
            SubscribeToEvent(E_PHYSICSPRESTEP, DRY_HANDLER(Match, HandleFixedUpdate));
            Log::Write(LOG_INFO, "Started recording replay");
        }
    }
}

void Match::End()
{
    Log::Write(LOG_INFO, "Match ended in " + String{ GetCyansScore() } + "-" + String{ GetRedScore() });

    SetAttribute("Duration", TIME->GetElapsedTime() - start_);
    UnsubscribeFromEvent(E_PHYSICSPRESTEP);
    SaveReplay();
}

void Match::SaveReplay()
{
    if (replayName_.IsEmpty())
        return;

    SharedPtr<JSONFile> jsonFile{ context_->CreateObject<JSONFile>() };
    JSONValue& val{ jsonFile->GetRoot() };
    if (SaveJSON(val))
    {
        FILES->SetCurrentDir(FILES->GetProgramDir());
        const String replaysFolder{ MC->GetReplaysPath() };

        if (!FILES->DirExists(GetParentPath(replaysFolder)))
            return;

        if (!FILES->DirExists(replaysFolder))
            FILES->CreateDir(replaysFolder);

        if (replayName_ == " ")
            replayName_ = String{ StringHash{ TIME->GetSystemTime() } };

        if (!replayName_.EndsWith(".json", false))
            replayName_.Append(".json");

        jsonFile->SaveFile(replaysFolder + replayName_);
        Log::Write(LOG_INFO, "Saved replay: " + (IsAbsolutePath(replaysFolder)
                                                 ? replaysFolder
                                                 : FILES->GetCurrentDir() + replaysFolder)
                   + replayName_);
    }
}

bool Match::SaveJSON(JSONValue& dest) const
{
    if (!Serializable::SaveJSON(dest))
        return false;

    JSONArray redGoals{};
    for (float rg: goalsRed_)
        redGoals.Push(rg);
    dest.Set("goals_red", redGoals);

    JSONArray cyansGoals{};
    for (float cg: goalsCyans_)
        cyansGoals.Push(cg);
    dest.Set("goals_cyans", cyansGoals);

    JSONValue ballValue{};
    JSONArray ballPos{};
    JSONArray ballRot{};

    for (const TrackPoint& ballPoint: trackBall_)
    {
        ballPos.Push(JSONValue{ JSONArray{ ballPoint.pos_.x_, ballPoint.pos_.y_, ballPoint.pos_.z_ } });
        ballRot.Push(JSONValue{ JSONArray{ ballPoint.rot_.x_, ballPoint.rot_.y_, ballPoint.rot_.z_, ballPoint.rot_.w_ } });
    }

    ballValue.Set("pos", ballPos);
    ballValue.Set("rot", ballRot);
    dest.Set("ball", ballValue);

    SaveTeamTrack(dest, TEAM_RED);
    SaveTeamTrack(dest, TEAM_CYANS);

    return true;
}

bool Match::LoadJSON(const JSONValue& source)
{
    if (!Serializable::LoadJSON(source))
        return false;

    if (duration_ == 0.f)
        return true;

    const JSONArray& goalsRed{ source.Get("goals_red").GetArray() };
    for (const JSONValue& val: goalsRed)
        goalsRed_.Push(val.GetFloat());

    const JSONArray& goalsCyans{ source.Get("goals_cyans").GetArray() };
    for (const JSONValue& val: goalsCyans)
        goalsCyans_.Push(val.GetFloat());

    const JSONValue& trackBall{ source.Get("ball") };
    const JSONArray& ballPosArray{ trackBall.Get("pos").GetArray() };
    const JSONArray& ballRotArray{ trackBall.Get("rot").GetArray() };

    const unsigned steps{ ballPosArray.Size() };
    assert(ballPosArray.Size() == steps &&
           ballRotArray.Size() == steps);
    const float dt{ duration_ / steps };

    auto vector3 = [&](const JSONValue& val) { const JSONArray& jVec{ val.GetArray() };
        return Vector3{ jVec.At(0).GetFloat(), jVec.At(1).GetFloat(), jVec.At(2).GetFloat() };
    };
    auto quaternion = [&](const JSONValue& val) { const JSONArray& jRot{ val.GetArray() };
        return Quaternion { jRot.At(0).GetFloat(), jRot.At(1).GetFloat(),
                            jRot.At(2).GetFloat(),
                            jRot.At(3).GetFloat() };
    };

    for (int s{ 0 }; s < teamSize_; ++s)
    {
        trackRed_[s] = {};
        trackCyans_[s] = {};

        const JSONValue& trackRed  { source.Get("red"   + String{ s }) };
        const JSONArray& redPosArray{ trackRed.Get("pos").GetArray() };
        const JSONArray& redRotArray{ trackRed.Get("rot").GetArray() };

        const JSONValue& trackCyans{ source.Get("cyans" + String{ s }) };
        const JSONArray& cyansPosArray{ trackCyans.Get("pos").GetArray() };
        const JSONArray& cyansRotArray{ trackCyans.Get("rot").GetArray() };

        for (unsigned i{ 0u }; i < steps; ++i)
        {
            trackRed_  [s].Push({ i * dt, vector3(  redPosArray.At(i)), quaternion(  redRotArray.At(i)) });
            trackCyans_[s].Push({ i * dt, vector3(cyansPosArray.At(i)), quaternion(cyansRotArray.At(i)) });
        }
    }

    for (unsigned i{ 0u }; i < steps; ++i)
    {
        trackBall_.Push({ i * dt, vector3(ballPosArray.At(i)), quaternion(ballRotArray.At(i)) });
    }

    return true;
}

void Match::SaveTeamTrack(JSONValue& dest, Team team) const
{
    bool cyans{ team == TEAM_CYANS };

    for (int i: (cyans ? trackCyans_.Keys() : trackRed_.Keys()))
    {
        const auto track{ *(cyans ? trackCyans_[i] : trackRed_[i]) };
        JSONValue value{};
        JSONArray pos{};
        JSONArray rot{};

        for (auto point: track)
        {
            pos.Push(JSONValue{ JSONArray{ point.pos_.x_, point.pos_.y_, point.pos_.z_ } });
            rot.Push(JSONValue{ JSONArray{ point.rot_.x_, point.rot_.y_, point.rot_.z_, point.rot_.w_ } });
        }

        value.Set("pos", pos);
        value.Set("rot", rot);
        dest.Set((cyans ? "cyans" : "red") + String{ i }, value);
    }
}

void Match::HandleFixedUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    const float t{ ElapsedTime() };
    if (t - lastT_ < 1/17.f)
        return;
    else
        lastT_ = t;

    Node* ballNode{ SPAWN->GetActiveNodes<Ball>().Front() };
    trackBall_.Push({ t, ballNode->GetWorldPosition(), ballNode->GetWorldRotation() });
    const float ballSpeed{ ballNode->GetComponent<RigidBody>()->GetLinearVelocity().Length() };
    if (ballSpeed > fastestBall_ && ballSpeed > 23.f)
        SetAttribute("FastestBall", ballSpeed);

    PODVector<Saucer*> redSaucers{ SPAWN->GetSaucers(TEAM_RED) };
    for (unsigned r{ 0u }; r < redSaucers.Size(); ++r)
    {
        Node* saucerNode{ redSaucers.At(r)->GetDiskNode() };
        trackRed_[r].Push({ t, saucerNode->GetWorldPosition(), saucerNode->GetWorldRotation() });
    }

    PODVector<Saucer*> cyansSaucers{ SPAWN->GetSaucers(TEAM_CYANS) };
    for (unsigned c{ 0u }; c < cyansSaucers.Size(); ++c)
    {
        Node* saucerNode{ cyansSaucers.At(c)->GetDiskNode() };
        trackCyans_[c].Push({ t, saucerNode->GetWorldPosition(), saucerNode->GetWorldRotation() });
    }
}

void Match::HandlePostRenderUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (GetSubsystem<Game>()->GetState() != Game::GS_REPLAY)
        return;

    DrawTrackPath(trackBall_, Color::YELLOW);

    for (const TrackPath& path: trackRed_.Values())
        DrawTrackPath(path, Color::RED);
    for (const TrackPath& path: trackCyans_.Values())
        DrawTrackPath(path, Color::CYAN);
}

void Match::DrawTrackPath(const TrackPath& path, const Color& color)
{
    DebugRenderer* debug{ MC->GetScene()->GetComponent<DebugRenderer>() };

    for (unsigned i{ 0u }; i < path.Size(); ++i)
    {
        const Vector3 pos{ path.At(i).pos_ };
        const Quaternion rot{ path.At(i).rot_ };

        if (i > 0u)
            debug->AddLine(path.At(i - 1).pos_, pos, color.Transparent(.23f));

        for (int j{ 0 }; j < 3; ++j)
        {
            switch (j) {
            case 0: debug->AddLine(pos, pos + rot * Vector3::RIGHT   * .5f, Color::RED);   break;
            case 1: debug->AddLine(pos, pos + rot * Vector3::UP      * .5f, Color::GREEN); break;
            case 2: debug->AddLine(pos, pos + rot * Vector3::FORWARD * .5f, Color::BLUE);  break;
            }
        }
    }
}

void Match::HandleGoal(StringHash eventType, VariantMap& eventData)
{
    const float time{ eventData[Goal::P_TIME].GetFloat() };
    if (eventData[Goal::P_TEAM].GetInt() == TEAM_CYANS)
        goalsCyans_.Push(time);
    else
        goalsRed_.Push(time);
}
