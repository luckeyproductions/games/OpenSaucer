/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SAUCER_H
#define SAUCER_H

#include "controllable.h"

struct Arc
{
    PODVector<Vector3> main_{};
    Vector<PODVector<Vector3>> branches_{};
};

class Saucer: public Controllable
{
    friend class SaucerBrain;

    DRY_OBJECT(Saucer, Controllable);

public:
    static void RegisterObject(Context* context);
    static void UpdateMetal(bool bloomEnabled);

    Saucer(Context* context);
    enum Actions{ THRUST = 0, JUMP, BEAM, SIGNAL };
    class ActionSet: public std::bitset<4>
    {
    public:
        ActionSet(bool thrust, bool jump, bool beam, bool signal): std::bitset<4>()
        {
            this->operator[](THRUST) = thrust;
            this->operator[](JUMP)   = jump;
            this->operator[](BEAM)   = beam;
            this->operator[](SIGNAL) = signal;
        }
    };

    void Start() override;
    void DelayedStart() override;

    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;

//    void Set(const Vector3& p) override { Set(p, TEAM_RED); }
    void Set(const Vector3& pos, Team team);
    void ClearControl() override;

    Node* GetDiskNode() const { return diskNode_; }
    Team GetTeam() const { return team_; }
    bool IsCyans() const{ return team_ == TEAM_CYANS; }
    Color GetColor() const { return TeamColor(GetTeam()); }

//    void SetThrust(bool thrust) { thrust_ = thrust; }
//    bool GetThrust() const { return thrust_; }

    Vector3 GetLinearVelocity() const { return sphereBody_->GetLinearVelocity(); }

    void Draw(StringHash eventType, VariantMap& eventData);

protected:
    void HandleAction(int actionId) override;
    void HandleReleaseAction(int actionId) override;

private:
    bool Jump();

    void DrawRingTrail();
    void DrawArc();
    void RecordTrail();
    void Bank();

    void HandleTeamSet();
    void HandleCollisionStart(StringHash eventType, VariantMap& eventData);
    void HandleRemoteCollision(StringHash eventType, VariantMap& eventData);
    void DebugDraw(StringHash eventType, VariantMap &eventData);

    static HashMap<int, SharedPtr<Material> > metal_;
    Team team_{ TEAM_ALL };
    bool thrust_;
    bool jump_;
    bool beam_;
    Vector3 aim_;

    SharedPtr<Material> glow_;
    RigidBody* sphereBody_;
    CollisionShape* sphereShape_;
    Node* diskNode_;
    RigidBody* diskBody_;

    StaticModel* saucerModel_;
    SoundSource3D* engineSource_;

    struct TrailSegment
    {
        TrailSegment Interpolate(const TrailSegment& rhs, float t = .5f) const
        {
            return { position_.Lerp(rhs.position_, t),
                     normal_.  Lerp(rhs.normal_,   t).Normalized(),
                     Lerp(spawned_, rhs.spawned_,  t) }; }
        Vector3 position_;
        Vector3 normal_;
        float spawned_;
    };

    Vector<TrailSegment> ringTrail_;
    Vector3 arc_;
    Arc lastArc_;
    void HandleRushChanged(StringHash eventType, VariantMap& eventData);
    void StartEngine();
};

DRY_EVENT(E_SAUCERCOLLISION, SaucerCollision)
{
    DRY_PARAM(P_IMPULSE, Impulse);  // float
    DRY_PARAM(P_NORMAL, Normal);    // Vector3
    DRY_PARAM(P_ARENA, Arena);      // bool
}

#endif // SAUCER_H
