/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "player.h"

#include "inputmaster.h"

using namespace LucKey;

InputMaster::InputMaster(Context* context): Object(context),
    pressedKeys_{},
    pressedJoystickButtons_{},
    axesPosition_{},
    controlledByPlayer_{},
    lastActions_{}
{
    boundKeysMaster_[KEY_UP]     = boundButtonsMaster_[static_cast<int>(CONTROLLER_BUTTON_DPAD_UP)]    = MasterInputAction::UP;
    boundKeysMaster_[KEY_DOWN]   = boundButtonsMaster_[static_cast<int>(CONTROLLER_BUTTON_DPAD_DOWN)]  = MasterInputAction::DOWN;
    boundKeysMaster_[KEY_LEFT]   = boundButtonsMaster_[static_cast<int>(CONTROLLER_BUTTON_DPAD_LEFT)]  = MasterInputAction::LEFT;
    boundKeysMaster_[KEY_RIGHT]  = boundButtonsMaster_[static_cast<int>(CONTROLLER_BUTTON_DPAD_RIGHT)] = MasterInputAction::RIGHT;
    boundKeysMaster_[KEY_RETURN] = boundButtonsMaster_[static_cast<int>(CONTROLLER_BUTTON_A)]          = MasterInputAction::CONFIRM;
    boundKeysMaster_[KEY_ESCAPE] = boundButtonsMaster_[static_cast<int>(CONTROLLER_BUTTON_B)]          = MasterInputAction::CANCEL;
    boundKeysMaster_[KEY_P]      = boundButtonsMaster_[static_cast<int>(CONTROLLER_BUTTON_START)]      = MasterInputAction::PAUSE;
    boundKeysMaster_[KEY_ESCAPE] = MasterInputAction::MENU;

    boundKeysPlayer_[1][KEY_W]     = PlayerInputAction::MOVE_FORWARD;
    boundKeysPlayer_[1][KEY_S]     = PlayerInputAction::MOVE_BACK;
    boundKeysPlayer_[1][KEY_A]     = PlayerInputAction::MOVE_LEFT;
    boundKeysPlayer_[1][KEY_D]     = PlayerInputAction::MOVE_RIGHT;
    boundKeysPlayer_[1][KEY_CAPSLOCK] = PlayerInputAction::ACTION_RUSH;
    boundKeysPlayer_[1][KEY_SHIFT] = PlayerInputAction::ACTION_THRUST;
    boundKeysPlayer_[1][KEY_SPACE] = PlayerInputAction::ACTION_JUMP;
    boundKeysPlayer_[1][KEY_ALT]   = PlayerInputAction::ACTION_BEAM;
    boundKeysPlayer_[1][KEY_CTRL]  = PlayerInputAction::ACTION_SIGNAL;
    boundKeysPlayer_[1][KEY_KP_8]  = PlayerInputAction::AIM_N;
    boundKeysPlayer_[1][KEY_KP_5]  = PlayerInputAction::AIM_S;
    boundKeysPlayer_[1][KEY_KP_2]  = PlayerInputAction::AIM_S;
    boundKeysPlayer_[1][KEY_KP_4]  = PlayerInputAction::AIM_W;
    boundKeysPlayer_[1][KEY_KP_6]  = PlayerInputAction::AIM_E;
    boundKeysPlayer_[1][KEY_KP_9]  = PlayerInputAction::AIM_NE;
    boundKeysPlayer_[1][KEY_KP_3]  = PlayerInputAction::AIM_SE;
    boundKeysPlayer_[1][KEY_KP_1]  = PlayerInputAction::AIM_SW;
    boundKeysPlayer_[1][KEY_KP_7]  = PlayerInputAction::AIM_NW;

    for (int p{ 1 }; p <= 16; ++p)
    {
        boundButtonsPlayer_[p][CONTROLLER_BUTTON_RIGHTSTICK] = PlayerInputAction::ACTION_RUSH;
        boundButtonsPlayer_[p][CONTROLLER_BUTTON_A] = PlayerInputAction::ACTION_THRUST;
        boundButtonsPlayer_[p][CONTROLLER_BUTTON_X] = PlayerInputAction::ACTION_JUMP;
        boundButtonsPlayer_[p][CONTROLLER_BUTTON_Y] = PlayerInputAction::ACTION_BEAM;
        boundButtonsPlayer_[p][CONTROLLER_BUTTON_B] = PlayerInputAction::ACTION_SIGNAL;
    }

    boundMouse_[MOUSEB_LEFT]   = PlayerInputAction::ACTION_THRUST;
    boundMouse_[MOUSEB_RIGHT]  = PlayerInputAction::ACTION_JUMP;
    boundMouse_[MOUSEB_MIDDLE] = PlayerInputAction::ACTION_SIGNAL;

    SubscribeToEvent(E_KEYDOWN,            DRY_HANDLER(InputMaster, HandleKeyDown));
    SubscribeToEvent(E_KEYUP,              DRY_HANDLER(InputMaster, HandleKeyUp));
    SubscribeToEvent(E_JOYSTICKBUTTONDOWN, DRY_HANDLER(InputMaster, HandleJoystickButtonDown));
    SubscribeToEvent(E_JOYSTICKBUTTONUP,   DRY_HANDLER(InputMaster, HandleJoystickButtonUp));
    SubscribeToEvent(E_JOYSTICKAXISMOVE,   DRY_HANDLER(InputMaster, HandleJoystickAxisMove));
    SubscribeToEvent(E_UPDATE,             DRY_HANDLER(InputMaster, HandleUpdate));
}

void InputMaster::HandleUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    InputActions activeActions{};
    for (Player* p: MC->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        Vector<PlayerInputAction> emptyActions{};
        activeActions.player_[pId] = emptyActions;
    }

    //Convert key presses to actions
    for (int key: pressedKeys_)
    {
        //Check for master key presses
        if (boundKeysMaster_.Contains(key))
        {
            MasterInputAction action{ boundKeysMaster_[key] };
            if (!activeActions.master_.Contains(action))
                activeActions.master_.Push(action);
        }
        //Check for player key presses
        for (Player* p: MC->GetPlayers())
        {
            int pId{ p->GetPlayerId() };
            if (boundKeysPlayer_[pId].Contains(key))
            {
                PlayerInputAction action{boundKeysPlayer_[pId][key]};
                if (!activeActions.player_[pId].Contains(action))
                    activeActions.player_[pId].Push(action);
            }
        }
    }
    //Check for joystick button presses
    for (Player* p: MC->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        for (int button: pressedJoystickButtons_[p->GetJoystickId()])
        {
            //Check for master button presses
            if (boundButtonsMaster_.Contains(button))
            {
                MasterInputAction action{ boundButtonsMaster_[button] };
                if (!activeActions.master_.Contains(action))
                    activeActions.master_.Push(action);
            }
            if (boundButtonsPlayer_[pId].Contains(button))
            {
                PlayerInputAction action{ boundButtonsPlayer_[pId][button] };

                if (p->GetRush())
                {
                    if (action == PlayerInputAction::ACTION_JUMP)
                        action = PlayerInputAction::ACTION_THRUST;
                    else if (action == PlayerInputAction::ACTION_THRUST)
                        action = PlayerInputAction::ACTION_JUMP;
                }

                if (!activeActions.player_[pId].Contains(action))
                    activeActions.player_[pId].Push(action);
            }
        }
    }

    for (int button: boundMouse_.Keys())
    {
        if (INPUT->GetMouseButtonDown(static_cast<MouseButton>(button)))
        {
            PlayerInputAction action{ boundMouse_[button] };
            if (!activeActions.player_[1].Contains(action))
                activeActions.player_[1].Push(action);
        }
    }

    //Handle the registered actions
    HandleActions(activeActions);
    lastActions_ = activeActions;
}

void InputMaster::HandleActions(const InputActions& actions)
{
    //Handle master actions
    for (MasterInputAction action: actions.master_)
    {
        switch (action){
        case MasterInputAction::UP:      break;
        case MasterInputAction::DOWN:    break;
        case MasterInputAction::LEFT:    break;
        case MasterInputAction::RIGHT:   break;
        case MasterInputAction::CONFIRM: break;
        case MasterInputAction::CANCEL:  break;
        case MasterInputAction::PAUSE:
        {
            if (!NETWORK->IsServerRunning() && !NETWORK->GetServerConnection())
                MC->TogglePaused();
        }
        break;
        case MasterInputAction::MENU:    break;
        default: break;
        }
    }

    //Handle player actions
    for (Player* p: MC->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        int jId{ p->GetJoystickId() };
        auto playerInputActions = actions.player_[pId];

        Controllable* controlled{ controlledByPlayer_[pId] };
        if (controlled)
        {
            if (playerInputActions->Contains(PlayerInputAction::ACTION_RUSH) &&
                !IsRepeating(pId, PlayerInputAction::ACTION_RUSH))
                p->ToggleRush();

            const Vector3 stickMove{ Vector3(axesPosition_[jId][0], 0.0f, -axesPosition_[jId][1]) };
            controlled->SetMove(GetMoveFromActions(playerInputActions) + stickMove * stickMove.LengthSquared());

            /// Move jib
//            const Vector3 stickAim{  Vector3(axesPosition_[jId][2], 0.0f, -axesPosition_[jId][3]) };
//            controlled->SetAim(GetAimFromActions(playerInputActions) + stickAim);

            std::bitset<4> restActions{};
            restActions[0] = playerInputActions->Contains(PlayerInputAction::ACTION_THRUST) ^ p->GetRush();
            restActions[1] = playerInputActions->Contains(PlayerInputAction::ACTION_JUMP);
            restActions[2] = playerInputActions->Contains(PlayerInputAction::ACTION_BEAM);
            restActions[3] = playerInputActions->Contains(PlayerInputAction::ACTION_SIGNAL);

            controlled->SetActions(restActions);
        }
    }
}

void InputMaster::Screenshot()
{
    Image screenshot(context_);
    GetSubsystem<Graphics>()->TakeScreenShot(screenshot);
    const String fileName{ GetSubsystem<FileSystem>()->GetProgramDir() + "Screenshots/Screenshot_" +
                Time::GetTimeStamp().Replaced(':', '_').Replaced('.', '_').Replaced(' ', '_') + ".webp" };

    (screenshot.SaveWEBP(fileName) ? Log::Write(LOG_INFO,  "Saved screenshot:" + fileName)
                                   : Log::Write(LOG_ERROR, "Failed to save screenshot"));
}

void InputMaster::HandleKeyDown(StringHash /*eventType*/, VariantMap& eventData)
{
    int key{ eventData[KeyDown::P_KEY].GetInt() };
    if (!pressedKeys_.Contains(key)) pressedKeys_.Push(key);

    switch (key){
//    case KEY_ESCAPE:{
//        MC->Exit();
//    } break;
    case KEY_9:{
        Screenshot();
    } break;
    default: break;
    }
}

void InputMaster::HandleKeyUp(StringHash eventType, VariantMap& eventData)
{ (void)eventType;

    int key{ eventData[KeyUp::P_KEY].GetInt() };
    if (pressedKeys_.Contains(key))
        pressedKeys_.Remove(key);
}

void InputMaster::HandleJoystickButtonDown(StringHash eventType, VariantMap& eventData)
{ (void)eventType;

    int joystickId{ eventData[JoystickButtonDown::P_JOYSTICKID].GetInt() };
    ControllerButton button{ static_cast<ControllerButton>(eventData[JoystickButtonDown::P_BUTTON].GetInt()) };
    if (!pressedJoystickButtons_[joystickId].Contains(button))
        pressedJoystickButtons_[joystickId].Push(button);
}

void InputMaster::HandleJoystickButtonUp(StringHash eventType, VariantMap& eventData)
{ (void)eventType;

    int joystickId{ eventData[JoystickButtonDown::P_JOYSTICKID].GetInt() };
    ControllerButton button{ static_cast<ControllerButton>(eventData[JoystickButtonUp::P_BUTTON].GetInt()) };
    if (pressedJoystickButtons_[joystickId].Contains(button))
        pressedJoystickButtons_[joystickId].Remove(button);
}

void InputMaster::HandleJoystickAxisMove(Dry::StringHash eventType, Dry::VariantMap& eventData)
{ (void)eventType;

    int joystickId{ eventData[JoystickAxisMove::P_JOYSTICKID].GetInt() };
    int axis{ eventData[JoystickAxisMove::P_AXIS].GetInt() };
    float position{ eventData[JoystickAxisMove::P_POSITION].GetFloat() };

    const float deadzone{ .1f };
    if (Abs(position) < deadzone)
        axesPosition_[joystickId][axis] = 0.f;
    else
        axesPosition_[joystickId][axis] = Clamp(PowN((1 / (1 - deadzone)) * Sign(position) * (Abs(position) - deadzone), 3), -1.f, 1.f);
}

Vector3 InputMaster::GetMoveFromActions(Vector<PlayerInputAction>* actions)
{
    return Vector3{Vector3::RIGHT *
                (actions->Contains(PlayerInputAction::MOVE_RIGHT) -
                 actions->Contains(PlayerInputAction::MOVE_LEFT))

                + Vector3::FORWARD *
                (actions->Contains(PlayerInputAction::MOVE_FORWARD) -
                 actions->Contains(PlayerInputAction::MOVE_BACK))};
}

Vector3 InputMaster::GetAimFromActions(Vector<PlayerInputAction>* actions)
{
    return Vector3{ Vector3::RIGHT *
                (actions->Contains(PlayerInputAction::AIM_E) -
                 actions->Contains(PlayerInputAction::AIM_W))

                +   Vector3::FORWARD *
                (actions->Contains(PlayerInputAction::AIM_N) -
                 actions->Contains(PlayerInputAction::AIM_S))
                + Quaternion(45.f, Vector3::UP) *
                (Vector3::RIGHT *
                 (actions->Contains(PlayerInputAction::AIM_SE) -
                  actions->Contains(PlayerInputAction::AIM_NW))

                 +   Vector3::FORWARD *
                 (actions->Contains(PlayerInputAction::AIM_NE) -
                  actions->Contains(PlayerInputAction::AIM_SW)))};
}

void InputMaster::SetPlayerControl(Player* player, Controllable* controllable)
{
    int playerId{ player->GetPlayerId() };
    if (player->IsLocal())
    {
        if (controlledByPlayer_.Contains(playerId))
        {
            if (controlledByPlayer_[playerId] == controllable)
                return;

            Controllable* lastControlled{ controlledByPlayer_[playerId] };
            controlledByPlayer_[playerId] = nullptr;
            if (lastControlled)
                lastControlled->ClearControl();
        }

        if (controllable)
        {
            controlledByPlayer_[playerId] = controllable;
            controllable->ClearControl();
        }
        else
        {
            controlledByPlayer_.Erase(playerId);
        }

        player->GetJib()->SetTarget(controllable);
    }
    else
    {
        Connection* client{ player->GetConnection() };
        if (client)
        {
            Controllable* lastControlled{ player->GetControllable() };
            player->SetRemoteControllable(controllable ? controllable->GetID() : 0u);

            if (lastControlled)
                lastControlled->ClearControl();
            if (controllable)
                controllable->ClearControl();


            VariantMap eventData{};
            eventData[SetControl::P_PLAYER] = playerId;
            eventData[SetControl::P_NODE]   = (controllable ? controllable->GetID() : 0u);
            client->SendRemoteEvent(E_SETCONTROL, true, eventData);
        }
    }
}

Player* InputMaster::GetPlayerByControllable(Controllable* controllable)
{
    for (int p: controlledByPlayer_.Keys())
    {
        if (controlledByPlayer_[p] == controllable)
            return MC->GetPlayer(p);
    }

    return nullptr;
}

Controllable* InputMaster::GetControllableByPlayer(const Player* player)
{
    const int id{ player->GetPlayerId() };

    if (controlledByPlayer_.Contains(id))
        return controlledByPlayer_[id];
    else if (!player->IsLocal())
        return player->GetControllable();

    return nullptr;
}
