/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GUI_H
#define GUI_H

#include "mastercontrol.h"


class GUI: public Object
{
    DRY_OBJECT(GUI, Object);

public:
    class Curve: TypedPolynomial<Vector2>
    {
        Matrix2 transform_;
        float start_{ 0.f };
        float end_{ 1.f }; // hypocycloid.SetSlope({from, to - from});

    };
    class Character: Vector<Curve>
    {

    };
    using Font = HashMap<StringHash, Character>;

    GUI(Context* context);

    Viewport* GetViewport() const { return RENDERER->GetViewportForScene(guiScene_, 0); }

private:
    void Draw(StringHash eventType, VariantMap& eventData);
    void HandleScreenModeChanged(StringHash eventType, VariantMap& eventData);
    void DrawScore();
    void DrawNumeral(const Vector2& position, int value, float glyphSize);

    void DrawPolynomial(const TypedPolynomial<Vector2>& polynomial, const Color& color, int segments, float start = 0.f, float end = 1.f);
    TypedPolynomial<Vector2> Hypocycloid(const Vector2& center, const Vector2& size, int cusps, Vector2 pointiness = Vector2::ONE);
    void DrawEllipse(const Vector2& center, const Vector2& size, const Color& color);
    void DrawVisor(Team team);

    int PlayerIdFromViewport() const
    {
        for (unsigned v{ 0u }; v < RENDERER->GetNumViewports(); ++v)
            if (RENDERER->GetViewport(v) == GetViewport())
                return v / 2 + 1;

        return 0;
    }

    SharedPtr<Scene> guiScene_;
    SharedPtr<Camera> guiCamera_;
    SharedPtr<DebugRenderer> guiRenderer_;
};

#endif // GUI_H
