/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include "mastercontrol.h"


class Settings: public Serializable
{
    DRY_OBJECT(Settings, Serializable);

public:
    Settings(Context* context);

    void OnSetAttribute(const AttributeInfo& attr, const Variant& src) override;
    void OnGetAttribute(const AttributeInfo& attr, Variant& dest) const override;
    const Vector<AttributeInfo>* GetAttributes() const override;
    const Vector<AttributeInfo>* GetNetworkAttributes() const override;
    bool Load(Deserializer& source) override;
    bool Save(Serializer& dest) const override;
    bool LoadXML(const XMLElement& source) override;
    bool SaveXML(XMLElement& dest) const override;
    bool LoadJSON(const JSONValue& source) override;
    bool SaveJSON(JSONValue& dest) const override;

    void ApplyAttributes()  override;
    bool SaveDefaultAttributes() const override;
    void MarkNetworkUpdate() override;

private:
    bool test_;
};

#endif // SETTINGS_H
