/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

#define MC GetSubsystem<MasterControl>()

namespace Dry {
class Node;
class Scene;
}

class Player;
class Match;

static const String SP_BLOOM        = "Bloom";
static const String SP_ANTIALIASING = "Anti-aliasing";
static const String SP_MUSIC        = "Music";
static const String SP_SAMPLES      = "Samples";
static const String SP_PLAYERS      = "NumPlayers";
static const String SP_TEAM         = "TeamSize";
static const String SP_RECORD       = "Record";
static const String SP_REPLAY       = "Replay";
static const String SP_HOST         = "Host";
static const String SP_JOIN         = "Join";

enum Team{ TEAM_RED = 00, TEAM_CYANS, TEAM_ALL};
static const char* teamNames[] = {  "Red", "Cyans", "All", nullptr };

DRY_EVENT(E_REQUESTSAUCERS, RequestSaucers)
{
    DRY_PARAM(P_IDS, IDs); // Player ID buffer
}

DRY_EVENT(E_SETCONTROL, SetControl)
{
    DRY_PARAM(P_PLAYER, Player);    // Local player ID
    DRY_PARAM(P_NODE,   Node);      // Controllable ID
}

static Color TeamColor(Team team)
{
    if (team == TEAM_RED)
        return Color::RED;
    if (team == TEAM_CYANS)
        return Color::CYAN;
    if (team == TEAM_ALL)
        return Color::CHARTREUSE;
    else
        return Color::WHITE;
}

class MasterControl: public Application
{
    DRY_OBJECT(MasterControl, Application);

public:
    MasterControl(Context* context);

    void Setup() override;
    void Start() override;
    void Stop() override;
    void Exit();

    String GetSettingsPath() const { return storagePath_ + "/Settings.json"; }
    String GetReplaysPath()  const { return storagePath_ + "/Replays/"; }

    void TogglePaused();

    Scene* GetScene() const { return scene_; }
    Player* AddPlayer(Connection* connection = nullptr, int id = 0);
    Player* GetPlayer(int playerId) const;
    Player* GetNearestPlayer(const Vector3& pos);
    Vector< SharedPtr<Player> > GetPlayers();
    Vector< SharedPtr<Player> > GetRemotePlayers(Connection* connection) const;
    Vector< SharedPtr<Player> > GetRemotePlayers() const;
    void RemovePlayers(Connection* connection);
    unsigned GetNumPlayers(bool includeRemote = false) const;
    void RemovePlayer(Player* player);
    void UpdateViewportRects();

    bool HasArgument(StringHash argName) { return arguments_.Contains(argName); }
    unsigned GetArgNumPlayers() const
    {
        if (arguments_.Contains(SP_PLAYERS))
            return arguments_[SP_PLAYERS]->GetUInt();
        else
            return 1;
    }

    String GetArgRecord()
    {
        // Burn filename after reading, but keep preference to record
        if (arguments_.Contains(SP_RECORD))
        {
            const String argRecord{ arguments_[SP_RECORD].GetString() };
            arguments_[SP_RECORD] = " ";
            return argRecord;
        }
        else
        {
            return "";
        }
    }

    unsigned short ServerPort() const
    {
        if (arguments_.Contains(SP_HOST))
            return arguments_[SP_HOST]->GetInt();
        else if (arguments_.Contains(SP_JOIN) && arguments_[SP_JOIN]->GetString().Contains(':'))
            return ToInt(arguments_[SP_JOIN]->GetString().Split(':').Back());

        return 2030u;
    }

private:
    void FindResourcePath();
    void ProcessArguments();
    void ConfigureDefaultZoneAndRenderPath();
    void CreateScene();
    void HandleServerConnection(StringHash eventType, VariantMap& eventData);
    void HandleSetControl(StringHash, VariantMap&);
    void DebugDraw(StringHash eventType, VariantMap& eventData);
    void HandleScreenMode(StringHash eventType, VariantMap& eventData);
    void AttemptSetControl(StringHash eventType, VariantMap& eventData);
    void SaveSettings() const;

    Team ToTeam(const String& argument)
    {
        if (argument.IsEmpty())
            return TEAM_ALL;

        if (IsDigit(argument.Front()))
            return static_cast<Team>(Clamp(ToInt(argument), 0, 2));
        else if (argument.StartsWith("R", false))
            return TEAM_RED;
        else if (argument.StartsWith("C", false) || argument.StartsWith("B", false))
            return TEAM_CYANS;
        else
            return TEAM_ALL;
    }

    String storagePath_;
    Scene* scene_;
    SharedPtr<Match> bootMatch_;
    VariantMap arguments_;
    Vector< SharedPtr<Player> > players_;
    HashMap< Connection*, Vector< SharedPtr<Player> > > remotePlayers_;
    Vector< Pair<int, unsigned> > controlQueue_;
};

#endif // MASTERCONTROL_H
