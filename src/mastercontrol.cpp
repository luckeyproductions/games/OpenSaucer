/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "inputmaster.h"
#include "settings.h"
#include "arena.h"
#include "ball.h"
#include "saucer.h"
#include "saucerbrain.h"
#include "jib.h"
#include "jukebox.h"
#include "player.h"
#include "spawnmaster.h"
#include "gui.h"
#include "game.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context* context): Application(context),
    storagePath_{},
    scene_{ nullptr },
    bootMatch_{ nullptr },
    arguments_{},
    players_{},
    remotePlayers_{},
    controlQueue_{}
{
}

void MasterControl::Setup()
{
//    for (AttributeInfo info: *context_->RegisterSubsystem<Settings>()->GetAttributes())
//        Log::Write(LOG_INFO, info.name_);

    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "saucer.log";
    engineParameters_[EP_WINDOW_TITLE] = "Open Saucer";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = true;
    engineParameters_[EP_WINDOW_RESIZABLE] = true;
//    engineParameters_[EP_BORDERLESS] = true;

//    engineParameters_[EP_FULL_SCREEN] = false;
//    engineParameters_[EP_WINDOW_WIDTH] = 600;
//    engineParameters_[EP_WINDOW_HEIGHT] = 720;

    Game::RegisterObject(context_);
    FindResourcePath();
    ProcessArguments();
}

void MasterControl::FindResourcePath()
{
    String resourcePaths{ "Resources" };
    if (!FILES->DirExists(AddTrailingSlash(FILES->GetProgramDir()) + resourcePaths))
    {
        const String installedResources{ RemoveTrailingSlash(RESOURCEPATH) };
        if (FILES->DirExists(installedResources))
            resourcePaths = installedResources;
    }

    if (resourcePaths == "Resources")
        storagePath_ = resourcePaths;
    else
        storagePath_ = RemoveTrailingSlash(FILES->GetAppPreferencesDir("luckey", "saucer"));

    engineParameters_[EP_RESOURCE_PATHS] = resourcePaths;
}

void MasterControl::ProcessArguments()
{
    const StringVector& args{ GetArguments() };
    int teamSize{};

    for (const String& arg: args)
    {
        if (GetExtension(arg) == ".json")
        {
            String replayName{ arg };
            if (!IsAbsolutePath(replayName))
                replayName = GetReplaysPath() + GetFileName(arg) + ".json";

            SharedPtr<JSONFile> replayFile{ RES(JSONFile, replayName) };
            SharedPtr<Match> match{ context_->CreateObject<Match>() };

            if (replayFile && match->LoadJSON(replayFile->GetRoot()))
                bootMatch_ = match;
        }
        else
        {
            const StringVector xSplit{ arg.ToUpper().Split('X', false) };

            // Resolution
            if (xSplit.Size() == 2u)
            {
                const int width{ ToInt(xSplit.Front()) };
                const int height{ ToInt(xSplit.Back()) };

                if (width > 0 && height > 0)
                {
                    engineParameters_[EP_WINDOW_WIDTH]  = width;
                    engineParameters_[EP_WINDOW_HEIGHT] = height;

                    const String heightString{ xSplit.Back() };
                    if (heightString.EndsWith("W", false))
                        engineParameters_[EP_FULL_SCREEN] = false;
                    if (heightString.Contains("B", false))
                        engineParameters_[EP_BORDERLESS] = true;
                }
            }
            // Arena size
            else if (xSplit.Size() == 3u)
            {
            }
            // Other parameters
            else
            {
                const StringVector equalsSplit{ arg.Split('=', false) };
                const bool equate{ equalsSplit.Size() == 2u };

                if (equate || arg.StartsWith("-"))
                {
                    String parameter{ (equate ? equalsSplit.Front() : arg) };
                    while (parameter.Front() == '-')
                        parameter = parameter.Substring(1u);
                    if (parameter.IsEmpty())
                        continue;

                    const int  valueInt{  (equate ? ToInt( equalsSplit.Back()) : 1) };
                    const bool valueBool{ (equate ? ToBool(equalsSplit.Back()) : false) };

                    if (IsDigit(parameter.Front()))
                    {
                        unsigned pId{ ToUInt(parameter) };
                        Team teamPreference{ ToTeam(equalsSplit.Back()) };
                        arguments_["Player" + String{ pId }] = teamPreference;
                    }
                    else if (parameter.StartsWith("T", false) && valueInt != 0)
                        arguments_[SP_TEAM] = teamSize = Abs(valueInt);
                    else if (parameter.StartsWith("P", false))
                        arguments_[SP_PLAYERS] = static_cast<unsigned>(Clamp(valueInt, 0, 16));
                    else if (parameter.StartsWith("R", false))
                        arguments_[SP_RECORD] = (equate ? equalsSplit.Back() : " ");
                    else if (parameter.StartsWith("A", false))
                        arguments_[SP_ANTIALIASING] = (equate ? valueBool : true);
                    else if (parameter.StartsWith("B", false))
                        arguments_[SP_BLOOM] = (equate ? valueBool : true);
                    else if (parameter.StartsWith("M", false))
                        arguments_[SP_MUSIC] = (equate ? valueBool : true);
                    else if (parameter.StartsWith("S", false))
                        arguments_[SP_SAMPLES] = (equate ? valueBool : true);
                    else if (parameter.StartsWith("H", false))
                        arguments_[SP_HOST] = (equate ? valueInt : ServerPort());
                    else if (parameter.StartsWith("J", false))
                        arguments_[SP_JOIN] = (equate ? equalsSplit.Back() : "localhost");
                }
            }
        }
    }

    if (bootMatch_) // Replay match
        return;

    bootMatch_ = context_->CreateObject<Match>();
    bootMatch_->SetHosted(arguments_.Contains(SP_HOST));

    if (HasArgument(SP_HOST) && !HasArgument(SP_JOIN))
    {
        engineParameters_[EP_HEADLESS] = true;

        if (arguments_.Contains(SP_PLAYERS))
            arguments_.Erase(SP_PLAYERS);
    }

    if (HasArgument(SP_PLAYERS))
    {
        if (teamSize == 0)
        {
            const unsigned p{ arguments_[SP_PLAYERS].GetUInt() };
            teamSize = Max(p / 2 + (p % 2), bootMatch_->GetTeamSize());
        }
    }

    if (teamSize != 0)
    {
        arguments_[SP_TEAM] = teamSize;
        bootMatch_->SetAttribute("TeamSize", { teamSize });
    }
}

void MasterControl::Start()
{
    context_->RegisterSubsystem(this);
    context_->RegisterSubsystem<JukeBox>();
    context_->RegisterSubsystem<SpawnMaster>();
    context_->RegisterSubsystem<InputMaster>();
    context_->RegisterSubsystem<Game>();

    Controllable::RegisterObject(context_);
    Saucer::RegisterObject(context_);
    SaucerBrain::RegisterObject(context_);
    Ball::RegisterObject(context_);
    context_->RegisterFactory<Arena>();
    context_->RegisterFactory<Jib>();
    context_->RegisterFactory<GUI>();
    context_->RegisterFactory<Metronome>();

    NETWORK->SetUpdateFps(42);
    NETWORK->RegisterRemoteEvent(E_SETCONTROL);
    NETWORK->RegisterRemoteEvent(E_REQUESTSAUCERS);

    if (GRAPHICS)
    {
        ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());
        INPUT->CenterMousePosition();
        ConfigureDefaultZoneAndRenderPath();
    }

    CreateScene();
    //    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(MasterControl, DebugDraw));
    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(MasterControl, HandleScreenMode));

    Game* game{ GetSubsystem<Game>() };
    if (bootMatch_ && (HasArgument(SP_HOST) || !HasArgument(SP_JOIN)))
    {
        if (bootMatch_->GetAttribute("Duration").IsZero())
            game->PrepareMatch(bootMatch_);
        else
            game->StartReplay(bootMatch_);
    }

    if (HasArgument(SP_JOIN) && !HasArgument(SP_HOST))
    {
        // Try join
        String address{ arguments_[SP_JOIN].GetString() };
        if (address.Contains(':')) // Assumes IPv4
            address = address.Split(':').Front();

        if (GetSubsystem<Network>()->Connect(address, ServerPort(), scene_))
        {
            Text* connectingText{ GetSubsystem<UI>()->GetRoot()->CreateChild<Text>() };
            connectingText->SetFont(RES(Font, "Fonts/Unitblock.ttf"));
            connectingText->SetColor(Color::CHARTREUSE);
            connectingText->SetFontSize(64);
            connectingText->SetAlignment(HA_CENTER, VA_CENTER);
            connectingText->SetTextAlignment(HA_CENTER);
            connectingText->SetText("Connecting to\n" + address + "\n at port \n" + String{ ServerPort() });
            SubscribeToEvent(E_CONNECTFAILED,   DRY_HANDLER(MasterControl, HandleServerConnection));
            SubscribeToEvent(E_SERVERCONNECTED, DRY_HANDLER(MasterControl, HandleServerConnection));
            SubscribeToEvent(E_SETCONTROL, DRY_HANDLER(MasterControl, HandleSetControl));

            return;
        }
    }
    else if (!bootMatch_)
    {
        game->PrepareMatch();
    }
}

void MasterControl::HandleServerConnection(StringHash eventType, VariantMap& eventData)
{
    Text* connectingText{ GetSubsystem<UI>()->GetRoot()->GetChildDynamicCast<Text>(0) };
    if (eventType == E_CONNECTFAILED)
        connectingText->SetText("Failed to connect");
    else
    {
        connectingText->Remove();
        // Request saucers for local players
        VectorBuffer players{};
        for (Player* p: GetPlayers())
            players.WriteInt(p->GetPlayerId());
        NETWORK->GetServerConnection()->
                SendRemoteEvent(E_REQUESTSAUCERS, false,
                                { { RequestSaucers::P_IDS, players } });
    }
}

void MasterControl::HandleSetControl(StringHash /*eventType*/, VariantMap& eventData)
{
    const unsigned nodeId{ eventData[SetControl::P_NODE].GetUInt() };
    const int playerId{ eventData[SetControl::P_PLAYER].GetInt() };
    Player* player{ GetPlayer(playerId) };

    for (const Pair<int, unsigned>& c: controlQueue_)
    {
        if (c.first_ == playerId || c.second_ == nodeId)
            controlQueue_.Remove(c);
    }

    if (player)
    {
        if (nodeId)
        {
            controlQueue_.Push({ playerId, nodeId });
            SubscribeToEvent(E_SCENEUPDATE, DRY_HANDLER(MasterControl, AttemptSetControl));
        }
        else
        {
            if (SPAWN->CountActive<Ball>())
            {
                Jib* jib{ player->GetJib() };
                IM->SetPlayerControl(player, nullptr);
                jib->SetTarget(SPAWN->GetActive<Ball>().Front());
            }
        }
    }
}

void MasterControl::AttemptSetControl(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (!NETWORK->GetServerConnection())
        controlQueue_.Clear();

    for (const Pair<int, unsigned>& c: controlQueue_)
    {
        Player* player{ GetPlayer(c.first_) };

        if (!player)
        {
            controlQueue_.Remove(c);
            continue;
        }

        Saucer* saucer{ dynamic_cast<Saucer*>(GetScene()->GetComponent(c.second_)) };
        if (saucer)
        {
            Jib* jib{ player->GetJib() };
            IM->SetPlayerControl(player, saucer->GetComponent<Saucer>());
            jib->SetTarget(saucer);

            controlQueue_.Remove(c);
        }
    }

    if (controlQueue_.IsEmpty())
    {
        UnsubscribeFromEvent(E_SCENEUPDATE);
        GetSubsystem<JukeBox>()->PickSong();
    }
}

void MasterControl::ConfigureDefaultZoneAndRenderPath()
{
    RENDERER->SetShadowMapSize(512);

    Zone* zone{ RENDERER->GetDefaultZone() };
    zone->SetAmbientColor(Color::WHITE * .23f);

    RenderPath* renderPath{ RENDERER->GetDefaultRenderPath() };
    renderPath->Append(RES(XMLFile, "PostProcess/FXAA3.xml"));
    renderPath->Append(RES(XMLFile, "PostProcess/BloomHDR.xml"));
    renderPath->SetShaderParameter("BloomHDRThreshold", .34f);
    renderPath->SetShaderParameter("BloomHDRMix", Vector2{ .9f, .42f, });
    renderPath->SetShaderParameter("BloomHDRBlurRadius", 23.f);
    renderPath->SetShaderParameter("BloomHDRBlurSigma", 5.f);

    FileSystem* fs{ GetSubsystem<FileSystem>() };
    SharedPtr<JSONFile> settingsFile{ context_->CreateObject<JSONFile>() };
    const String settingsPath{ GetSettingsPath() };
    if (fs->FileExists(settingsPath) && settingsFile->LoadFile(settingsPath))
    {
        const JSONValue& root{ settingsFile->GetRoot() };

        bool antiAliasing{ root.Get("Anti-aliasing").GetBool(true) };
        bool bloom{ root.Get("Bloom").GetBool(true) };

        renderPath->SetEnabled("FXAA3", antiAliasing);
        renderPath->SetEnabled("BloomHDR", bloom);
    }
    else
    {
        renderPath->SetEnabled("FXAA3", true);
        renderPath->SetEnabled("BloomHDR", true);
    }

    if (arguments_.Contains(SP_ANTIALIASING))
        renderPath->SetEnabled("FXAA3", arguments_[SP_ANTIALIASING].GetBool());
    if (arguments_.Contains(SP_BLOOM))
        renderPath->SetEnabled("BloomHDR", arguments_[SP_BLOOM].GetBool());

    const bool bloomEnabled{ renderPath->IsEnabled("BloomHDR") };
    Saucer::UpdateMetal(bloomEnabled);
    for (Ball* ball: SPAWN->GetAll<Ball>())
    {
        SharedPtr<Material> ballMaterial{ ball->GetComponent<StaticModel>()->GetMaterial(0u) };
        if (bloomEnabled)
        {
            ballMaterial->SetShaderParameter("MatDiffColor",     Color{ .7f, .7f, .7f, 1.f });
            ballMaterial->SetShaderParameter("MatSpecColor",     Color{ .09f, .1f, .1f, 23.f });
            ballMaterial->SetShaderParameter("MatEmissiveColor", Color{ .025f, .025f, .025f, 1.f });
        }
        else
        {
            ballMaterial->SetShaderParameter("MatDiffColor",     Color{ .9f, .9f, .9f, 1.f });
            ballMaterial->SetShaderParameter("MatSpecColor",     Color{ .8f, 1.f, 1.f, 23.f });
            ballMaterial->SetShaderParameter("MatEmissiveColor", Color{ .0f, .0f, .0f, 0.f });
        }
    }
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>(LOCAL);
    PhysicsWorld* physics{ scene_->CreateComponent<PhysicsWorld>(LOCAL) };
    physics->SetGravity(200/3.f * Vector3::DOWN);
    physics->SetSplitImpulse(true);
    physics->SetFps(90);
    scene_->SetTimeScale(Sqrt(physics->GetFps() / 70.f));

    scene_->CreateComponent<DebugRenderer>(LOCAL)->SetLineAntiAlias(true);

    //Lights
    Node* lightNode{ scene_->CreateChild("Light", LOCAL) };
    lightNode->SetPosition(Vector3::UP);
    lightNode->LookAt(Vector3::ZERO);
    Light* light{ lightNode->CreateComponent<Light>(LOCAL) };
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetRange(420.f);
    light->SetCastShadows(true);
    light->SetShadowIntensity(.17f);
    light->SetShadowResolution(1.f);
    light->SetShadowDistance(200.f);
    light->SetShadowCascade(CascadeParameters{ 20.f, 60.f, 140.f, 300.f, .5f });

    Node* fieldLightNode{ scene_->CreateChild("Light", LOCAL) };
    fieldLightNode->LookAt(Vector3::UP);
    Light* fieldLight{ fieldLightNode->CreateComponent<Light>(LOCAL) };
    fieldLight->SetLightType(LIGHT_DIRECTIONAL);
    fieldLight->SetRange(1.f);
    fieldLight->SetColor({ .6f, .7f, .5f });
    fieldLight->SetBrightness(.23f);

    // Arena & Ball
    if (!HasArgument(SP_JOIN) || HasArgument(SP_HOST))
    {
        scene_->CreateChild("Arena")->CreateComponent<Arena>();
        SPAWN->Create<Ball>();
    }

    // Local players and their cameras
    if (!ENGINE->IsHeadless())
    {
        const unsigned numPlayers{ (arguments_.Contains(SP_PLAYERS) ? arguments_[SP_PLAYERS].GetUInt() : Clamp(INPUT->GetNumJoysticks(), 1u, 4u)) };
        while (GetNumPlayers() < Max(1u, numPlayers))
        {
            Jib* jib{ scene_->CreateChild("Jib", LOCAL)->CreateComponent<Jib>() };
            AddPlayer()->SetJib(jib);
        }

        UpdateViewportRects();

        AUDIO->SetMasterGain(SOUND_MASTER, 1.f);
        AUDIO->SetMasterGain(SOUND_MUSIC,  (arguments_.Contains(SP_MUSIC)   ? arguments_[SP_MUSIC]  .GetBool() : 1.f) * .23f);
        AUDIO->SetMasterGain(SOUND_EFFECT, (arguments_.Contains(SP_SAMPLES) ? arguments_[SP_SAMPLES].GetBool() : 1.f));
    }
}

void MasterControl::DebugDraw(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };
    GetScene()->GetComponent<PhysicsWorld>()->DrawDebugGeometry(debug, false);
}

void MasterControl::Stop()
{
    Match* match{ GetSubsystem<Game>()->GetCurrentMatch() };
    if (match)
        match->End();

    if (NETWORK->GetServerConnection())
        NETWORK->Disconnect();
    else if (NETWORK->IsServerRunning())
        NETWORK->StopServer();

//    SaveSettings();

//    engine_->DumpResources(true);
//    engine_->DumpResources(false);
}

void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::UpdateViewportRects()
{
    const IntVector2 windowSize{ GRAPHICS->GetSize() };
    const int nPlayers{ static_cast<int>(GetNumPlayers()) };

    int xSectors{ 1 };
    int ySectors{ 1 };
    if (windowSize.x_ > windowSize.y_)
    {
        xSectors = CeilToInt(Sqrt(static_cast<float>(GetNumPlayers())));
        ySectors = nPlayers / xSectors + (nPlayers % xSectors != 0);
    }
    else
    {
        ySectors = CeilToInt(Sqrt(static_cast<float>(GetNumPlayers())));
        xSectors = nPlayers / ySectors + (nPlayers % ySectors != 0);
    }

    const int viewportsPerPlayer{ 2u };
    const IntVector2 screenCut{  windowSize / IntVector2{ xSectors, ySectors } };

    for (int v{ 0u }; v < static_cast<int>(RENDERER->GetNumViewports()); ++v)
    {
        Viewport* viewport{ RENDERER->GetViewport(v) };

        const int righth{ ((v / viewportsPerPlayer) % xSectors) };
        const int bottomth{ (v / (xSectors * viewportsPerPlayer)) };
        const int right{  screenCut.x_ * righth };
        const int bottom{ screenCut.y_ * bottomth };

        viewport->SetRect({ right, bottom, screenCut.x_ + right, screenCut.y_ + bottom });

        /// Split screen patchup
//        if (v % viewportsPerPlayer == 0u)
//        {
//            Camera* camera{ viewport->GetCamera() };
//            camera->SetProjectionOffset({ -righth + xSectors / 2.f - .5f, bottomth - ySectors / 2.f + .5f });
//            camera->SetFov(camera->GetFov() / PowN(ySectors, 4));
//        }
    }
}

Player* MasterControl::AddPlayer(Connection* connection, int id)
{
    if (connection)
        assert(id > 0);
    else
        id = static_cast<int>(players_.Size() + 1);

    SharedPtr<Player> player{ MakeShared<Player>(id, context_) };

    if (!connection)
    {
        players_.Push(player);
        if (arguments_.Contains("Player0"))
        {
            player->SetTeamPreference(static_cast<Team>(arguments_["Player0"].GetUInt()));
        }
        else
        {
            const String teamKey{ "Player" + String{ id } };
            if (arguments_.Contains(teamKey))
                player->SetTeamPreference(static_cast<Team>(arguments_[teamKey].GetUInt()));
        }
    }
    else
    {
        if (!remotePlayers_.Contains(connection))
            remotePlayers_.Insert({ connection, { player } });
        else
            remotePlayers_[connection].Push(player);

        player->SetConnection(connection);
    }

    return player;
}

Vector<SharedPtr<Player> > MasterControl::GetPlayers()
{
    return players_;
}

Vector<SharedPtr<Player> > MasterControl::GetRemotePlayers(Connection* connection) const
{
    if (remotePlayers_.Contains(connection))
        return *remotePlayers_.operator[](connection);
    else
        return {};
}

Vector<SharedPtr<Player> > MasterControl::GetRemotePlayers() const
{
    Vector<SharedPtr<Player> > players{};
    for (const Vector<SharedPtr<Player> >& connectionPlayers: remotePlayers_.Values())
        players.Push(connectionPlayers);
    return players;
}

void MasterControl::RemovePlayers(Connection* connection)
{
    remotePlayers_.Erase(connection);
}

unsigned MasterControl::GetNumPlayers(bool includeRemote) const
{
    return (includeRemote ? players_.Size() + remotePlayers_.Values().Size()
                          : players_.Size());
}

Player* MasterControl::GetPlayer(int playerId) const
{
    for (Player* p: players_)
    {
        if (p->GetPlayerId() == playerId)
            return p;
    }

    return nullptr;
}

Player* MasterControl::GetNearestPlayer(const Vector3& pos)
{
    Player* nearest{};
    Vector3 nearestPos{};

    for (Player* p: players_)
    {
        const Vector3 otherPos{ p->GetControllable()->GetWorldPosition() };

        if (p->IsAlive() &&
            (!nearest || otherPos.DistanceToPoint(pos) <
                       nearestPos.DistanceToPoint(pos)))
        {
            nearest = p;
            nearestPos = otherPos;
        }
    }

    return nearest;
}

void MasterControl::TogglePaused()
{
    if (!IM->IsRepeating(MasterInputAction::PAUSE))
    {
        scene_->SetUpdateEnabled(!scene_->IsUpdateEnabled());
//        SharedPtr<ValueAnimation> timeWarp{ context_->CreateObject<ValueAnimation>() };
//        timeWarp->SetKeyFrame(0.f, Max(.5f, scene_->GetTimeScale()));
//        timeWarp->SetKeyFrame(.1f, (scene_->GetTimeScale() < .1f ? scene_->GetAttributeAnimation("Time Scale")->GetKeyFrames().Front().value_.GetFloat()
//                                                                  : 0.f));
//        scene_->SetAttributeAnimation("Time Scale", timeWarp, WM_CLAMP);
    }
}

void MasterControl::HandleScreenMode(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateViewportRects();
}


void MasterControl::SaveSettings() const
{
    SharedPtr<JSONFile> settingsFile{ context_->CreateObject<JSONFile>() };
    JSONValue& root{ settingsFile->GetRoot() };

    GetSubsystem<Settings>()->SaveJSON(root);

//    Graphics* graphics{ GetSubsystem<Graphics>() };
//    root.Set("Resolution", JSONArray{ graphics->GetWidth(), graphics->GetHeight() });
//    root.Set("Fullscreen", graphics->GetFullscreen());
//    root.Set("V-Sync",     graphics->GetVSync());

//    RenderPath* renderPath{ GetSubsystem<Renderer>()->GetDefaultRenderPath() };
//    root.Set("Anti-aliasing", renderPath->IsEnabled("FXAA3"));
//    root.Set("Bloom", renderPath->IsEnabled("BloomHDR"));

//    Audio* audio{ GetSubsystem<Audio>() };
//    root.Set("Music", audio->GetMasterGain(SOUND_MUSIC) != 0.f);

    settingsFile->SaveFile(GetSettingsPath());
}
