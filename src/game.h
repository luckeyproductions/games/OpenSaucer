/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GAME_H
#define GAME_H

#include "mastercontrol.h"

#include "ball.h"
#include "match.h"

class Game: public Object
{
    DRY_OBJECT(Game, Object);

public:
    enum State{ GS_MENU = 0, GS_PLAY, GS_REPLAY };

    static void RegisterObject(Context* context);

    Game(Context* context);

    State GetState() const { return gameState_; }

    void PrepareMatch(Match* match = nullptr);
    void StartReplay(Match* match);

    Match* GetCurrentMatch() const { return currentMatch_; }

    void Replay(float from = 0.f, float to = M_INFINITY);
    void InstantReplay(float duration);

private:
    void HandleClientConnected(StringHash eventType, VariantMap& eventData);
    void HandleRequestSaucers(StringHash eventType, VariantMap& eventData);
    void HandleClientDisconnected(StringHash eventType, VariantMap& eventData);
    void HandleMatchJoined(StringHash eventType, VariantMap& eventData);

    State gameState_;
    SharedPtr<Match> currentMatch_;
    SharedPtr<class Replay> replay_;
    void JoinMatch(SharedPtr<Match> match);
};

#endif // GAME_H
