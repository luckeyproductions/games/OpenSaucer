/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Dry/2D/Drawable2D.h"

#include "inputmaster.h"
#include "player.h"
#include "saucer.h"
#include "game.h"

#include "gui.h"

GUI::GUI(Context* context): Object(context),
    guiScene_{ new Scene{ context } },
    guiCamera_{   guiScene_->CreateChild("Camera")->CreateComponent<Camera>() },
    guiRenderer_{ guiScene_->CreateComponent<DebugRenderer>() }
{
    guiRenderer_->SetLineAntiAlias(true);
    guiScene_->CreateComponent<Octree>();

    SharedPtr<Viewport> viewport{ new Viewport{ context_, guiScene_, guiCamera_ } };
    SharedPtr<RenderPath> renderPath{ new RenderPath{} };
    renderPath->Load(RES(XMLFile, "RenderPaths/Forward.xml"));
    renderPath->RemoveCommand(0);
    viewport->SetRenderPath(renderPath);
    RENDERER->SetViewport(MC->GetNumPlayers() * 2 + 1, viewport);

    guiCamera_->GetNode()->SetWorldPosition(Vector3::BACK);
    guiCamera_->SetOrthographic(true);
    guiCamera_->SetOrthoSize(GRAPHICS->GetHeight());

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(GUI, Draw));
    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(GUI, HandleScreenModeChanged));
}

void GUI::Draw(StringHash eventType, VariantMap &eventData)
{
    if (MC->GetPlayers().Size())
    {
        Saucer* saucer{ dynamic_cast<Saucer*>(IM->GetControllableByPlayer(MC->GetPlayer(PlayerIdFromViewport()))) };
        if (saucer)
            DrawVisor(saucer->GetTeam());//Mod(guiScene_->GetElapsedTime(), 2.f) < 1.f);
        else
            DrawVisor(TEAM_ALL);
    }
    else
    {
        DrawVisor(TEAM_ALL);
    }

    DrawScore();

//    const Vector2 halfScreen{ GRAPHICS->GetSize() / 2.f };
//    guiRenderer_->AddTriangle(Vector3::ZERO, Vector3{-halfScreen.x_, halfScreen.y_, 0.f}, Vector3{halfScreen.x_, halfScreen.y_, 0.f}, Color::WHITE*.5f, true);
}

void GUI::HandleScreenModeChanged(StringHash eventType, VariantMap &eventData)
{
    guiCamera_->SetOrthoSize(GRAPHICS->GetHeight());
}

void GUI::DrawPolynomial(const TypedPolynomial<Vector2>& polynomial, const Color& color, int segments, float start, float end)
{
    const Vector2 halfScreen{ GRAPHICS->GetSize() * .5f };

    const float dt{ 1.f / segments };
    for (int i{ 0 }; i < segments; ++i)
    {
        const Vector2 from{ polynomial.Solve((i) * dt) };
        const Vector2 to{ polynomial.Solve((i + 1.f) * dt) };
        if (Abs(from.x_) > halfScreen.x_ && Abs(from.y_) > halfScreen.y_
         && Abs(to.x_)   > halfScreen.x_ && Abs(to.y_)   > halfScreen.y_)
            continue;

        guiRenderer_->AddLine(from, to, color, false);
    }
}

void GUI::DrawEllipse(const Vector2& center, const Vector2& size, const Color& color)
{
    TypedPolynomial<Vector2> ellipse{};
    ellipse.SetPolynomialType(0, PT_HARMONIC_SIN);
    ellipse.SetPolynomialType(1, PT_HARMONIC_COS);
    ellipse.SetCoefficient(0, center);
    ellipse.SetCoefficient(1, size * .5f);

    DrawPolynomial(ellipse, color, 100);
}

TypedPolynomial<Vector2> GUI::Hypocycloid(const Vector2& center, const Vector2& size, int cusps, Vector2 pointiness)
{
    const float a{ .5f * size.x_ };
    const float b{ a / cusps };

    TypedPolynomial<Vector2> hypocycloid{};
    hypocycloid.SetPolynomialType(0, PT_HARMONIC_SIN);
    hypocycloid.SetPolynomialType(1, PT_HARMONIC_COS);
    hypocycloid.SetCoefficient(0, center);
    hypocycloid.SetCoefficient(1,         0.5f  * Vector2{ a, size.y_ * .5f} * (a - b));
    hypocycloid.SetCoefficient(cusps - 1, 0.25f * Vector2{ -1.f * size.x_ * pointiness.y_, 1.f * size.y_ * pointiness.x_} * b);

    return hypocycloid;
}

void GUI::DrawVisor(Team team)
{
    const Vector2 center{};

    for (int i{ 0 }; i < (5 + static_cast<int>(team)); ++i)
    {
        Vector2 size{ Vector2{ GRAPHICS->GetSize() * .55f } };
        if (GetViewport())
        {
            float graphicsRatio{ 1.f * GRAPHICS->GetWidth() / GRAPHICS->GetHeight() };
            float viewportRatio{ 1.f * GetViewport()->GetRect().Width() / GetViewport()->GetRect().Height() };
            float ratioRatio{ graphicsRatio / viewportRatio };
            size.x_ /= ratioRatio;
        }

        Color color{ TeamColor(team) };
        float curvature;

        switch (team) {
        case TEAM_RED:
            size *= .95f + .05f * i;
            color = color.Transparent(.25f + .075f * i);
            curvature = -.1f;
        break;
        case TEAM_CYANS:
            size *= 1.05f + .075f * i;
            color = color.Transparent(.125f + .08f * i);
            curvature = .12f;
        break;
        case TEAM_ALL: default:
            size *= Vector2{ .875f + 0.07f * i, 1.1f + 0.05f * i };
            color = color.Transparent(.2f + .1f * i);
            curvature = .1f;
        break;
        }

        TypedPolynomial<Vector2> ellipse{};
        ellipse.SetPolynomialType(0, PT_HARMONIC_SIN);
        ellipse.SetPolynomialType(1, PT_HARMONIC_COS);
        ellipse.SetCoefficient(0, center);
        ellipse.SetCoefficient(1, size);
        ellipse.SetCoefficient(team == TEAM_ALL ? 4 : 5, (1.0f + 0.05f * Sin(70.f * TIME->GetElapsedTime() + i * 23.f)) * size * curvature);

        DrawPolynomial(ellipse, color, 256);
    }
}

void GUI::DrawScore()
{
    Match* match{ GetSubsystem<Game>()->GetCurrentMatch() };

    if (!match)
        return;

    const int red{ match->GetRedScore() };
    const int cyans{ match->GetCyansScore() };
    const float glyphSize{ GRAPHICS->GetSize().y_ * .125f };
    const Vector2 mid{ 0.f, glyphSize * 3.f };

    if (red == 0)
        DrawEllipse(mid + Vector2::RIGHT * glyphSize * 1.5f, { glyphSize * 2.f, glyphSize }, Color::RED.Transparent(.4f));
    else
        DrawNumeral(mid + Vector2::RIGHT * glyphSize, red, glyphSize);

    if (cyans == 0)
        DrawEllipse(mid + Vector2::LEFT * glyphSize * 1.5f, { glyphSize * 2.f, glyphSize }, Color::CYAN.Transparent(.4f));
    else
        DrawNumeral(mid + Vector2::LEFT * glyphSize, cyans, glyphSize);
}

void GUI::DrawNumeral(const Vector2& position, int value, float glyphSize)
{
    const int glyphs{ (value / 25) + 1 };

    for (int g{ 0 }; g < glyphs; ++g)
    {
        const float x0{ position.x_ + glyphSize * 1.1f * g * Sign(position.x_) };

        for (int b{ 0 }; b < 5; ++b)
        {
            const float y{ position.y_ + (b-2)*glyphSize/5 };
            const int rest{ value - g * 25 - b * 5 };

            if (rest >= 5)
            {
                DrawEllipse({ x0, y }, { glyphSize, glyphSize*.2f }, x0 > 0.f ? Color::RED : Color::CYAN);
            }
            else
            {
                for (int r{ 0 }; r < rest; ++r)
                {
                    const float x{ x0 + (r-rest/2)*glyphSize*.2f + ((rest+1)%2)*glyphSize*.1f};

                    DrawEllipse({ x, y }, { glyphSize*.2f, glyphSize*.2f }, x0 > 0.f ? Color::RED : Color::CYAN);
                }
            }
        }
    }

}
