/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PLAYER_H
#define PLAYER_H

#include "jib.h"
#include "gui.h"

#include "mastercontrol.h"

class Controllable;

class Player : public Object
{
    DRY_OBJECT(Player, Object);

public:
    Player(int playerId, Context* context);

    void SetJib(Jib* jib);

    Controllable* GetControllable() const;
    Jib* GetJib() const { return jib_; }

    int GetPlayerId() const { return playerId_; }
    int GetJoystickId() const { return playerId_ - 1 - (MC->GetNumPlayers() > INPUT->GetNumJoysticks()); }

    void SetTeamPreference(Team preference) { teamPreference_ = preference; }
    Team GetTeamPreference() const { return teamPreference_; }

    bool GetRush() const { return rush_; }
    void ToggleRush();
    void AddScore(int points);
    unsigned GetScore() const { return score_; }
    void Die();
    void Respawn();
    void ResetScore();

    void SetConnection(Connection* connection)
    {
        connection_ = connection;
        gui_ = nullptr;
    }
    void SetRemoteControllable(const unsigned id) { controllableId_ = id; }

    Connection* GetConnection() const { return connection_; }
    bool IsLocal() const noexcept { return connection_ == nullptr; }
    bool IsAlive() const noexcept { return alive_; }

private:
    int playerId_;
    bool alive_;
    bool rush_;

    unsigned score_;
    int multiplier_;

    void SetScore(int points);

    Jib* jib_;
    SharedPtr<GUI> gui_;
    Connection* connection_;
    Team teamPreference_;
    unsigned controllableId_;
};

DRY_EVENT(E_RUSHCHANGED, RushChanged) {}

#endif // PLAYER_H
