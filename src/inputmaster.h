/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef INPUTMASTER_H
#define INPUTMASTER_H

#include "mastercontrol.h"
#include "controllable.h"

#define IM GetSubsystem<InputMaster>()

enum class MasterInputAction { UP, RIGHT, DOWN, LEFT, CONFIRM, CANCEL, PAUSE, MENU, SCREENSHOT };
enum class PlayerInputAction { MOVE_FORWARD, MOVE_BACK, MOVE_LEFT, MOVE_RIGHT,
                               AIM_N, AIM_NE, AIM_E, AIM_SE, AIM_S, AIM_SW, AIM_W, AIM_NW,
                               ACTION_THRUST, ACTION_JUMP, ACTION_BEAM, ACTION_SIGNAL, ACTION_RUSH,
                               JIB_UP, JIB_DOWN, JIB_LEFT, JIB_RIGHT, JIB_STEPLEFT, JIB_STEPRIGHT };

struct InputActions {
    Vector<MasterInputAction> master_;
    HashMap<int, Vector<PlayerInputAction>> player_;
};

class Player;

class InputMaster: public Object
{
    DRY_OBJECT(InputMaster, Object);

public:
    InputMaster(Context* context);

    void SetPlayerControl(Player* player, Controllable* controllable);
    Player* GetPlayerByControllable(Controllable* controllable);
    Controllable* GetControllableByPlayer(const Player* player);
    bool IsRepeating(MasterInputAction action) const { return lastActions_.master_.Contains(action); }
    bool IsRepeating(int player, PlayerInputAction action) const { return lastActions_.player_[player]->Contains(action); }

private:
    HashMap<int, MasterInputAction> boundKeysMaster_;
    HashMap<int, MasterInputAction> boundButtonsMaster_;
    HashMap<int, HashMap<int, PlayerInputAction> > boundKeysPlayer_;
    HashMap<int, HashMap<int, PlayerInputAction> > boundButtonsPlayer_;
    HashMap<int, PlayerInputAction> boundMouse_;

    Vector<int> pressedKeys_;
    HashMap<int, Vector<ControllerButton> > pressedJoystickButtons_;
    HashMap<int, HashMap<int, float> > axesPosition_;

    HashMap<int, Controllable*> controlledByPlayer_;
    InputActions lastActions_;

    void HandleUpdate(              StringHash eventType, VariantMap& eventData);
    void HandleKeyDown(             StringHash eventType, VariantMap& eventData);
    void HandleKeyUp(               StringHash eventType, VariantMap& eventData);
    void HandleJoystickButtonDown(  StringHash eventType, VariantMap& eventData);
    void HandleJoystickButtonUp(    StringHash eventType, VariantMap& eventData);
    void HandleJoystickAxisMove(    StringHash eventType, VariantMap& eventData);

    void HandleActions(const InputActions& actions);
    void HandlePlayerAction(PlayerInputAction action, int playerId);
    Vector3 GetMoveFromActions(Vector<PlayerInputAction>* actions);
    Vector3 GetAimFromActions(Vector<PlayerInputAction>* actions);
    void Screenshot();

    void PauseButtonPressed();
};

#endif // INPUTMASTER_H
