/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MATCH_H
#define MATCH_H

#include "mastercontrol.h"


struct TrackPoint
{
    float t_;
    Vector3 pos_;
    Quaternion rot_;
};
using TrackPath = PODVector<TrackPoint>;

class Replay: public Component
{
    DRY_OBJECT(Replay, Component);

public:
    Replay(Context* context): Component(context),
        currentTime_{},
        motion_{ context->CreateObject<ObjectAnimation>() }
    {}


    void FromMatch(Match* match, float begin = 0.f, float end = M_INFINITY)
    {

    }

    void SetCurrentTime(float time) { currentTime_ = time; }

private:
    float currentTime_;
    SharedPtr<ObjectAnimation> motion_;
};


DRY_EVENT(E_MATCHJOINED, MatchJoined)
{
    DRY_PARAM(P_TIMESTAMP, TimeStamp); // String
    DRY_PARAM(P_GOALS, Goals); // VectorBuffer
}

DRY_EVENT(E_GOAL, Goal)
{
    DRY_PARAM(P_TEAM, Team); // int
    DRY_PARAM(P_TIME, Time); // float
}


class Match: public Serializable
{
    friend class Replay;
    DRY_OBJECT(Match, Serializable);

public:
    static void RegisterObject(Context* context);

    Match(Context* context);
    Match(Context* context, const VariantMap& info);;

    bool SaveJSON(JSONValue& dest) const override;
    bool LoadJSON(const JSONValue& source) override;

    void Start();
    void End();

    void SetHosted(bool hosted) { hosted_ = hosted; }
    bool IsHosted() const { return hosted_; }

    int GetTeamSize() const { return teamSize_; }
    int GetNumSaucers() const { return numTeams_ * teamSize_; }
    int GetRedScore()   { return goalsRed_  .Size(); }
    int GetCyansScore() { return goalsCyans_.Size(); }

    void RedScored()
    {
        goalsRed_.Push(ElapsedTime());

        if (NETWORK->IsServerRunning())
        {
            const VariantMap eventData{
                { Goal::P_TEAM, TEAM_RED },
                { Goal::P_TIME, goalsRed_.Back() } };
            NETWORK->BroadcastRemoteEvent(E_GOAL, false, eventData);
        }
    }

    void CyansScored()
    {
        goalsCyans_.Push(ElapsedTime());

        if (NETWORK->IsServerRunning())
        {
            const VariantMap eventData{
                { Goal::P_TEAM, TEAM_CYANS },
                { Goal::P_TIME, goalsCyans_.Back() } };
            NETWORK->BroadcastRemoteEvent(E_GOAL, false, eventData);
        }
    }

    void SaveReplay();

    VariantMap MatchInfo()
    {
        VectorBuffer goals{};
        for (const float& c: goalsCyans_)
            goals.WriteFloat(-c);
        for (const float& r: goalsRed_)
            goals.WriteFloat(r);

        VariantMap info{};
        info[MatchJoined::P_TIMESTAMP]  = timeStamp_;
        info[MatchJoined::P_GOALS]      = goals;

        return info;
    }

private:
    void SaveTeamTrack(JSONValue& dest, Team team) const;
    void HandleFixedUpdate(StringHash eventType, VariantMap& eventData);
    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);
    void DrawTrackPath(const TrackPath& path, const Color& color);

    void HandleGoal(StringHash eventType, VariantMap& eventData);

    float ElapsedTime() const { return TIME->GetElapsedTime() - start_; }

    bool hosted_;
    String replayName_;
    String timeStamp_;
    float start_;
    float lastT_;
    float duration_;

    int numTeams_;
    int teamSize_;

    PODVector<float> goalsRed_;
    PODVector<float> goalsCyans_;
    float fastestBall_;

    TrackPath trackBall_;
    HashMap<int, TrackPath> trackRed_;
    HashMap<int, TrackPath> trackCyans_;
};

#endif // MATCH_H
