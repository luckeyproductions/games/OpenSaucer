/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "arena.h"
#include "game.h"
#include "saucer.h"

#include "ball.h"

void Ball::RegisterObject(Context* context)
{
    context->RegisterFactory<Ball>();
    context->NETWORK->RegisterRemoteEvent(E_BALLCOLLISION);
}

Ball::Ball(Context* context): SceneObject(context),
    rigidBody_{},
    lastTouched_( TEAM_ALL, -1 ),
    trail_{},
    hit_{ nullptr },
    thud_{ nullptr }
{
    SetUpdateEventMask(USE_FIXEDUPDATE | USE_FIXEDPOSTUPDATE);
}

void Ball::Start()
{
    node_->SetName("Ball");
    node_->AddTag("Ball");
    node_->SetScale(5.f);

    rigidBody_ = node_->CreateComponent<RigidBody>(REPLICATED);
}

void Ball::DelayedStart()
{
    SceneObject::DelayedStart();

    hit_  = RES(Sound, "Samples/Hit.wav");
    thud_ = RES(Sound, "Samples/Thud.wav");

    if (!ENGINE->IsHeadless())
    {
        StaticModel* ballModel{ node_->CreateComponent<StaticModel>(LOCAL) };
        ballModel->SetModel(RES(Model, "Models/Triacontahedron.mdl"));
        ballModel->SetMaterial(RES(Material, "Materials/Ball.xml")->Clone());
        ballModel->SetCastShadows(true);
    }

    rigidBody_->SetCollisionLayer(LAYER(4));
    rigidBody_->SetCollisionMask(LAYER(1) | LAYER(3) | LAYER(4));
    rigidBody_->SetMass(1.f);
    rigidBody_->SetRestitution(1.f);
    rigidBody_->SetFriction(2/3.f);
    rigidBody_->SetRollingFriction(4/5.f);
    rigidBody_->SetLinearRestThreshold(.025f);
    rigidBody_->SetAngularRestThreshold(.025f);

    CollisionShape* collisionShape{ node_->CreateComponent<CollisionShape>(REPLICATED) };
    collisionShape->SetSphere(.975f);
    rigidBody_->SetCcdRadius(node_->GetScale().x_ * collisionShape->GetSize().x_ * .5f);
    rigidBody_->SetCcdMotionThreshold(.5f * rigidBody_->GetCcdRadius() * GetScene()->GetComponent<PhysicsWorld>()->GetFps());

    RibbonTrail* trail{ node_->CreateComponent<RibbonTrail>() };
    trail->SetMaterial(RES(Material, "Materials/Trail.xml"));
    trail->SetUpdateInvisible(true);
    trail->SetVertexDistance(1/4.f);
    trail->SetWidth(2.75f);
    trail->SetEndScale(2/3.f);
    trail->SetLifetime(.17f);
    trail->SetStartColor(Color::WHITE);
    trail->SetEndColor(Color::WHITE.Transparent());

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Ball, HandleCollisionStart));
    SubscribeToEvent(E_BALLCOLLISION, DRY_HANDLER(Ball, HandleRemoteCollision));
    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Ball, Draw));
}

void Ball::Reset()
{
    Set(BALL_SPAWNPOS);

    rigidBody_->ResetForces();
    rigidBody_->SetLinearVelocity(Vector3::ZERO);
    rigidBody_->SetAngularVelocity(Vector3::ZERO);
}

void Ball::Draw(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
   if (ENGINE->IsHeadless())
       return;

   DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };

   PhysicsRaycastResult result{ FloorCast() };
   debug->AddCircle(result.position_, result.normal_ * .01f, 5/3.f, Color::GREEN.Lerp(Color::YELLOW, .75f) * .5f, 64, true);

   Material* mat{ GetComponent<StaticModel>()->GetMaterial() };
   mat->SetShaderParameter("OutlineWidth", Cbrt(GetVelocity().Length() + rigidBody_->GetAngularVelocity().Length()) * 2.f * M_MIN_NEARCLIP + 3.f * M_MIN_NEARCLIP);

//   debug->AddSphere({ node_->GetWorldPosition(), rigidBody_->GetCcdMotionThreshold() }, Color::YELLOW, false);
}

void Ball::HandleCollisionStart(StringHash /*eventType*/, VariantMap& eventData)
{
    MemoryBuffer contacts{ eventData[NodeCollisionStart::P_CONTACTS].GetBuffer() };

    const Pair<float, Vector3> impulseNormal{ GetContactMaxImpulseAndNormal(contacts) };
    float impulse{ impulseNormal.first_ };

    if (impulse == 0.f)
        impulse = FakeImpulse(rigidBody_, static_cast<RigidBody*>(eventData[NodeCollisionStart::P_OTHERBODY].GetPtr()));

    const float hitGain{ HitGainFromImpulse(impulse) };

    if (hitGain > 0.f)
    {
        PlaySample(hit_, hitGain);

        if (node_ && NETWORK->IsServerRunning())
        {
            VariantMap eventData{ { BallCollision::P_IMPULSE, impulse } };
            NETWORK->BroadcastRemoteEvent(node_, E_BALLCOLLISION, false, eventData);
        }
    }
}

void Ball::HandleRemoteCollision(StringHash eventType, VariantMap& eventData)
{
    PlaySample(hit_, HitGainFromImpulse(eventData[BallCollision::P_IMPULSE].GetFloat()));
}

void Ball::FixedPostUpdate(float timeStep)
{
    // Server keeps the score
    if (NETWORK->GetServerConnection())
        return;

    const Vector3 v{ GetVelocity() };
    trail_.SetCoefficient(0, node_->GetWorldPosition());
    trail_.SetCoefficient(1, v);
    trail_.SetCoefficient(2, 200/3.f * Vector3::DOWN);

    const float xPos{ rigidBody_->GetPosition().x_ };

    if (Abs(xPos) > GetSubsystem<Arena>()->GetSize().x_ * .5f)
    {
        Game* game{ GetSubsystem<Game>() };

        if (Match* match{ game->GetCurrentMatch() })
        {
            if (xPos < 0.f)
                match->RedScored();
            else
                match->CyansScored();
        }

        Reset();
    }

    UpdateCcdMotionThreshold();
}

void Ball::UpdateCcdMotionThreshold()
{
    const int physicsFps{ GetScene()->GetComponent<PhysicsWorld>()->GetFps() };
    const Vector3 v{ GetVelocity() };
    float relativeMotion{ 0.f };
    PODVector<Saucer*> saucers{ SPAWN->GetActive<Saucer>() };
    for (Saucer* saucer: saucers)
    {
        const Vector3 rsv{ saucer->GetLinearVelocity() - v };
        const float saucerMotion{ rsv.Length() };
        const float distance{ saucer->GetWorldPosition().DistanceSquaredToPoint(node_->GetWorldPosition()) };

        if (saucerMotion / physicsFps < distance)
            continue;
        else if (saucerMotion > relativeMotion)
            relativeMotion = saucerMotion;
    }

    relativeMotion /= physicsFps;
    rigidBody_->SetCcdMotionThreshold(Max(M_EPSILON, 2.f * rigidBody_->GetCcdRadius() - relativeMotion));
}

void Ball::FixedUpdate(float timeStep)
{
    SceneObject::FixedUpdate(timeStep);

    const Vector3 v{ GetVelocity() };
    RibbonTrail* trail{ GetComponent<RibbonTrail>() };
    const float alpha{ Min(1.f, PowN(v.Length() * 1e-2f, 2)) };
    trail->SetStartColor(Color::WHITE.Transparent(Lerp(trail->GetStartColor().a_, alpha, timeStep * 5.f)));

    // Prevent lengthy top spins
    rigidBody_->SetAngularDamping(Max(.05f, 2.f/3.f - v.Length()));

    // Magnus effect
    const Vector3 a{ rigidBody_->GetAngularVelocity() };
    const Vector3 spinCross{ a.CrossProduct(v) * rigidBody_->GetRollingFriction() };
    rigidBody_->ApplyForce(spinCross * timeStep);
    rigidBody_->ApplyTorque(-a.Normalized() * spinCross.Length() * timeStep);
}
