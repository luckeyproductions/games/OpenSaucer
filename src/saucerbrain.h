/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SAUCERBRAIN_H
#define SAUCERBRAIN_H

#include "saucer.h"

enum Mindset{ MIND_SUPPORT = 0, MIND_DEFENCE, MIND_OFFENCE };


DRY_EVENT(E_TICK, MetronomeTick) {}
class Metronome: public Component, public Timer
{
    DRY_OBJECT(Metronome, Component)

public:

    Metronome(Context* context): Component(context), Timer(),
        interval_{},
        delay_{}
    {
        Reset();
        SubscribeToEvent(E_UPDATE, DRY_HANDLER(Metronome, Sway));
    }

    void SetInterval(float interval, float delay = 0.f)
    {
        interval_ = interval;
        delay_    = delay;
        Reset();
    }

    void SetTempo(float bpm, float delay = 0.f)
    {
        SetInterval(60.f / bpm, delay);
    }

    float GetSec(bool reset = false)
    {
        return 1e-3f * GetMSec(reset);
    }

    void Sway(StringHash, VariantMap&)
    {
        if (GetSec() < delay_)
            return;
        else
            delay_ = 0.f;

        if (GetSec() >= interval_)
        {
            SendEvent(E_TICK);
            Reset();
        }
    }

private:
    float interval_;
    float delay_;
};

class SaucerBrain: public Component
{
    DRY_OBJECT(SaucerBrain, Component);

public:
    static void RegisterObject(Context* context);

    SaucerBrain(Context* context);
    ~SaucerBrain();
    void Reset();

protected:
    void OnNodeSet(Node* node) override;
    void Think(StringHash, VariantMap&);

private:
    Saucer* saucer_;
    Mindset mindset_;
    float randomizer_;
};

#endif // SAUCERBRAIN_H
