/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "spawnmaster.h"

#define ATTENUATION 10.f, 500.f, 4/3.f

class SceneObject: public LogicComponent
{
    DRY_OBJECT(SceneObject, Object);

public:
    SceneObject(Context* context);

    void DelayedStart() override;
    virtual void Set(const Vector3& position);
    void FixedUpdate(float timeStep) override;

    virtual Vector3 GetWorldPosition() const { return node_->GetWorldPosition(); }
    float GetRandomizer() const { return randomizer_; }

    PhysicsRaycastResult FloorCast() const;
    SoundSource3D* PlaySample(Sound* sample, float gain = 0.3f, bool stay = true) const;

    Vector3 Gravity() const
    {
        PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
        RigidBody* rb{ GetComponent<RigidBody>() };
        if (rb)
        {
            const Vector3 grav{ rb->GetGravityOverride() };
            return (rb->GetUseGravity() ? (Equals(grav.LengthSquared(), 0.f)
                                           ? physics->GetGravity()
                                           : grav)
                                        : Vector3::ZERO);
        }

        return physics->GetGravity();
    }
    Vector3 GravUp() const { return -Gravity().NormalizedOrDefault(Vector3::DOWN); }

protected:
    void Disable();
    SoundSource3D* CreateSoundSource(Node* node) const;
    Pair<float, Vector3> GetContactMaxImpulseAndNormal(MemoryBuffer& contacts) const;
    float FakeImpulse(RigidBody* lhs, RigidBody* rhs);

    float randomizer_;
};

#endif // SCENEOBJECT_H

