HEADERS += \
    $$PWD/arena.h \
    $$PWD/ball.h \
    $$PWD/controllable.h \
    $$PWD/game.h \
    $$PWD/gui.h \
    $$PWD/inputmaster.h \
    $$PWD/jib.h \
    $$PWD/jukebox.h \
    $$PWD/luckey.h \
    $$PWD/mastercontrol.h \
    $$PWD/match.h \
    $$PWD/player.h \
    $$PWD/saucer.h \
    $$PWD/saucerbrain.h \
    $$PWD/sceneobject.h \
    $$PWD/settings.h \
    $$PWD/spawnmaster.h \

SOURCES += \
    $$PWD/arena.cpp \
    $$PWD/ball.cpp \
    $$PWD/controllable.cpp \
    $$PWD/game.cpp \
    $$PWD/gui.cpp \
    $$PWD/inputmaster.cpp \
    $$PWD/jib.cpp \
    $$PWD/jukebox.cpp \
    $$PWD/luckey.cpp \
    $$PWD/mastercontrol.cpp \
    $$PWD/match.cpp \
    $$PWD/player.cpp \
    $$PWD/saucer.cpp \
    $$PWD/saucerbrain.cpp \
    $$PWD/sceneobject.cpp \
    $$PWD/settings.cpp \
    $$PWD/spawnmaster.cpp \
