/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "settings.h"

Settings::Settings(Context* context): Serializable(context),
    test_{ false }
{
//    DRY_ATTRIBUTE("Bloom", bool, test_, true, AM_FILE);

    ResetToDefault();
}

bool Settings::SaveJSON(JSONValue& dest) const
{
    if (!Serializable::SaveJSON(dest))
        return false;

    return true;
}

bool Settings::LoadJSON(const JSONValue& source)
{
    return Serializable::LoadJSON(source);
}

void Settings::OnSetAttribute(const AttributeInfo& attr, const Variant& src) { Serializable::OnSetAttribute(attr, src); }
void Settings::OnGetAttribute(const AttributeInfo& attr, Variant& dest) const { Serializable::OnGetAttribute(attr, dest); }
const Vector<AttributeInfo>* Settings::GetAttributes() const { return Serializable::GetAttributes(); }
const Vector<AttributeInfo>* Settings::GetNetworkAttributes() const { return Serializable::GetNetworkAttributes(); }
bool Settings::Load(Deserializer& source) { return Serializable::Load(source); }
bool Settings::Save(Serializer& dest) const { return Serializable::Save(dest); }
bool Settings::LoadXML(const XMLElement& source) { return Serializable::LoadXML(source); }
bool Settings::SaveXML(XMLElement& dest) const { return Serializable::SaveXML(dest); }

void Settings::ApplyAttributes() {}
bool Settings::SaveDefaultAttributes() const { return false; }
void Settings::MarkNetworkUpdate() {}
