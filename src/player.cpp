/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "player.h"

#include "mastercontrol.h"
#include "inputmaster.h"

Player::Player(int playerId, Context* context): Object(context),
    playerId_{ playerId },
    alive_{ true },
    rush_{ false },
    score_{ 0 },
    multiplier_{ 1 },
    jib_{ nullptr },
    gui_{ (ENGINE->IsHeadless() ? nullptr : context_->CreateObject<GUI>()) },
    connection_{ nullptr },
    teamPreference_{ TEAM_ALL },
    controllableId_{ 0u }
{
}

void Player::SetJib(Jib* jib)
{
    jib_ = jib;
    jib_->SetPlayer(playerId_);

    if (GetControllable())
        jib_->SetTarget(GetControllable());
}

void Player::Die()
{
    alive_ = false;
}

void Player::Respawn()
{
    ResetScore();
    multiplier_ = 1;
    alive_ = true;
}

void Player::SetScore(int points)
{
    score_ = points;
}

void Player::ResetScore()
{
    SetScore(0);
}

void Player::AddScore(int points)
{
    score_ += points;
}

Controllable* Player::GetControllable() const
{
    if (IsLocal())
        return GetSubsystem<InputMaster>()->GetControllableByPlayer(this);
    else
        return dynamic_cast<Controllable*>(MC->GetScene()->GetComponent(controllableId_));
}

void Player::ToggleRush()
{
    rush_ = !rush_;
    SendEvent(E_RUSHCHANGED);
}
