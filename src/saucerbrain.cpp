/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "ball.h"
#include "arena.h"

#include "saucerbrain.h"

void SaucerBrain::RegisterObject(Context* context)
{
    context->RegisterFactory<SaucerBrain>();
    DRY_ATTRIBUTE("Randomizer", float, randomizer_, M_INFINITY, AM_NET);
}

SaucerBrain::SaucerBrain(Context* context): Component(context),
    saucer_{ nullptr },
    mindset_{},
    randomizer_{ M_INFINITY }
{
}

SaucerBrain::~SaucerBrain()
{
    if (node_ && node_->HasComponent<Metronome>())
        node_->RemoveComponent<Metronome>();
}

void SaucerBrain::OnNodeSet(Node* node)
{
    if (!node)
        return;

    saucer_ = node_->GetComponent<Saucer>();

    if (!saucer_)
        Remove();

    Reset();
}

void SaucerBrain::Reset()
{
    randomizer_ = Random();
    saucer_->ClearControl();

    node_->RemoveComponent<Metronome>();
    Metronome* metronome{ node_->CreateComponent<Metronome>(LOCAL) };
    metronome->SetTempo(123 + saucer_->GetRandomizer() * 50, saucer_->GetRandomizer());
    SubscribeToEvent(metronome, E_TICK, DRY_HANDLER(SaucerBrain, Think));

    MarkNetworkUpdate();
}

void SaucerBrain::Think(StringHash, VariantMap&)
{
    Arena* arena{ GetSubsystem<Arena>() };
    const Vector3 saucerPos{ saucer_->GetWorldPosition() };
    const Vector3 saucerVelocity{ saucer_->sphereBody_->GetLinearVelocity() };
    const Vector3 flatVelocity{ saucerVelocity.ProjectOntoPlane(Vector3::UP) };
    const Vector3 goal{ arena->GetGoalCenter(!saucer_->team_) };
    const Vector3 goalDirection{ (goal - node_->GetPosition()).ProjectOntoPlane(Vector3::UP).Normalized() };

    Ball* ball{ SPAWN->GetNearest<Ball>(saucerPos) };
    RigidBody* ballBody{ ball->GetComponent<RigidBody>() };
    const Vector3 ballPos{ ball->GetWorldPosition() };
    const Vector3 ballVelocity{ ballBody->GetLinearVelocity() };

    const Vector3 ballDelta{ (ballPos - saucerPos) + .23f * ballVelocity.Normalized().DotProduct(goalDirection) * ballVelocity.ProjectOntoPlane(flatVelocity) };
    const Vector3 ballDirection{ ballDelta.Normalized() };
    Vector3 ballSide{ flatVelocity.ProjectOntoPlane(ballDirection).Normalized() };
    ballSide *= -ballSide.DotProduct(goalDirection);
    const float ballDistance{ ballDelta.Length() };
    const float ballOut{ (Max(0.f, Abs(ballPos.z_) - 111.f) * .0125f ) };

    const float slow{ ballDirection.Angle(flatVelocity) * .1f };
    const float goalBall{ ballDirection.DotProduct(goalDirection) };

    IntVector3 roundPos{ VectorRoundToInt(node_->GetPosition()) / IntVector3{ 2, 1, 4 } };
    saucer_->SetMove((2.e-3f * (Vector3{ IntVector3{ 1, 0, 1 } - VectorMod(roundPos, IntVector3{ 2, 1, 2 }) })
                      + ballDirection * (.1f + Max(1.f, ballDistance) * .5f + goalBall)
                      + ballSide * Cbrt(1.f - goalBall)) * 17.f
                      - flatVelocity * .05f * slow * slow
                      - Vector3::BACK * ballOut * ballPos.z_);

    if (NETWORK->GetServerConnection())
        return;

    const Vector3 flatBallDelta{ ballDelta.ProjectOntoPlane(Vector3::UP) };
    const Vector3 diskUp{ saucer_->diskNode_->GetWorldUp() };
    const bool atBall{ ballDirection.DotProduct(diskUp) > .875f };
    const bool overhead{ ballDelta.y_ > 1.f && ballVelocity.y_ < -randomizer_ };
    const float goalDistanceFactor{ node_->GetWorldPosition().DistanceToPoint(goal) / arena->GetSize().x_ };
    const bool atGoal{ goalDirection.DotProduct(flatBallDelta.Normalized()) > M_1_SQRT2 * goalDistanceFactor };
    const bool atSide{ Abs(flatBallDelta.Normalized().DotProduct(Vector3::FORWARD)) > M_1_SQRT3 - .23f * (1.f - 2.f * PowN(goalDistanceFactor, 2)) };
    bool shoot{ ballDistance < 11.f && atBall && (atGoal || atSide || overhead) && goalBall > 0.f };
    bool rush{ flatBallDelta.Length() > 1.7f + 23.f * (goalDistanceFactor - PowN(goalBall, 3) * .55f) + 5.f * PowN(randomizer_, 2) };

    const bool thrust{ rush };
    const bool jump{   (shoot || (rush && diskUp.DotProduct(Vector3::UP) < .1f && ballDistance > 11.f - 5.f * goalBall)) && saucer_->actions_[Saucer::JUMP] == 0 };
    const bool beam{   false };
    const bool signal{ false };
    saucer_->SetActions(Saucer::ActionSet{ thrust, jump, beam, signal });
}
