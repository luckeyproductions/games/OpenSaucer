/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef JIB_H
#define JIB_H

#include "mastercontrol.h"

class Jib: public LogicComponent
{
    DRY_OBJECT(Jib, LogicComponent);

public:
    Jib(Context* context);

    void OnNodeSet(Node* node) override;
    void PostUpdate(float timeStep) override;

    Node* GetTarget() const { return target_; }
    void SetTarget(Node* node);
    void SetTarget(Component* component) { SetTarget((!component ? nullptr : component->GetNode())); }
    Camera* GetCamera() const { return camera_; }

    void Rotate(const Vector2& rotation);
    void SetPlayer(int id) { player_ = id; }

    void Shake(const Vector3& amplitude);
    
private:
    void LerpFov(float endFov, float duration = .5f);
    unsigned JoystickId() const;

    void HandleNetworkUpdate(StringHash, VariantMap&);
    void ShakeOff(StringHash eventType, VariantMap& eventData);

    Node* cameraNode_;
    Node* target_;
    Camera* camera_;

    float toYaw_;
    int player_;
    void TrackTarget(float timeStep);
};

DRY_EVENT(E_ENDSHAKE, EndShake) {}

#endif // JIB_H
