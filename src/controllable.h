/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CONTROLLABLE_H
#define CONTROLLABLE_H

#include <bitset>

#include "sceneobject.h"

class Controllable: public SceneObject
{
    DRY_OBJECT(Controllable, SceneObject);

public:
    static void RegisterObject(Context* context);
    Controllable(Context* context);

    void Start() override;
    void Update(float timeStep) override;

    void SetMove(const Vector3& move);
    Vector3 GetMove() const { return move_; }
    void SetActions(std::bitset<4> actions);
    virtual void ClearControl();
    Player* GetPlayer();

    void SetActions(unsigned actions)
    {
        std::bitset<4> bits{};
        for (unsigned a{ 0u }; a < actions_.size(); ++a)
            bits.set(a, actions & 1 << a);

        SetActions(bits);
    }

    unsigned GetActions() const
    {
        unsigned actions{};
        for (unsigned a{ 0u }; a < actions_.size(); ++a)
            actions += actions_[a] << a;
        return  actions;
    }
protected:
    virtual void HandleAction(int actionId);
    virtual void HandleReleaseAction(int actionId);

    Vector3 move_;
    std::bitset<4> actions_;
    HashMap<int, float> actionSince_;
    HashMap<int, float> actionInterval_;

private:
    void HandleClientSetMove(StringHash eventType, VariantMap& eventData);
    void HandleClientSetActions(StringHash eventType, VariantMap& eventData);

    float lastMoveUpdateJibYaw_;
};

DRY_EVENT(E_SETMOVE, SetMove) { DRY_PARAM(P_MOVE, Move); } // Vector3
DRY_EVENT(E_SETACTIONS, SetActions) { DRY_PARAM(P_ACTIONS, Actions); } // unsigned

#endif // CONTROLLABLE_H
