/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "sceneobject.h"
#include "arena.h"

SceneObject::SceneObject(Context* context): LogicComponent(context),
    randomizer_{ Random() }
{
}

void SceneObject::DelayedStart()
{
    if (NETWORK->GetServerConnection())
        SPAWN->Adopt(this);
}

void SceneObject::Set(const Vector3 &position)
{
    node_->SetWorldPosition(position);
    node_->SetEnabledRecursive(true);
}

void SceneObject::Disable()
{
    node_->SetEnabledRecursive(false);
}

SoundSource3D* SceneObject::PlaySample(Sound* sample, float gain, bool stay) const
{
    Node* node{ node_ };

    if (stay)
    {
        node = GetScene()->CreateChild("Sound");
        node->SetWorldPosition(node_->GetWorldPosition());
    }

    SoundSource3D* source{ CreateSoundSource(node) };
    source->SetGain(gain);
    source->Play(sample);

    return source;
}

SoundSource3D* SceneObject::CreateSoundSource(Node* node) const
{
    SoundSource3D* sampleSource{ node->CreateComponent<SoundSource3D>(LOCAL) };
    sampleSource->SetSoundType(SOUND_EFFECT);
    sampleSource->SetDistanceAttenuation(ATTENUATION);
    sampleSource->SetAutoRemoveMode(REMOVE_COMPONENT);

    return sampleSource;
}

Pair<float, Vector3> SceneObject::GetContactMaxImpulseAndNormal(MemoryBuffer& contacts) const
{
    float maxImpulse{};
    Vector3 normal{};

    while (!contacts.IsEof())
    {
        // Read position, normal and distance, then impulse.
        contacts.ReadVector3();
        const Vector3 n{ contacts.ReadVector3() };
        contacts.ReadFloat();
        const float contactImpulse{ contacts.ReadFloat() };

        if (contactImpulse > maxImpulse)
        {
            maxImpulse = contactImpulse;
            normal = n;
        }
    }

    return { maxImpulse, normal };
}

PhysicsRaycastResult SceneObject::FloorCast() const
{
    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
    PhysicsRaycastResult result{};
    Ray ray{ GetWorldPosition(), Gravity() };
    physics->RaycastSingle(result, ray, GetSubsystem<Arena>()->GetSize().y_, LAYER(1));
    return result;
}

void SceneObject::FixedUpdate(float timeStep)
{
    /// Gravity override. Doesn't work with angular factor.
    RigidBody* rb{ node_->GetComponent<RigidBody>() };
//    if (!rb)
        return;

    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
    const float strength{ physics->GetGravity().Length() };
    Arena* arena{ GetSubsystem<Arena>() };
    const Vector3 arenaCenter{ arena->GetCenteroid() };
//    const float effect{ Cbrt((node_->GetWorldPosition().y_ - arenaCenter.y_) / (arena->GetSize().y_ * -.5f))};
//    rb->SetGravityOverride(Vector3::DOWN * strength * effect);
    rb->SetGravityOverride((node_->GetWorldPosition() - arenaCenter).ProjectOntoPlane(Vector3::RIGHT).NormalizedOrDefault(Vector3::DOWN) * strength);
    rb->SetGravityOverride(-FloorCast().normal_ * strength);
}

float SceneObject::FakeImpulse(RigidBody* lhs, RigidBody* rhs)
{
    if (!lhs || !rhs)
        return 0.f;

    Node* node{ lhs->GetNode() };
    Node* otherNode{ rhs->GetNode() };
    const Vector3 deltaPos{ otherNode->GetWorldPosition() - node->GetWorldPosition() };
    const Vector3 velocity{ lhs->GetLinearVelocity() };
    const Vector3 otherVelocity{ rhs->GetLinearVelocity() };
    const float direct{ rhs->GetMass() == 0.f ? 1.f : Max(0.f, 1.f - velocity.Normalized().AbsDotProduct(deltaPos.Normalized())) };
    const float impulse{ Cbrt(direct * 9e3f * (velocity - otherVelocity).Length()) };

//    Log::Write(LOG_INFO, "Faked impulse " + node->GetName().ToLower() + " - " + otherNode->GetName().ToLower() + ": " + String{ impulse });

    return impulse;
}
