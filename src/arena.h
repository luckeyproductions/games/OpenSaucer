/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ARENA_H
#define ARENA_H

#include "luckey.h"


struct Spectator
{
    Vector3 position_;
    float bias_;
};

class Arena : public Component
{
    DRY_OBJECT(Arena, Component);

public:
    Arena(Context* context);
    void UpdateSpectators(StringHash eventType, VariantMap& eventData);

    void MarkNetworkUpdate() override;
    void Draw(StringHash eventType, VariantMap& eventData);

    void SetSize(const Vector3& size, float wallAngle = 0.f);
    Vector3 GetSize() const { return size_; }
    float GetMidWidth() const { return size_.y_ * Tan(wallAngle_) + size_.z_; }

    Vector3 GetCenteroid() const
    {
        return Vector3::UP * size_.y_ * .5f;
    }

    Vector3 GetGoalCenter(bool cyans) const
    {
        const Vector3 halfGoal{ goalSize_ * .5f };
        const Vector3 halfSize{ size_ * .5f };
        const Vector3 goalCenter{ Vector3{ (cyans ? -1.f : 1.f) * (halfSize.x_ + halfGoal.x_),
                                            halfGoal.y_,
                                            0.f } };
        return goalCenter;
    }

protected:
    void OnNodeSet(Node* scene) override;
    void OnMarkedDirty(Node* node) override;

private:
    void RandomizeSpectators();
    Pair<Vector3, Vector3> SpectatorLimits(const Spectator& s) const
    {
        const float extent{ size_.x_ * .5f };
        return { { -extent, size_.y_ * .5f, s.position_.z_ },
                 {  extent, size_.y_ * .5f, s.position_.z_ } };
    }

    Node* wallNode_;
    SharedPtr<Model> walls_;
    Vector<CollisionShape*> wallColliders_;
    Vector<CollisionShape*> goalColliders_;

    Vector3 size_;
    Vector3 goalSize_;
    float wallAngle_;
    Polyhedron hall_;

    Vector<Spectator> spectators_;
};

#endif // ARENA_H
