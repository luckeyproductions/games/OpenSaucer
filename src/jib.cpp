/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "saucer.h"
#include "ball.h"
#include "player.h"

#include "jib.h"


Jib::Jib(Context* context): LogicComponent(context),
    cameraNode_{ nullptr },
    target_{ nullptr },
    camera_{ nullptr },
    toYaw_{ 0.f },
    player_{ 0u }
{
}

void Jib::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->SetPosition(Vector3::UP * 5.f);
    cameraNode_ = node_->CreateChild("Camera", LOCAL);

    cameraNode_->SetPosition(Quaternion{ 33.f, Vector3::RIGHT} * Vector3::BACK * 23.f);
    cameraNode_->LookAt(node_->GetWorldPosition());
    camera_ = cameraNode_->CreateComponent<Camera>();
    camera_->SetFov(80.f);
    camera_->SetFov(180.f);
    LerpFov(80.f);

    SharedPtr<Viewport> viewport{ new Viewport{ context_, GetScene(), camera_ } };
    RENDERER->SetViewport(MC->GetNumPlayers() * 2, viewport);
    AUDIO->AddListener(cameraNode_->CreateComponent<SoundListener>());
}

void Jib::PostUpdate(float timeStep)
{
    if (!node_->GetScene())
        return;

    const bool keyboardPlayer{ player_ == 1u };
    Input* input{ GetSubsystem<Input>() };

    float zoom{ static_cast<float>(input->GetMouseMoveWheel()) };
    zoom += 2.3f * ((input->GetKeyDown(KEY_KP_PLUS) || input->GetKeyDown(KEY_EQUALS)) - (input->GetKeyDown(KEY_KP_MINUS) || input->GetKeyDown(KEY_MINUS)));

    if (!keyboardPlayer)
        zoom = 0;

    if (input->GetNumJoysticks() != 0)
    {
        if (JoystickState* joy{ input->GetJoystickByIndex(JoystickId()) })
            zoom += 5.f * PowN(joy->GetAxisPosition(CONTROLLER_AXIS_TRIGGERLEFT) - joy->GetAxisPosition(CONTROLLER_AXIS_TRIGGERRIGHT), 3.f);
    }

    LerpFov(Clamp(camera_->GetFov() - zoom * 2.3f, 55.f, 110.f));
    if (camera_->GetAttributeAnimation("FOV"))
    {
        Vector3 camPos{ cameraNode_->GetPosition() };
        float camDist{ Lerp(camPos.Length(), 17.f + 2.3f * Cbrt(camera_->GetFov() - 5.f), timeStep * 23.f) };
        cameraNode_->SetPosition(camPos.Normalized() * camDist);
    }

    if (target_)
        TrackTarget(timeStep);

    Vector2 move{ input->GetMouseMove() * 23.f };
    move += Vector2{ static_cast<float>(input->GetKeyDown(KEY_KP_6) - input->GetKeyDown(KEY_KP_4)),
                     static_cast<float>(input->GetKeyDown(KEY_KP_8) - input->GetKeyDown(KEY_KP_2)) }
            * 123.f;

    if (!keyboardPlayer)
        move = Vector2::ZERO;

    /// Should be InputMaster::PlayerInputAction that includes keys
    if (input->GetNumJoysticks() != 0)
    {
        if (JoystickState* joy{ input->GetJoystickByIndex(JoystickId()) })
        {
            move += Vector2{ PowN(joy->GetAxisPosition(CONTROLLER_AXIS_RIGHTX), 3.f),
                    PowN(joy->GetAxisPosition(CONTROLLER_AXIS_RIGHTY), 3.f) }
                    * 235.f;

            int view{ -1 };
            if (joy->GetButtonPress(CONTROLLER_BUTTON_DPAD_UP))
                view = 0;
            else if (joy->GetButtonPress(CONTROLLER_BUTTON_DPAD_DOWN))
                view = 2;
            else if (joy->GetButtonPress(CONTROLLER_BUTTON_DPAD_LEFT))
                view = 3;
            else if (joy->GetButtonPress(CONTROLLER_BUTTON_DPAD_RIGHT))
                view = 1;

            if (view >= 0)
            {
                Player* p{ MC->GetPlayer(player_) };
                if (Saucer* saucer{ static_cast<Saucer*>(p->GetControllable()) })
                {
                    Team team{ saucer->GetTeam() };
                    if (team != TEAM_ALL)
                        view += team == TEAM_CYANS ? 1 : 3;
                }

                const float aimYaw{ view * 90.f };
                float yawDelta{ Mod(aimYaw - node_->GetWorldRotation().YawAngle() + 360.f, 360.f) };
                if (yawDelta > 180.f)
                    yawDelta -= 360.f;

                toYaw_ = yawDelta;
            }
            else
            {
                const int direction{ (joy->GetButtonPress(CONTROLLER_BUTTON_RIGHTSHOULDER) - joy->GetButtonPress(CONTROLLER_BUTTON_LEFTSHOULDER)) };
                float nextHexant{ 60.f - Mod(node_->GetWorldRotation().YawAngle() + toYaw_ + 90.f, 60.f) };
                if (nextHexant < 5.f && direction > 0)
                    nextHexant += 60.f;
                else if (nextHexant > 55.f && direction < 0)
                    nextHexant -= 60.f;

                toYaw_ += direction >= 0 ? nextHexant * direction
                                         : (60.f - nextHexant) * direction;
            }
        }
    }

    Rotate(move * timeStep);
    const float restYaw{ toYaw_ * timeStep * 17.f };
    toYaw_ -= restYaw;
    Rotate({ restYaw, 0.f });

    if (keyboardPlayer && input->GetKeyPress(KEY_TAB))
    {
        PODVector<Node*> targets{ SPAWN->GetActiveNodes<Saucer>()
                                + SPAWN->GetActiveNodes<Ball>() };

        if (!targets.IsEmpty())
        {
            unsigned targetIndex{ M_MAX_UNSIGNED };
            if (target_ && targets.Contains(target_))
                targetIndex = targets.IndexOf(target_);

            if (input->GetQualifierDown(QUAL_SHIFT))
            {
                if (targetIndex == 0u || targetIndex == M_MAX_UNSIGNED)
                    targetIndex = targets.Size() - 1;
                else
                    --targetIndex;
            }
            else
            {
                targetIndex = ++targetIndex % targets.Size();
            }

            SetTarget(targets.At(targetIndex));
        }
    }
}

void Jib::SetTarget(Node* node)
{
    target_ = node;

    if (!target_)
        LerpFov(110.f);
    else if (target_->HasComponent<Ball>())
        LerpFov(100.f);
    else if (target_->HasComponent<Saucer>())
        LerpFov(80.f);
    else
        LerpFov(80.f);
}

void Jib::TrackTarget(float timeStep)
{
    const bool ballTarget{ target_->HasComponent<Ball>() };
    const Vector3 fovUp{ Vector3::UP * (Cbrt(camera_->GetFov() - 2.f) + 3.f)  };

    if (!ballTarget)
    {
        const Vector3 targetPos{ target_->GetWorldPosition() };
        node_->SetWorldPosition(targetPos + fovUp);
    }
    else
    {
        const PhysicsRaycastResult result{ target_->GetComponent<Ball>()->FloorCast() };
        Vector3 to{ result.position_ + fovUp * 2.3f };
        to.y_ = Lerp(node_->GetWorldPosition().y_, to.y_, Min(1.f, timeStep * 5.f));
        node_->SetWorldPosition(node_->GetWorldPosition().Lerp(to, Min(1.f, timeStep * 17.f)));
    }
}

void Jib::LerpFov(float endFov, float duration)
{
    ValueAnimation* fovAnim{ new ValueAnimation(context_) };
    fovAnim->SetKeyFrame(0.f, camera_->GetFov());
    fovAnim->SetKeyFrame(duration, endFov);

    const float l{ fovAnim->GetEndTime() };
    const float beginVal{ fovAnim->GetAnimationValue(0.f).GetFloat() };
    const float endVal{ fovAnim->GetAnimationValue(l).GetFloat() };

    if (!Equals(beginVal, endVal))
    {
        const int n{ 235 };
        const float dt{ l / n };
        for (int f{ 1 }; f < n; ++f)
        {
            const float t{ f * dt };
            const float tl{ 1.f - Pow(1.f - t / l, 5.f) };
            fovAnim->SetKeyFrame(t, Lerp(beginVal, endVal, tl));
        }

        camera_->SetAttributeAnimation("FOV", fovAnim, WM_ONCE);
    }
}

unsigned Jib::JoystickId() const
{
    Player* p{ MC->GetPlayer(player_) };
    const int id{ (p ? p->GetJoystickId() : M_MAX_INT) };
    return id < 0 ? M_MAX_UNSIGNED : id;
}

void Jib::Rotate(const Vector2& rotation)
{
    node_->Yaw(rotation.x_);
    cameraNode_->RotateAround(cameraNode_->WorldToLocal(node_->GetWorldPosition() - node_->GetWorldDirection() * 2.f), Quaternion{ .5f * rotation.y_, Vector3::RIGHT }, TS_LOCAL);
}

void Jib::Shake(const Vector3& amplitude)
{
    SharedPtr<ValueAnimation> shake{ context_->CreateObject<ValueAnimation>() };
    shake->SetInterpolationMethod(IM_SPLINE);
    const float dur{ amplitude.z_ };
    const float dt{ .023f };
    for (int i{ 0 }; i * dt < dur; ++i)
    {
        const float t{ i * dt };
        const float factor{ PowN(1.f - t / dur, 3) };
        const Vector2 offset{ amplitude.x_ * Sin(t * 360 * 7) * factor , amplitude.y_ * Sin(t * 360 * 11) * factor };
        shake->SetKeyFrame(i * dt, offset);
    }
    shake->SetEventFrame(dur, E_ENDSHAKE);
    SubscribeToEvent(E_ENDSHAKE, DRY_HANDLER(Jib, ShakeOff));

    camera_->SetAttributeAnimation("Projection Offset", shake, WM_ONCE);
}

void Jib::ShakeOff(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    camera_->RemoveAttributeAnimation("Projection Offset");
    camera_->SetProjectionOffset(Vector2::ZERO);
    UnsubscribeFromEvent(E_ENDSHAKE);
}
