/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "player.h"
#include "ball.h"
#include "arena.h"
#include "saucerbrain.h"

#include "saucer.h"

HashMap<int, SharedPtr<Material> > Saucer::metal_{};

void Saucer::RegisterObject(Context* context)
{
    context->RegisterFactory<Saucer>();

    DRY_COPY_BASE_ATTRIBUTES(Controllable);
    DRY_ENUM_ATTRIBUTE_EX("Team", team_, HandleTeamSet, teamNames, TEAM_ALL, AM_DEFAULT);

    for (int t{ 0 }; t <= TEAM_ALL; ++t)
    {
        metal_[t] = SharedPtr<Material>(context->RES(Material, "Materials/Metal.xml")->Clone());
        metal_[t]->SetShaderParameter("OutlineColor", TeamColor(static_cast<Team>(t)).Transparent(.23f));
    }

    context->NETWORK->RegisterRemoteEvent(E_SAUCERCOLLISION);
}

void Saucer::UpdateMetal(bool bloomEnabled)
{
    for (int t{ 0 }; t <= TEAM_ALL; ++t)
    {
        metal_[t]->SetShaderParameter("MatSpecColor", (bloomEnabled
                                                       ? Color{ .2f, .25f, .3f, 7.f }
                                                       : Color{ .8f, .95f, 1.f, 7.f }));
    }
}

Saucer::Saucer(Context* context): Controllable(context),
    thrust_{ false },
    jump_{ false },
    beam_{ false },
    aim_( Vector3::FORWARD ),
    sphereBody_{ nullptr },
    sphereShape_{ nullptr },
    diskNode_{},
    diskBody_{},
    engineSource_{},
    ringTrail_{},
    arc_{},
    lastArc_{}
{
    ResetToDefault();

    actionSince_[JUMP] = actionInterval_[JUMP] = 2/3.f;
    actionSince_[SIGNAL] = 1.f;

    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);
}

void Saucer::HandleTeamSet()
{
    if (node_)
    {
        saucerModel_->SetMaterial(0, metal_[static_cast<int>(team_)]);

        switch (team_)
        {
        case TEAM_RED:   glow_ = RES(Material, "Materials/Glow.xml"); break;
        case TEAM_CYANS: glow_ = RES(Material, "Materials/Cyans.xml"); break;
        default: case TEAM_ALL:
        {
            glow_ = RES(Material, "Materials/Glow.xml")->Clone();
            for (const MaterialShaderParameter& param: glow_->GetShaderParameters().Values())
            {
                if (param.name_.Contains("Color"))
                {
                    Color col{ GetColor() };
                    col.FromHSV(col.Hue() + Random(-.05f, .1f), col.SaturationHSV(), col.Value());
                    glow_->SetShaderParameter(param.name_, col);
                }
            }
        }
        break;
        }

        saucerModel_->SetMaterial(1, glow_);

        if (node_->HasComponent<Light>())
            node_->GetComponent<Light>()->SetColor(GetColor());
    }
}

void Saucer::Start()
{
    Controllable::Start();
    node_->SetName("Saucer");

    //Audio
    SharedPtr<Sound> sample{ RES(Sound, "Samples/Engine.wav") };
    sample->SetLooped(true);
    engineSource_ = CreateSoundSource(node_);
    engineSource_->SetGain(0.f);
    StartEngine();
    engineSource_->Play(sample);

    diskNode_ = GetScene()->CreateChild("Disk", REPLICATED);

    saucerModel_ = diskNode_->CreateComponent<StaticModel>(LOCAL);
    saucerModel_->SetModel(RES(Model, "Models/Saucer.mdl"));
    saucerModel_->SetMaterial(RES(Material, "Materials/Metal.xml"));
    saucerModel_->SetCastShadows(true);

    if (GetSubsystem<Network>()->GetServerConnection())
        return;

    sphereBody_  = node_->CreateComponent<RigidBody>(REPLICATED);
    sphereShape_ = node_->CreateComponent<CollisionShape>(REPLICATED);
}

void Saucer::StartEngine()
{
    engineSource_->SetFrequency(2.3e4f);
    SharedPtr<ValueAnimation> fadeIn{ context_->CreateObject<ValueAnimation>() };
    fadeIn->SetKeyFrame(.0f, .0f);
    fadeIn->SetKeyFrame(.1f, .075f);
    engineSource_->SetAttributeAnimation("Gain", fadeIn, WM_ONCE);
}

void Saucer::DelayedStart()
{
    SceneObject::DelayedStart();

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Saucer, Draw));
    SubscribeToEvent(node_, E_SAUCERCOLLISION, DRY_HANDLER(Saucer, HandleRemoteCollision));

    if (!ENGINE->IsHeadless())
    {
        RibbonTrail* trail{ node_->CreateComponent<RibbonTrail>(LOCAL) };
        trail->SetMaterial(RES(Material, "Materials/Thrust.xml"));
        trail->SetUpdateInvisible(true);
        trail->SetVertexDistance(.25f);
        trail->SetWidth(1.f);
        trail->SetEndScale(2/3.f);
        trail->SetLifetime(.17f);
        trail->SetStartColor(GetColor());
        trail->SetEndColor(GetColor().Transparent());

        Light* light{ node_->CreateComponent<Light>(LOCAL) };
        light->SetLightType(LIGHT_POINT);
        light->SetPerVertex(true);
        light->SetRange(23.f);
        light->SetBrightness(.23f);
        light->SetColor(GetColor());
    }

    if (GetSubsystem<Network>()->GetServerConnection())
    {
        sphereBody_ = node_->GetComponent<RigidBody>();
        diskBody_ = diskNode_->GetComponent<RigidBody>();
        return;
    }
    else
    {
        node_->Translate(Vector3::UP * 2.f);
        node_->LookAt(node_->GetWorldPosition() + aim_);

        diskNode_->SetWorldTransform(node_->GetWorldTransform());

        sphereBody_->SetAngularFactor(Vector3::UP);
        sphereBody_->SetMass(.8f);
        sphereBody_->SetFriction(0.f);
        sphereBody_->SetRollingFriction(0.f);
        sphereBody_->SetAngularDamping(2/3.f);
        sphereBody_->SetCollisionLayer(LAYER(2));
        sphereBody_->SetCollisionMask(LAYER(1) | LAYER(2));
        sphereBody_->SetRestitution(1e-2f);

        sphereShape_->SetSphere(4.875f);

        diskBody_ = diskNode_->CreateComponent<RigidBody>(REPLICATED);
        diskBody_->SetMass(4.2f);
        diskBody_->SetFriction(1/16.f);
        diskBody_->SetAngularDamping(.9f);
        diskBody_->SetRestitution(7/8.f);
        diskBody_->SetCollisionLayer(LAYER(3));
        diskBody_->SetCollisionMask(LAYER(4));

        const float coneHeight{ .75f };
        const float margin{ coneHeight * 3/4.f };
        for (bool top: { true, false })
        {
            CollisionShape* diskShape{ diskNode_->CreateComponent<CollisionShape>(REPLICATED) };
            const Vector3 pos{ (.5f * (coneHeight - margin)) * (top ? Vector3::UP : Vector3::DOWN) };
            const Quaternion rot{ 180.f * !top, 0.f, 0.f };
            diskShape->SetCone(5.f - margin * 2.f, coneHeight - margin, pos, rot);
            diskShape->SetMargin(margin);
        }

        SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Saucer, HandleCollisionStart));
        SubscribeToEvent(diskNode_, E_NODECOLLISIONSTART, DRY_HANDLER(Saucer, HandleCollisionStart));
    }

    Constraint* constraint{ node_->CreateComponent<Constraint>(LOCAL) };
    constraint->SetConstraintType(CONSTRAINT_POINT);
    constraint->SetOtherBody(diskBody_);
}

void Saucer::Set(const Vector3& pos, Team team)
{
    SceneObject::Set(pos);
    ClearControl();

    diskNode_->SetWorldPosition(pos);

    if (team_ != team || !glow_)
    {
        team_ = team;
        saucerModel_->SetMaterial(0, metal_[static_cast<int>(team_)]);

        switch (team_)
        {
        case TEAM_RED:   glow_ = RES(Material, "Materials/Glow.xml"); break;
        case TEAM_CYANS: glow_ = RES(Material, "Materials/Cyans.xml"); break;
        default: case TEAM_ALL:
        {
            glow_ = RES(Material, "Materials/Glow.xml")->Clone();
            for (const MaterialShaderParameter& param: glow_->GetShaderParameters().Values())
            {
                if (param.name_.Contains("Color"))
                {
                    Color col{ GetColor() };
                    col.FromHSV(col.Hue() + Random(-.05f, .1f), col.SaturationHSV(), col.Value());
                    glow_->SetShaderParameter(param.name_, col);
                }
            }
        }
        break;
        }

        saucerModel_->SetMaterial(1, glow_);

        RibbonTrail* trail{ node_->GetComponent<RibbonTrail>() };
        if (trail)
        {
            trail->SetStartColor(GetColor());
            trail->SetEndColor(GetColor().Transparent());
        }

        MarkNetworkUpdate();
    }

    if (!ENGINE->IsHeadless())
        StartEngine();

    if (node_->HasComponent<SaucerBrain>())
        node_->GetComponent<SaucerBrain>()->Reset();
}

void Saucer::ClearControl()
{
    Controllable::ClearControl();

    Player* p{ GetPlayer() };
    if (p)
    {
        thrust_ = p->GetRush();
        SubscribeToEvent(p, E_RUSHCHANGED, DRY_HANDLER(Saucer, HandleRushChanged));
    }
    else
    {
        thrust_ = false;
        UnsubscribeFromEvent(E_RUSHCHANGED);
    }
}

void Saucer::HandleRushChanged(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    thrust_ = !thrust_;
}

void Saucer::Draw(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (GetSubsystem<Network>()->GetServerConnection())
        diskNode_->SetWorldPosition(node_->GetWorldPosition());

   DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };
   debug->AddCircle(diskNode_->GetPosition(), diskNode_->GetWorldUp() * M_LARGE_EPSILON, 2.5f, GetColor(), 64, true);

   if (actions_[SIGNAL] || actionSince_[SIGNAL] < .5f)
   {
       for (int c{ 0 }; c < 3; ++c)
       {
           const float r{ .1f + Mod(4.f * actionSince_[SIGNAL] + c * .23f, 2.f) };
           debug->AddCircle(diskNode_->GetPosition() + Vector3::UP   * (.23f * Sin(r * M_RADTODEG * 17.f) + 3.f * Sqrt(r)), Vector3::UP * M_LARGE_EPSILON, r*r, GetColor().Transparent(.5f*r*r+(-.23f * r)*r), 64, true);
       }
   }

   DrawRingTrail();
   DrawArc();

   PhysicsRaycastResult result{ FloorCast() };
   debug->AddCircle(result.position_, result.normal_ * .01f, 2.f, Color::GREEN.Lerp(Color::YELLOW, .75f) * 0.5f, 64, true);

//   diskBody_->DrawDebugGeometry(debug, false);
}

void Saucer::DrawRingTrail()
{
    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };

    if (!ringTrail_.IsEmpty())
    {
        const int segs{ 3 };
        for (int s{ 1 }; s < segs - 1; ++s)
        {
            const float t{ 1.f * s / segs };
            const TrailSegment seg{ ringTrail_.Front().Interpolate({ diskNode_->GetWorldPosition(), diskNode_->GetWorldUp(), GetScene()->GetElapsedTime() }, t) };
            debug->AddCircle(seg.position_, seg.normal_ * M_EPSILON, 2.5f, GetColor().Transparent(.23f - .125f * PowN(t, 3)), 64, true);
        }
    }

    const float time{ GetScene()->GetElapsedTime() };
    for (unsigned i{ 0u }; i < ringTrail_.Size(); ++i)
    {
        const TrailSegment& seg{ ringTrail_.At(i) };
        const float a{ .333f * PowN(Clamp(2.3f * (seg.spawned_ - time) + 1.f, 0.f, 1.f), 5) };
        debug->AddCircle(seg.position_, seg.normal_ * M_EPSILON, 2.5f, GetColor().Transparent(a), 64, true);
    }
}

void Saucer::DrawArc()
{
    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };

    if (!Equals(arc_.LengthSquared(), 0.f))
    {
        Color col{};

        // Draw last arc when paused
        if (!GetScene()->IsUpdateEnabled())
        {
            for (unsigned m{ 0u }; m < lastArc_.main_.Size() - 1u; ++m)
            {
                col.FromHSV(GetColor().Hue(), .23f * m + .17f, 1.f);
                debug->AddLine(lastArc_.main_.At(m), lastArc_.main_.At(m + 1u), col);
            }

            for (const PODVector<Vector3>& branch: lastArc_.branches_)
            {
                for (unsigned b{ 0u }; b < branch.Size() - 1u; ++b)
                    debug->AddLine(branch.At(b), branch.At(b + 1u), GetColor());
            }
        }
        else
        {
            const Vector3 from{ node_->GetWorldPosition() + arc_.Normalized() * .5f };
            const Vector3 to{ from + arc_ };
            if (actionSince_[JUMP] > .05f)
                arc_ = Vector3::ZERO;
            else
                arc_ = arc_.Normalized() * (arc_.Length() - (sphereBody_->GetLinearVelocity() * TIME->GetTimeStep()).DotProduct(arc_.Normalized()));

            int n{ 8 };
            const float d{ from.DistanceToPoint(to) / n };
            Vector3 a{ from };

            Arc arc{};
            arc.main_.Push(from);

            for (int i{ 0 }; i <= n; ++i)
            {
                const Vector3 b{ (i == n ? to : (from.Lerp(to, i * 1.f / n) + Vector3{ RandomOffCenter(d), RandomOffCenter(.5f * d), RandomOffCenter(d) })) };
                for (int j{ 0 }; j < 4; ++j)
                {
                    col.FromHSV(GetColor().Hue(), .23f * j + .17f, 1.f);

                    const Vector3 r{ Quaternion{ i * 45.f, diskNode_->GetWorldUp() } *
                                     diskNode_->GetWorldUp().CrossProduct(Vector3::UP).NormalizedOrDefault(node_->GetWorldRight()) };
                    const float w{ .05f };
                    const float x{ (j - 1.5f) * w  };
                    const Vector3 start{ a + r * x };
                    const Vector3 end{ b + r * x };
                    debug->AddLine(start, end, col);
                    arc.main_.Push(end);
                }

                //Branch
                if (Random(3))
                {
                    PODVector<Vector3> branch{ a };

                    const Vector3 c{ a + Vector3{ RandomOffCenter(d), -Random(d), RandomOffCenter(d) } };
                    const float l{ c.DistanceToPoint(to) };
                    debug->AddLine(a, c, GetColor());
                    branch.Push(c);
                    if (l < d * 3.f)
                    {
                        const Vector3 cTo{ to + Vector3{ RandomOffCenter(2.f * d, 3.f * d), -Random(1/3.f * l, 2/3.f * l), RandomOffCenter(2.f * d, 3.f * d) }.ProjectOntoPlane(arc_) };
                        debug->AddLine(c, cTo, GetColor());
                        branch.Push(cTo);
                    }

                    arc.branches_.Push(branch);
                }

                a = b;
            }

            lastArc_ = arc;
        }
    }
}

void Saucer::FixedUpdate(float timeStep)
{
    RecordTrail();

//    SceneObject::FixedUpdate(timeStep);

    const Vector3 up{ GravUp() };
    const Vector3 flatDirection{ node_->GetWorldDirection().ProjectOntoPlane(up).Normalized() };

    if (GetPlayer())
    {
        if (GetPlayer()->IsLocal())
            aim_ = GetPlayer()->GetJib()->GetNode()->GetDirection();
    }

    const Vector3 flatAim{ aim_.ProjectOntoPlane(up).Normalized() };
    const Vector3 flatRight{ Quaternion{ 90.f, up } * flatAim };
    const float theta{ Asin(up.DotProduct(flatDirection.CrossProduct(flatAim))) };

    //Correct direction
    sphereBody_->ApplyTorque(up * (theta - sphereBody_->GetAngularVelocity().y_ * 5.f) * timeStep * 1e3f);

    //Accelerate
    const float thruster{ 1.f + 2/3.f * thrust_ };
    const Vector3 flatMove{ move_.x_ * flatRight + move_.z_ * flatAim };
    const Vector3 flatVelocity{ sphereBody_->GetLinearVelocity().ProjectOntoPlane(up) };
    const Vector3 curve{ flatMove.ProjectOntoPlane(flatVelocity) };
    const float speedLimit{ 64.f * thruster };

    const float dot{ flatMove.DotProduct(flatVelocity.NormalizedOrDefault(Vector3::ZERO)) };
    const float slack{ Lerp(dot, Sign(dot) * Sqrt(Abs(dot)), .5f) };
    const float limiter{ Cbrt(Max(0.f, 1.f - (slack * flatVelocity.Length() / speedLimit))) };
    const float force{ Lerp(limiter, 1.f, PowN(curve.Length(), 2)) * 4e4f * thruster };

    sphereBody_->ApplyForce(timeStep * curve.Lerp(flatMove, Cbrt(limiter)) * force);

    //Brake
    if (move_.LengthSquared() == .0f)
    {
        const Vector3 v{ sphereBody_->GetLinearVelocity().ProjectOntoPlane(up) };
        sphereBody_->ApplyForce(-v.Normalized() * Sqrt(v.Length()) * timeStep * 4e3f);
    }

    //Bank
    const Vector3 bank{ (1e-3f * sphereBody_->GetLinearVelocity().ProjectOntoPlane(up) + flatMove * Pow(thruster, 3.f) + up).Normalized() };

    const Vector3 diskUp{ diskNode_->GetWorldUp() };
    const Vector3 flatDiskDirection{ diskNode_->GetWorldDirection().ProjectOntoPlane(up).Normalized() };
    const Vector3 diskRight{ diskNode_->GetWorldRight() };

    const float psi{ Asin(diskRight.DotProduct(diskUp.ProjectOntoPlane(diskRight).Normalized().CrossProduct(bank.ProjectOntoPlane(diskRight).Normalized()))) };
    const float phi{ Asin(flatDiskDirection.DotProduct(diskUp.ProjectOntoPlane(flatDiskDirection).Normalized().CrossProduct(bank.ProjectOntoPlane(flatDiskDirection).Normalized()))) };

    diskBody_->ApplyTorque((diskBody_->GetRotation() * Vector3{ psi, 0.f, phi } - diskBody_->GetAngularVelocity() * 3.f) * timeStep * 7e2);
}

void Saucer::RecordTrail()
{
    if (ENGINE->IsHeadless())
        return;

    if (!NETWORK->GetServerConnection() &&
        Equals(sphereBody_->GetLinearVelocity() .LengthSquared(), 0.f) &&
        Equals(sphereBody_->GetAngularVelocity().LengthSquared(), 0.f))
        return;

    if (GetSubsystem<Network>()->GetServerConnection())
        diskNode_->SetWorldPosition(node_->GetWorldPosition());

    TrailSegment seg{ diskNode_->GetWorldPosition(), diskNode_->GetWorldUp(), GetScene()->GetElapsedTime() };
    if (!ringTrail_.IsEmpty())
    {
        const int segs{ 3 - GetTeam() };
        for (int s{ 1 }; s < segs; ++s)
        ringTrail_.Insert(0u, ringTrail_.At(s - 1).Interpolate(seg, 1.f / segs * s));
    }
    ringTrail_.Insert(0u, seg);
    while (ringTrail_.Size() > 23)
        ringTrail_.Pop();
}

void Saucer::Update(float timeStep)
{
    Controllable::Update(timeStep);

    const Vector3 flatVelocity{ sphereBody_->GetLinearVelocity().ProjectOntoPlane(GravUp()) };
    const float t{ Min(1.f, timeStep * 7.f) };
    const float oldFreq{ engineSource_->GetFrequency() };
    const float newFreq{ 2.3e4f + 2.3e2f * flatVelocity.Length() + 5.f * randomizer_ };
    engineSource_->SetFrequency(Lerp(oldFreq, newFreq, Min(1.f, t * oldFreq * 1e-3f)));

    RibbonTrail* trail { node_->GetComponent<RibbonTrail>() };
    if (trail)
    {
        const Color col{ trail->GetStartColor() };
        trail->SetStartColor(col.Lerp(col.Transparent(thrust_ && !Equals(0.f, move_.LengthSquared())), Min(1.f, timeStep * 10.f)));
    }
}

bool Saucer::Jump()
{
    if (!sphereBody_ || !GetScene()->IsUpdateEnabled())
        return false;

    PhysicsRaycastResult result{ FloorCast() };
    const float fromFloor{ (node_->GetWorldPosition() - result.position_).DotProduct(result.normal_) };
    if (fromFloor < 5.f)
    {
        sphereBody_->ApplyImpulse(-sphereBody_->GetLinearVelocity() * 2.3f);
        sphereBody_->ApplyImpulse(diskNode_->GetWorldUp().Lerp(GravUp(), .1f).Normalized() * 235.f * (.75f + thrust_ * .5f));

        PlaySample(RES(Sound, "Samples/Jump.wav"), .023f + .017f * thrust_, false);
        const Vector3 worldPos{ node_->GetWorldPosition() };
        arc_ = result.normal_ * (result.position_ - worldPos).DotProduct(result.normal_) - diskNode_->GetWorldUp().ProjectOntoPlane(result.normal_) * sphereBody_->GetLinearVelocity().Length() * 5e-3f;

        return true;
    }
    else
    {
        return false;
    }
}

void Saucer::HandleAction(int actionId)
{
    bool handled{ true };
    switch (actionId)
    {
    case JUMP: handled &= Jump(); break;
    case THRUST: thrust_ = true; break;
    case BEAM:   beam_   = true; break;
    default: break;
    }

    if (handled)
        Controllable::HandleAction(actionId);
}

void Saucer::HandleReleaseAction(int actionId)
{
    switch (actionId)
    {
    case THRUST: thrust_ = false; break;
    case BEAM:   beam_   = false; break;
    default: break;
    }

    Controllable::HandleReleaseAction(actionId);
}

void Saucer::HandleCollisionStart(StringHash /*eventType*/, VariantMap& eventData)
{
    if (NETWORK->GetServerConnection())
        return;

    RigidBody* otherBody{ static_cast<RigidBody*>(eventData[NodeCollisionStart::P_OTHERBODY].GetPtr()) };
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
    bool arena{ otherNode->GetParent() == GetSubsystem<Arena>()->GetNode() };

    MemoryBuffer contacts{ eventData[NodeCollisionStart::P_CONTACTS].GetBuffer() };
    const Pair<float, Vector3> impulseNormal{ GetContactMaxImpulseAndNormal(contacts) };
    float impulse{ impulseNormal.first_ / (1 + arena) };
    const Vector3 normal{ impulseNormal.second_ };

    if (impulse == 0.f && !arena)
        impulse = FakeImpulse(sphereBody_, otherBody);

    const float gain{ Min(2/3.f, PowN(impulse * 1e-3f, 2)) };
    SoundSource3D* source{ PlaySample(RES(Sound, "Samples/Thud.wav"), gain, false) };
    source->SetFrequency(source->GetFrequency() * (.95f + (Random(6) + GetTeam() * (3.f - 2.f * randomizer_)) * .05f));

    if (impulse > 256.f)
    {
        SoundSource* echo{ GetScene()->CreateComponent<SoundSource>() };
        echo->SetGain(Min(1/6.f, Cbrt(impulse * 1e-5f)));
        echo->SetAutoRemoveMode(REMOVE_COMPONENT);
        echo->Play(RES(Sound, "Samples/ThudEcho.wav"));
    }

    const Vector3 worldNormal{ arena ? normal
                                     : (node_->GetWorldPosition() - otherNode->GetWorldPosition()).Normalized() };
    if (gain > .023f && GetPlayer() && GetPlayer()->IsLocal())
    {
        Jib* jib{ GetPlayer()->GetJib() };
        Node* camNode{ jib->GetCamera()->GetNode() };
        const Vector3 direction{ camNode->GetWorldRotation().Inverse() * worldNormal};

        jib->Shake(gain * Vector3{ direction.x_ * .4f, direction.z_ * .2f, Sqrt(gain * 55.f) });
    }

    if (node_ && NETWORK->IsServerRunning())
    {
        VariantMap eventData{
            { SaucerCollision::P_IMPULSE, impulse },
            { SaucerCollision::P_NORMAL, worldNormal },
            { SaucerCollision::P_ARENA, arena }
        };

        NETWORK->BroadcastRemoteEvent(node_, E_SAUCERCOLLISION, false, eventData);
    }

    bool diskBallHit{ otherNode->HasComponent<Ball>() && static_cast<RigidBody*>(eventData[NodeCollisionStart::P_BODY].GetPtr()) == diskBody_ };
    if (diskBallHit)
    {
        const Vector3 diskUp{ diskNode_->GetWorldUp() };
        const Vector3 ballDelta{ otherNode->GetWorldPosition() - diskNode_->GetWorldPosition() };
        const Vector3 ballVelocity{ otherBody->GetLinearVelocity() };
        const Vector3 ballVector{ (ballVelocity - ballDelta.Normalized()).Normalized() };
        const float ballDot{ PowN(ballVector.DotProduct(diskUp), 3) };
        const Vector3 ballForce{ diskUp * ballDot * 5.5f };
        diskBody_->ApplyForce(-ballForce);
        otherBody->ApplyImpulse(ballForce);
        otherBody->ApplyTorqueImpulse(-ballVector.CrossProduct(diskUp) * (Max(0.f, (-ballVelocity).DotProduct(diskBody_->GetLinearVelocity()))) * (1.f - ballDot) * .023f);
    }
}

void Saucer::HandleRemoteCollision(StringHash /*eventType*/, VariantMap& eventData)
{
    const float impulse{  eventData[SaucerCollision::P_IMPULSE].GetFloat() };
    const Vector3 normal{ eventData[SaucerCollision::P_NORMAL].GetVector3() };
    const bool arena{ eventData[SaucerCollision::P_ARENA].GetBool() };
    const float gain{ Min(2/3.f, PowN(impulse * 1e-3f, 2)) };
    SoundSource3D* source{ PlaySample(RES(Sound, "Samples/Thud.wav"), gain, false) };
    source->SetFrequency(source->GetFrequency() * (.95f + (Random(6) + GetTeam() * (3.f - 2.f * randomizer_)) * .05f));

    if (impulse > 256.f)
    {
        SoundSource* echo{ GetScene()->CreateComponent<SoundSource>() };
        echo->SetGain(Min(1/6.f, Cbrt(impulse * 1e-5f)));
        echo->SetAutoRemoveMode(REMOVE_COMPONENT);
        echo->Play(RES(Sound, "Samples/ThudEcho.wav"));
    }

    if (gain > .023f && GetPlayer())
    {
        Jib* jib{ GetPlayer()->GetJib() };
        Node* camNode{ jib->GetCamera()->GetNode() };
        const Vector3 direction{ arena ? normal
                                       : camNode->GetWorldRotation().Inverse() * normal };

        jib->Shake(gain * Vector3{ direction.x_ * .4f, direction.z_ * .2f, Sqrt(gain * 55.f) });
    }
}
