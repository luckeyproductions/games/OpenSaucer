/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef JUKEBOX_H
#define JUKEBOX_H

#include "mastercontrol.h"

struct Song {
    String name_;
    float gain_;
};

class JukeBox: public Object
{
    const Vector<Song> songs_
    {
        { "Green.ogg", .42f },
        { "Red.ogg",   .34f },
        { "Cyans.ogg", .55f },
        { "Green.ogg", .42f }
    };

    DRY_OBJECT(JukeBox, Object);

public:
    enum Songs{ MUSIC_MENU = 0, MUSIC_RED, MUSIC_CYANS, MUSIC_GREEN };
    JukeBox(Context* context);

    Node* GetMusicNode() const { return musicNode_; }

    void Play(int s)
    {
        Song song{ songs_.At(s) };
        Play(song.name_, true, song.gain_);
    }

    void Play(const String& song, bool loop = true, float gain = 1.f);
    void Stop(float fadeTime);
    void PickSong();

private:
    void HandleMusicFaded(StringHash eventType, VariantMap& eventData);

    Node* musicNode_;
    Vector<SoundSource*> tracks_;
};

DRY_EVENT(E_MUSICFADED, MusicFaded)
{
}

#endif // JUKEBOX_H
