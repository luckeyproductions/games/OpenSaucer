/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BALL_H
#define BALL_H

#include "sceneobject.h"

#define BALL_SPAWNPOS (5.f * (Vector3::UP + RandomSign() * 2e-3f * Vector3::FORWARD))

class Ball: public SceneObject
{
    DRY_OBJECT(Ball, SceneObject);

public:
    void static RegisterObject(Context* context);
    Ball(Context* context);

    void Start() override;
    void DelayedStart() override;
    void Reset();

    void FixedUpdate(float timeStep) override;
    void FixedPostUpdate(float timeStep) override;

    Vector3 GetVelocity() const { return rigidBody_->GetLinearVelocity(); }

private:
    static float HitGainFromImpulse(float impulse) { return Min(1/3.f, Sqrt(Max(0.f, impulse - 23.f)) * .05f); }
    void UpdateCcdMotionThreshold();

    void Draw(StringHash eventType, VariantMap &eventData);
    void HandleCollisionStart(StringHash eventType, VariantMap& eventData);
    void HandleRemoteCollision(StringHash eventType, VariantMap& eventData);

    RigidBody* rigidBody_;
    Pair<Team, int> lastTouched_;
    TypedPolynomial<Vector3> trail_;

    SharedPtr<Sound> hit_;
    SharedPtr<Sound> thud_;

};

DRY_EVENT(E_BALLCOLLISION, BallCollision)
{
    DRY_PARAM(P_IMPULSE, Impulse);  // float
}

#endif // BALL_H
