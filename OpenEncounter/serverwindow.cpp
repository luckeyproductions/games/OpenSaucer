/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "serverwindow.h"

#include <QMouseEvent>
#include <QVBoxLayout>

ServerWindow::ServerWindow(const QString& program, const QStringList& args, int port): QWidget{nullptr},
    closeButton_{ new QPushButton{} },
    saucer_{},
    mousePos_{},
    moveWindow_{ false }
{
    QSize size{ 192, 192 };
    setMinimumSize(size);
    setMaximumSize(size);
    setWindowTitle("Open Saucer Server");
    setWindowIcon(QIcon{ ":/Icon" });
    setWindowFlag(Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground);

    QLabel* background{ new QLabel{} };
    background->setObjectName("background");
    background->setPixmap(QPixmap{ ":/Icon" }.scaled(size, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    background->setScaledContents(true);
    background->setMargin(16);
    QVBoxLayout* backgroundLayout{ new QVBoxLayout{} };
    setStyleSheet("QLabel {"
                  "background:qlineargradient("
                  "spread:pad, x1:0, y1:.023, x2:0, y2:.55,"
                  "stop:0 " + QWidget::palette().color(QPalette::Dark).name() + ","
                  "stop:1 " + QWidget::palette().color(QWidget::backgroundRole()).name() + ");"
                  "border-radius: 12px; border: 1px solid " + QWidget::palette().color(QPalette::Shadow).name() + ";"
                  "color: chartreuse; }");

    QVBoxLayout* mainLayout{ new QVBoxLayout{} };
    mainLayout->setAlignment(Qt::AlignRight);
    mainLayout->setSpacing(0);

    QHBoxLayout* closeLayout{ new QHBoxLayout{} };
    closeLayout->setAlignment(Qt::AlignRight);
    closeButton_->setIcon(QIcon{":/Close"});
    closeButton_->setFlat(true);
    closeButton_->setFixedSize({ 25, 25 });
    closeButton_->setIconSize({ 23, 23 });
    closeButton_->installEventFilter(this);
    connect(closeButton_, SIGNAL(clicked()), this, SLOT(close()));

    QLabel* runningLabel{ new QLabel{ "Server running\non port"} };
    runningLabel->setMargin(0);
    runningLabel->setAlignment(Qt::AlignCenter);
    QFont font{ runningLabel->font() };
    font.setPointSize(11);
    font.setWeight(100);
    runningLabel->setFont(font);
    QLabel* portLabel{ new QLabel{ QString::number(port) } };
    portLabel->setMargin(0);
    portLabel->setAlignment(Qt::AlignCenter);
    font.setPointSize(17);
    font.setWeight(100);
    portLabel->setFont(font);

    closeLayout->addWidget(closeButton_);
    backgroundLayout->addLayout(closeLayout);
    backgroundLayout->addSpacerItem(new QSpacerItem{ 0, 72 } );
    backgroundLayout->addWidget(runningLabel);
    backgroundLayout->addWidget(portLabel);
    background->setLayout(backgroundLayout);
    mainLayout->addWidget(background);
    setLayout(mainLayout);
    installEventFilter(this);
    show();

    saucer_ = new QProcess{};
    saucer_->start(program, args);
    connect(saucer_, SIGNAL(finished(int)), this, SLOT(close()));
}

ServerWindow::~ServerWindow()
{
    saucer_->deleteLater();
}

void ServerWindow::closeEvent(QCloseEvent*)
{
    saucer_->kill();
}

void ServerWindow::mouseMoveEvent(QMouseEvent* event)
{
    if(moveWindow_)
    {
        move(geometry().topLeft() + event->globalPos() - mousePos_);
        mousePos_ = event->globalPos();
    }
}

bool ServerWindow::eventFilter(QObject* object, QEvent* event)
{
    if (object == closeButton_)
    {
        QHoverEvent* hoverEvent{ dynamic_cast<QHoverEvent*>(event) };
        if (hoverEvent)
        {
            if (event->type() == QEvent::HoverEnter)
                closeButton_->setIcon(QIcon{ ":/CloseHover" });
            else if (event->type() == QEvent::HoverLeave)
                closeButton_->setIcon(QIcon{ ":/Close" });
        }
    }
    else
    {
        QMouseEvent* mouseEvent{ dynamic_cast<QMouseEvent*>(event) };
        if (mouseEvent && mouseEvent->button() == Qt::LeftButton)
        {
            if (event->type() == QEvent::MouseButtonPress)
            {
                mousePos_ = mouseEvent->globalPos();
                moveWindow_ = true;
                return true;
            }
            else if (event->type() == QEvent::MouseButtonRelease)
            {
                moveWindow_ = false;
                return true;
            }
        }
    }

    return false;
}
