/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QComboBox>

namespace sf
{
class VideoMode;
}

class MainWindow: public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private:
    QPixmap inputPixmap(int number, bool joystick = true);
    QLabel* linkedLabel(QWidget* parent, QString text);
    QLabel* linkedLabel(QWidget* parent) { return parent->property("label").value<QLabel*>(); }
    QHBoxLayout* createNetworkRow();
    QHBoxLayout* createJoystickRow();
    QWidget* createSettingsWidget();
    QHBoxLayout* createResolutionRow();
    QString findSaucer() const;

    void updateNumJoysticks();

    void mouseMoveEvent(QMouseEvent* event);
    bool eventFilter(QObject* object, QEvent* event);

    QLabel*      logo_;
    QPushButton* closeButton_;

    QCheckBox*   hostBox_;
    QCheckBox*   joinBox_;
    QLineEdit*   ipBox_;
    QSpinBox*    portBox_;

    QPushButton* keyboardButton_;
    QVector<QPushButton*> joystickButtons_;
    QSpinBox*    playersBox_;
    QSpinBox*    teamSizeBox_;
    QCheckBox*   recordBox_;

    QWidget* settingsWidget_;
    QComboBox*   resolutionBox_;
    QSpinBox*    windowWidthBox_;
    QSpinBox*    windowHeightBox_;
    QCheckBox*   fullscreenBox_;
    QCheckBox*   borderlessBox_;
    QCheckBox*   antialiasingBox_;
    QCheckBox*   bloomBox_;

    QCheckBox*   musicBox_;
    QCheckBox*   samplesBox_;

    QMap<int, int> teamPreference_;
    QPoint mousePos_;
    bool moveWindow_;
    int numJoysticks_;
    
private slots:
    void networkRoleChanged();
    void handleNumPlayersChanged(int players);
    void handleInputButtonClicked();
    void updateResolutionBoxesVisibility();
    void onTimeout();
    void launch();
};

inline QSize ModeToSize(sf::VideoMode mode);
inline QString SizeToText(const QSize& size);
inline int area(const QSize& size)                                  { return abs(size.width() * size.height()); }
inline bool resolutionGreaterThan(const QSize& r1, const QSize& r2) { return area(r1) > area(r2); }
inline uint qHash(const QSize& key)                                 { return (key.width() << 16) + key.height(); }

#endif // MAINWINDOW_H
