/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SERVERWINDOW_H
#define SERVERWINDOW_H

#include <QProcess>
#include <QPushButton>

#include <QLabel>

class ServerWindow : public QWidget
{
    Q_OBJECT
public:
    explicit ServerWindow(const QString& program, const QStringList& args, int port);
    ~ServerWindow();
    void closeEvent(QCloseEvent*) override;

private:
    void mouseMoveEvent(QMouseEvent* event) override;
    bool eventFilter(QObject* object, QEvent* event) override;

    QPushButton* closeButton_;
    QProcess* saucer_;

    QPoint mousePos_;
    bool moveWindow_;
};

#endif // SERVERWINDOW_H
