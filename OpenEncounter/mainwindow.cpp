/* Open Saucer
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mainwindow.h"
#include "serverwindow.h"

#include <QAction>
#include <QPainter>
#include <QProcess>
#include <QDir>
#include <QDebug>
#include <QScreen>
#include <QWindow>
#include <QFormLayout>
#include <QSizeGrip>
#include <QRandomGenerator>
#include <QTimer>
#include <QGuiApplication>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <unordered_set>
#include <QMouseEvent>

MainWindow::MainWindow(QWidget* parent): QMainWindow(parent),
    logo_            { new QLabel{}      },
    closeButton_     { new QPushButton{} },
    hostBox_         { new QCheckBox{} },
    joinBox_         { new QCheckBox{} },
    ipBox_           { new QLineEdit{} },
    portBox_         { new QSpinBox{} },
    keyboardButton_  { nullptr },
    joystickButtons_ {},
    playersBox_      { new QSpinBox{}    },
    teamSizeBox_     { new QSpinBox{}    },
    recordBox_       { new QCheckBox{}   },
    settingsWidget_  { nullptr },
    resolutionBox_   { new QComboBox{}   },
    windowWidthBox_  { new QSpinBox{}    },
    windowHeightBox_ { new QSpinBox{}    },
    fullscreenBox_   { new QCheckBox{}   },
    borderlessBox_   { new QCheckBox{}   },
    antialiasingBox_ { new QCheckBox{}   },
    bloomBox_        { new QCheckBox{}   },
    musicBox_        { new QCheckBox{}   },
    samplesBox_      { new QCheckBox{}   },
    teamPreference_  {},
    mousePos_        {},
    moveWindow_      { false },
    numJoysticks_    { 0 }
{
    setWindowTitle("Open Encounter");
    setWindowIcon(QIcon{ ":/Icon" });
    setWindowFlag(Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground);
    setStyleSheet("#mainWidget {"
                  "background:qlineargradient("
                  "spread:pad, x1:0, y1:.023, x2:0, y2:.55,"
                  "stop:0 rgba(5, 55, 5, 111),"
                  "stop:1 " + QWidget::palette().color(QWidget::backgroundRole()).name() + ");"
                  "border-radius: 12px; border: 1px solid " + QWidget::palette().color(QPalette::Shadow).name() + ";}");

    for (QCheckBox* box: { fullscreenBox_, antialiasingBox_, bloomBox_, musicBox_, samplesBox_ })
        box->setChecked(true);

    QWidget* mainWidget{ new QWidget{} };
    mainWidget->setObjectName("mainWidget");
    QVBoxLayout* mainLayout{ new QVBoxLayout{} };
    mainLayout->setContentsMargins(8, 5, 8, 3);

    closeButton_->setIcon(QIcon{":/Close"});
    closeButton_->setFlat(true);
    closeButton_->setFixedSize({ 25, 25 });
    closeButton_->setIconSize({ 23, 23 });
    closeButton_->installEventFilter(this);
    connect(closeButton_, SIGNAL(clicked()), this, SLOT(close()));

    logo_->setPixmap(QPixmap{ ":/Logo" }.copy(0, 24, 640, 320)
                     .scaled(480, 240, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    logo_->setAlignment(Qt::AlignCenter);
    QVBoxLayout* logoLayout{ new QVBoxLayout{} };
    logoLayout->addWidget(closeButton_);
    logoLayout->addItem(new QSpacerItem{ 0, logo_->height()});
    logoLayout->setAlignment(Qt::AlignRight);
    logo_->setLayout(logoLayout);
    logo_->installEventFilter(this);

    QPushButton* launchButton{ new QPushButton{ "Start Match" } };
    connect(launchButton, SIGNAL(clicked()), this, SLOT(launch()));

    QFormLayout* form{ new QFormLayout{} };
    form->setContentsMargins(24, 4, 24, 8);
    form->addRow(linkedLabel(playersBox_, "Players"), playersBox_);
    form->addRow(linkedLabel(teamSizeBox_, "Team size"), teamSizeBox_);
    form->addRow("Record replay", recordBox_);

    QSizeGrip* sizeGrip{ new QSizeGrip{ mainWidget } };
    sizeGrip->setLayoutDirection(Qt::RightToLeft);
    sizeGrip->setFixedSize(24, 24);
    QHBoxLayout* gripLayout{ new QHBoxLayout{} };
    gripLayout->setContentsMargins(0,0,0,0);
    QLabel* gripLabel{ new QLabel{} };
    QPixmap gripPixmap{ QSize{ 24, 16 } };
    gripPixmap.fill(Qt::transparent);
    QPainter p{ &gripPixmap };
    p.setRenderHint(QPainter::Antialiasing);
    p.setOpacity(.7);
    p.setPen(QPen{{ QWidget::palette().color(QPalette::Shadow) }, 2 });
    p.drawEllipse(-13, -9, 37, 23);
    p.setPen(QPen{{ QWidget::palette().color(QPalette::Light) }, 1 });
    p.drawEllipse(-13, -8, 37, 23);
    p.drawPixmap(-6, -1, gripPixmap);
    gripLabel->setPixmap(gripPixmap);
    gripLabel->setMinimumWidth(16);
    gripLabel->setMaximumHeight(16);
    gripLayout->addWidget(gripLabel);
    gripLayout->setAlignment(Qt::AlignLeft);
    sizeGrip->setLayout(gripLayout);

    mainLayout->addWidget(logo_);
    mainLayout->addWidget(launchButton);
    mainLayout->addLayout(createNetworkRow());
    mainLayout->addLayout(createJoystickRow());
    mainLayout->addLayout(form);
    QFrame* line{ new QFrame{} };
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    mainLayout->addWidget(line);
    mainLayout->addWidget(settingsWidget_ = createSettingsWidget());
    mainLayout->addWidget(sizeGrip);
    mainLayout->setStretchFactor(logo_, 1);
    mainWidget->setLayout(mainLayout);

    int numPlayers{ std::max(1, numJoysticks_) };

    playersBox_->setMinimum(0);
    playersBox_->setMaximum(16);
    playersBox_->setValue(numPlayers);
    connect(playersBox_, SIGNAL(valueChanged(int)), this, SLOT(handleNumPlayersChanged(int)));

    teamSizeBox_->setMinimum(1);
    teamSizeBox_->setMaximum(21); // 3 x 7 saucers
    teamSizeBox_->setValue(std::max(numPlayers / 2 + (numPlayers % 2),
                                    QRandomGenerator::global()->bounded(3) + 1));

    networkRoleChanged();
    setCentralWidget(mainWidget);
    launchButton->setFocus();

    auto centerToScreen = [&](){
        const QRect screenGeometry{ QGuiApplication::primaryScreen()->geometry() };
        move(screenGeometry.center() - frameGeometry().center()
             - QPoint{ 0, screenGeometry.height() / 17 } );
    };
    QTimer::singleShot(1, centerToScreen);

    onTimeout();
    QTimer* timer{ new QTimer{ this } };
    connect(timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    timer->start(23);

    show();
}

MainWindow::~MainWindow()
{
}

void MainWindow::onTimeout()
{
    sf::Joystick::update();
    updateNumJoysticks();
}

QPixmap MainWindow::inputPixmap(int number, bool joystick)
{
    int team{ teamPreference_.value(number, -1) };

    QColor col{ QWidget::palette().color(QPalette::Shadow) };
    if (team == 0)
        col = Qt::red;
    else if (team == 1)
        col = Qt::cyan;

    QPixmap joystickPixmap{ (joystick ? ":/Joystick" : ":/Keyboard") };
    QPainter joystickPainter{ &joystickPixmap };
    joystickPainter.setCompositionMode(QPainter::CompositionMode_SourceAtop);
    joystickPainter.fillRect(joystickPixmap.rect(), col);

    if (number > 0)
    {
        QFont font{ joystickPainter.font() };
        font.setWeight(64);
        joystickPainter.setFont(font);
        const QString numberString{ QString::number(number) };
        const QPoint numberPosition{ joystickPixmap.width()  / 2 - joystickPainter.fontMetrics().descent() - 1,
                                     joystickPixmap.height() / 2 + 1 };
        joystickPainter.setPen(Qt::black);
        joystickPainter.drawText(numberPosition + QPoint{ 0, 1 }, numberString);
        joystickPainter.setPen(QWidget::palette().color(QPalette::Text));
        joystickPainter.drawText(numberPosition, numberString);
    }

    return joystickPixmap;
}

QLabel* MainWindow::linkedLabel(QWidget* parent, QString text)
{
    QLabel* label{ new QLabel{ text } };
    QVariant labelVariant{};
    labelVariant.setValue(label);
    parent->setProperty("label", labelVariant);
    return label;
}

QHBoxLayout* MainWindow::createNetworkRow()
{
    QHBoxLayout* networkRow{ new QHBoxLayout{} };
    networkRow->setContentsMargins(20, 0, 20, 4);

    networkRow->addWidget(new QLabel{ "Host" });
    networkRow->addWidget(hostBox_);
    networkRow->addWidget(new QLabel{ "Join" });
    networkRow->addWidget(joinBox_);
    networkRow->addWidget(linkedLabel(ipBox_, "IP"));
    networkRow->addWidget(ipBox_);
    networkRow->addWidget(linkedLabel(portBox_, "Port"));
    networkRow->addWidget(portBox_);

    networkRow->setStretch(1, 1);
    networkRow->setStretch(3, 1);
    networkRow->setStretch(5, 5);
    networkRow->setStretch(7, 2);

    ipBox_->setText("localhost");
    portBox_->setMinimum(1024);
    portBox_->setMaximum(65535);
    portBox_->setValue(2030);

    connect(hostBox_, SIGNAL(toggled(bool)), this, SLOT(networkRoleChanged()));
    connect(joinBox_, SIGNAL(toggled(bool)), this, SLOT(networkRoleChanged()));

    return networkRow;
}

QHBoxLayout* MainWindow::createJoystickRow()
{
    QHBoxLayout* joystickRow{ new QHBoxLayout{} };
    joystickRow->setAlignment(Qt::AlignCenter);
    for (int j{ 0 }; j <= 010; ++j)
    {
        const int p{ std::max(j, 1) };
        QPushButton* joystickButton{ new QPushButton{} };
        joystickButton->setFixedSize(32, 24);
        const QPixmap pixmap{ inputPixmap(p, j != 0) };
        joystickButton->setIcon(pixmap);
        joystickButton->setIconSize(pixmap.rect().size());
        joystickButton->setFlat(true);
        joystickRow->addWidget(joystickButton);
        joystickButton->setProperty("player", p);
        joystickButton->setProperty("team", -1);
        connect(joystickButton, SIGNAL(clicked()), this, SLOT(handleInputButtonClicked()));

        if (j != 0)
        {
            joystickButton->setVisible(false);
            joystickButtons_.push_back(joystickButton);
        }
        else
        {
            keyboardButton_ = joystickButton;
        }
    }

    return joystickRow;
}

QWidget* MainWindow::createSettingsWidget()
{
    QGridLayout* settingsLayout{ new QGridLayout{} };
    settingsLayout->setContentsMargins(24, 4, 24, 0);

    settingsLayout->addWidget(new QLabel{"Resolution"},     0, 0);
    settingsLayout->addLayout(createResolutionRow(),        0, 1, 1, 3);
    settingsLayout->addWidget(new QLabel{"Fullscreen"},     1, 0);
    settingsLayout->addWidget(fullscreenBox_,               1, 1);
    settingsLayout->addWidget(linkedLabel(borderlessBox_, "Borderless"), 1, 2);
    settingsLayout->addWidget(borderlessBox_,               1, 3);

    settingsLayout->addWidget(new QLabel{"Anti-aliasing"},  0, 4);
    settingsLayout->addWidget(antialiasingBox_,             0, 5);
    settingsLayout->addWidget(new QLabel{ "Bloom" },        1, 4);
    settingsLayout->addWidget(bloomBox_,                    1, 5);

    QFrame* line{ new QFrame{} };
    line->setFrameShape(QFrame::VLine);
    line->setFrameShadow(QFrame::Sunken);
    settingsLayout->addWidget(line,                         0, 6, 2, 1);

    settingsLayout->addWidget(new QLabel{ "Music" },        0, 7);
    settingsLayout->addWidget(musicBox_,                    0, 8);
    settingsLayout->addWidget(new QLabel{ "SoundFX" },      1, 7);
    settingsLayout->addWidget(samplesBox_,                  1, 8);

    QWidget* settingsWidget{ new QWidget{} };
    settingsWidget->setLayout(settingsLayout);
    updateResolutionBoxesVisibility();
    connect(fullscreenBox_, SIGNAL(toggled(bool)), this, SLOT(updateResolutionBoxesVisibility()));

    return settingsWidget;
}

QHBoxLayout* MainWindow::createResolutionRow()
{
    QHBoxLayout* resolutionRow{ new QHBoxLayout{} };
    resolutionRow->addWidget(resolutionBox_);
    resolutionRow->addWidget(windowWidthBox_);
    resolutionRow->addWidget(windowHeightBox_);

    QList<QSize> resolutions{};
    for (auto mode: sf::VideoMode::getFullscreenModes())
    {
        const QSize resolution{ ModeToSize(mode) };
        if (!resolutions.contains(resolution))
            resolutions.push_back(resolution);
    }

    std::sort(resolutions.begin(), resolutions.end(), resolutionGreaterThan);
    for (const QSize& resolution: resolutions)
        resolutionBox_->addItem(SizeToText(resolution), { resolution });

    sf::VideoMode mode{ sf::VideoMode::getDesktopMode() };
    QSize currentResolution{ ModeToSize(mode) };
    resolutionBox_->setCurrentText(SizeToText(currentResolution));

    QSize maximumResolution{ resolutions.first() };
    windowWidthBox_->setMinimum(010);
    windowWidthBox_->setMaximum(maximumResolution.width());
    windowWidthBox_ ->setValue(currentResolution.width());
    windowHeightBox_->setMinimum(010);
    windowHeightBox_->setMaximum(maximumResolution.height());
    windowHeightBox_->setValue(currentResolution.height());

    return resolutionRow;
}

void MainWindow::networkRoleChanged()
{
    const bool host{ hostBox_->isChecked() };
    const bool join{ joinBox_->isChecked() };
    const bool local{ !host && !join};
    const bool headless{ host && !join};
    const bool setup{ local || host };

    ipBox_->setEnabled(join && !host);
    linkedLabel(ipBox_)->setEnabled(ipBox_->isEnabled());
    portBox_->setEnabled(!local);
    linkedLabel(portBox_)->setEnabled(portBox_->isEnabled());
    teamSizeBox_->setEnabled(setup);
    linkedLabel(teamSizeBox_)->setEnabled(teamSizeBox_->isEnabled());
    playersBox_->setEnabled(!headless);
    QLabel* playersLabel{ linkedLabel(playersBox_) };
    playersLabel->setText((local ? "Players" : "Local players"));
    playersLabel->setEnabled(playersBox_->isEnabled());
    settingsWidget_->setEnabled(!headless);
}

void MainWindow::handleNumPlayersChanged(int players)
{
    if (players == 0)
    {
        for (QPushButton* l: joystickButtons_)
            l->setIcon(inputPixmap(0, true));
        keyboardButton_->setIcon(inputPixmap(0, false));
    }
    else
    {
        for (int j{ 0 }; j < joystickButtons_.count(); ++j)
        {
            QPushButton* l{ joystickButtons_.at(j) };
            l->setIcon(inputPixmap((j < players ? (j + (numJoysticks_ < players ? 2 : 1))
                                                  : 0)));
        }

        keyboardButton_->setIcon(inputPixmap(1, false));
    }
}

void MainWindow::handleInputButtonClicked()
{
    int player{ sender()->property("player").toInt() };
    int team{ teamPreference_.value(player, -1) + 1 };
    if (team > 1)
        team = -1;

    teamPreference_[player] = team;

    if (player == 1)
        keyboardButton_->setIcon(inputPixmap(player, false));

    joystickButtons_.at(player - 1)->setIcon(inputPixmap(player));
}

void MainWindow::updateResolutionBoxesVisibility()
{
    const bool fullscreen{ fullscreenBox_->isChecked() };

    if (fullscreen)
    {
        windowWidthBox_ ->setVisible(false);
        windowHeightBox_->setVisible(false);
        resolutionBox_  ->setVisible(true);
        borderlessBox_  ->setEnabled(false);
        linkedLabel(borderlessBox_)->setEnabled(false);

        const QSize windowSize{ windowWidthBox_->value(), windowHeightBox_->value() };
        int mismatch{ std::numeric_limits<int>::max() };
        int index{ 0 };
        for (int i{ 0 }; i < resolutionBox_->count(); ++i)
        {
            const int difference{ abs(area(resolutionBox_->itemData(i).toSize()) - area(windowSize)) };
            if (difference < mismatch)
            {
                mismatch = difference;
                index = i;
            }
        }

        resolutionBox_->setCurrentIndex(index);
    }
    else
    {
        resolutionBox_  ->setVisible(false);
        windowWidthBox_ ->setVisible(true);
        windowHeightBox_->setVisible(true);
        borderlessBox_  ->setEnabled(true);
        linkedLabel(borderlessBox_)->setEnabled(true);

        windowWidthBox_ ->setValue(resolutionBox_->currentData().toSize().width());
        windowHeightBox_->setValue(resolutionBox_->currentData().toSize().height());
    }
}

void MainWindow::updateNumJoysticks()
{
    int numJoysticks{};
    for (int j{ 0 }; j < 010; ++j)
    {
        if (sf::Joystick::isConnected(j))
            ++numJoysticks;
    }

    if (numJoysticks_ != numJoysticks)
    {
        numJoysticks_ = numJoysticks;

        for (int l{ 0 }; l < joystickButtons_.count(); ++l)
            joystickButtons_.at(l)->setVisible(l < numJoysticks_);

        // Automatically increase number of players and teamsize
        const int numPlayers{ std::max(playersBox_->value(),
                                       numJoysticks_) };
        if (playersBox_->value() != numPlayers)
        {
            playersBox_->setValue(numPlayers);
            teamSizeBox_->setValue(std::max(teamSizeBox_->value(),
                                            numPlayers / 2 + (numPlayers % 2)));
        }

        handleNumPlayersChanged(numPlayers);
    }
}

void MainWindow::launch()
{
    QStringList args{ (fullscreenBox_->isChecked() ? resolutionBox_->currentText()
                                                   : windowWidthBox_ ->text() + "x"
                                                   + windowHeightBox_->text()
                                                   + (borderlessBox_->isChecked() ? "bw" : "w")),
                "-a=" + QString::number(antialiasingBox_->isChecked()),
                "-b=" + QString::number(bloomBox_       ->isChecked()),
                "-m=" + QString::number(musicBox_       ->isChecked()),
                "-s=" + QString::number(samplesBox_     ->isChecked()),
                "-p=" + playersBox_ ->text(),
                "-t=" + teamSizeBox_->text() };

    if (recordBox_->isChecked())
        args.push_back("-r");
    if (hostBox_->isChecked())
        args.push_back("-h=" + portBox_->text());
    if (joinBox_->isChecked())
        args.push_back("-j=" + ipBox_->text() + ":" + portBox_->text());

    for (int p: teamPreference_.keys())
        args.push_back("-" + QString::number(p) + "=" + QString::number(teamPreference_.value(p, -1)));

    const QString program{ findSaucer() };
    if (!program.isEmpty())
    {
        if (hostBox_->isChecked() && !joinBox_->isChecked())
            new ServerWindow{ program, args, portBox_->value() };
        else
            QProcess::startDetached(program, args);
    }
}

QString MainWindow::findSaucer() const
{
    const QString binName{ "/saucer" };
    const QList<QString> possibleLocations
    {
        QDir::currentPath().append(binName),
        QDir::currentPath().replace("OpenEncounter", "OpenSaucer").append(binName),
        QDir::currentPath().append("/bin" + binName),
        "/usr/games" + binName,
        "/usr/bin" + binName,
        QDir::homePath() + "/.config/itch/apps/opensaucer" + binName,
        QDir::homePath() + "/.config/itch/apps/opensaucer/bin" + binName
    };

    for (const QString& location: possibleLocations)
        if (QFile::exists(location))
            return location;

    return "";
}

void MainWindow::mouseMoveEvent(QMouseEvent* event)
{
    if(moveWindow_)
    {
        move(geometry().topLeft() + event->globalPos() - mousePos_);
        mousePos_ = event->globalPos();
    }
}

bool MainWindow::eventFilter(QObject* object, QEvent* event)
{
    if (object == logo_)
    {
        QMouseEvent* mouseEvent{ dynamic_cast<QMouseEvent*>(event) };
        if (mouseEvent && mouseEvent->button() == Qt::LeftButton)
        {
            if (event->type() == QEvent::MouseButtonPress)
            {
                mousePos_ = mouseEvent->globalPos();
                moveWindow_ = true;
                return true;
            }
            else if (event->type() == QEvent::MouseButtonRelease)
            {
                moveWindow_ = false;
                return true;
            }
        }
    }
    else if (object == closeButton_)
    {
        QHoverEvent* hoverEvent{ dynamic_cast<QHoverEvent*>(event) };
        if (hoverEvent)
        {
            if (event->type() == QEvent::HoverEnter)
                closeButton_->setIcon(QIcon{ ":/CloseHover" });
            else if (event->type() == QEvent::HoverLeave)
                closeButton_->setIcon(QIcon{ ":/Close" });
        }
    }

    return false;
}

QSize ModeToSize(sf::VideoMode mode)
{
#if SFML_VERSION_MAJOR >= 3
    return { static_cast<int>(mode.size.x),
             static_cast<int>(mode.size.y) };
#else
    return { static_cast<int>(mode.width),
             static_cast<int>(mode.height) };
#endif
}

QString SizeToText(const QSize& size)
{
    return QString::number(size.width()) + "x" + QString::number(size.height());
}
