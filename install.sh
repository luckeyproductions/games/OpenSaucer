#!/bin/sh
# abort this script on any error
set -e

if [ -z "$DRY_HOME" ]
then
    ./.installreq.sh
    ./.builddry.sh
fi

packages="";
if [ -x "$(command -v apt-get)" ]
then
    dpkg --verify libsfml-dev || packages=$packages"libsfml-dev "
    if [ -n "$packages" ]
    then sudo apt-get install $packages
    fi
fi

git pull
qmake OpenSaucer.pro -o ../Saucer-build/; cd ../Saucer-build
sudo make install; cd -; 
qmake OpenEncounter.pro -o ../Encounter-build/; cd ../Encounter-build
sudo make install; cd -;
./.postinstall.sh
rm -rf ../Saucer-build
rm -rf ../Encounter-build
