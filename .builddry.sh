#!/bin/sh
# abort this script on any error
set -e

cd `dirname $0`;
if [ ! -d Dry ]
then
    git clone https://gitlab.com/luckeyproductions/Dry.git
    cd Dry
else
    cd Dry
    git checkout master
    git pull
fi

./script/cmake_clean.sh
./script/cmake_generic.sh . \
    -DDRY_ANGELSCRIPT=0 -DDRY_2D=0 \
    -DDRY_SAMPLES=0 -DDRY_TOOLS=0
make
export DRY_HOME=$PWD
cd ..
